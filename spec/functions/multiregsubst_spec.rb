require 'spec_helper'

describe 'multiregsubst' do
  it 'is defined' do
    is_expected.not_to eq(nil)
  end

  it 'validates parameters' do
    is_expected.to run \
      .with_params(12, %r{test} => 'two')
      .and_raise_error(ArgumentError,
                       %r{parameter 'target' expects a String value, got Integer})
    is_expected.to run \
      .with_params('test', [%r{test}, 'two'])
      .and_raise_error(ArgumentError,
                       %r{parameter 'substmap' expects a Hash value, got Tuple})
  end

  it 'is idempotent when the pattern does not match' do
    is_expected.to run \
      .with_params(
        'joker',
        %r{batman} => 'robin',
      )
      .and_return('joker')
  end

  it 'correctly substitutes a single pattern' do
    is_expected.to run \
      .with_params(
        'batman_is_the_best',
        %r{batman} => 'robin',
      )
      .and_return('robin_is_the_best')
  end

  it 'correctly substitutes multiple patterns' do
    is_expected.to run \
      .with_params(
        '/db2/_SID_/db2_sid_',
        %r{_SID_} => 'SCP',
        %r{_sid_} => 'scp',
      )
      .and_return('/db2/SCP/db2scp')
  end

  it 'performs the substitutions in order' do
    is_expected.to run \
      .with_params(
        '/db2/_SID_/db2_sid_',
        %r{_SID_} => '_sid_test',
        %r{_sid_} => 'scp',
      )
      .and_return('/db2/scptest/db2scp')
  end

  it 'replaces multiple occurances of the same pattern' do
    is_expected.to run \
      .with_params(
        'one_this_two_to_two',
        %r{one} => '1',
        %r{two} => '2',
      )
      .and_return('1_this_2_to_2')
  end
end
