# @summary Hash describing an instance which is part of a given SAP SID.
#
# @param ha_mode
#   Indicates the HA mode used by this database. Valid values are described
#   by [Sap::DbHaMode](#sapdbhamode)
#
# @param nodes
#   An array containing the trusted cert names of each node that is part of
#   this database cluster.
#
# @param fcm_base_port
#   The TCP/IP port used as a starting point for configuring this instance.
#   It will be added to the `com_port` to determine the first communication
#   port for this database.
#
# @param com_port
#   The TCP/IP port used to by clients to connect to this database instance.
#
# @param host
#   Virtual name used by clients to connect to the HA/DR DB2 cluster. Mandatory
#   when ha_mode is set to 'hadr'.
#
# @param hadr_base_port
#   Used in the configuration of DB2 HA settings.
# 
type Sap::SIDDatabaseDb2 = Struct[
  ha_mode                  => Sap::DbHaMode,
  nodes                    => Array[Stdlib::Fqdn],
  fcm_base_port            => Stdlib::Port,
  com_port                 => Stdlib::Port,
  Optional[host]           => Stdlib::Fqdn,
  Optional[hadr_base_port] => Stdlib::Port,
]
