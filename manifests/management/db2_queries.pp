# @summary Implementation detail - creates sql query scheduling infrastructure
#
# @param system_ids
#   Replicated from sap::system_ids primarily to make the test code easier to
#   write. See the parent for further detail.
#   
class sap::management::db2_queries (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids = $sap::system_ids,
) {
  # Create the overal script
  file { '/usr/local/bin/db2_execute_sql':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => file('sap/db2_execute_sql'),
  }

  # Create the individual SQL components for each SID
  $system_ids.each |$sid, $sid_data| {
    # Skip non-database SIDs 
    unless('db2' in $sid_data['components'] and 'db2_query_schedules' in $sid_data) { next() }

    # Create the query directory for this SID
    $sid_lower = downcase($sid)
    $query_directory = "/db2/db2${sid_lower}/queries"
    file { $query_directory:
      ensure => 'directory',
      owner  => "db2${sid_lower}",
      group  => "db${sid_lower}adm",
    }

    # Iterate through each query schedule and create the local type
    $sid_data['db2_query_schedules'].each |$index, $entry| {
      sap::management::db2_query_schedule { "db2_${sid}_schedule_${index}":
        sid             => $sid,
        query_directory => $query_directory,
        require         => File[$query_directory],
        *               => $entry,
      }
    }
  }
}
