# @summary Format of an SAP system ID
#
# @example Valid data
#   ECP
#   PRD
#   QAS
#   SCP
#   EQ0
type Sap::SID = Pattern[/\A[A-Z0-9]{3}\z/]
