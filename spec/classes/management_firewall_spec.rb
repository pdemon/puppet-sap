require 'spec_helper'
require 'deep_merge'

describe 'sap::management::firewall' do
  let(:params) do
    {
      system_ids: {},
    }
  end
  let(:service_definitions) do
    {
      'dispatcher' => {
        'desc' => 'SAP Dispatcher Ports',
        'ports' => [
          {
            'port_pattern' => '32%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '47%02d',
            'protocol' => 'tcp',
          },
        ],
      },
      'enqueue' => {
        'desc' => 'SAP Enqueue Server',
        'ports' => [
          {
            'port_pattern' => '32%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d16',
            'protocol' => 'tcp',
          },
        ],
      },
      'gateway' => {
        'desc' => 'SAP Gateway Services',
        'ports' => [
          {
            'port_pattern' => '33%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '48%02d',
            'protocol' => 'tcp',
          },
        ],
      },
      'igs' => {
        'desc' => 'SAP Internet Graphics Server',
        'ports' => [
          {
            'port_pattern' => '4%02d00',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '4%1$02d01:4%1$02d99',
            'protocol' => 'tcp',
          },
        ],
      },
      'java_as' => {
        'desc' => 'SAP Java Application Server',
        'ports' => [
          {
            'port_pattern' => '5%02d00',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d01',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d02',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d03',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d04',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d05',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d06',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d07',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d08',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d10',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%1$02d20:5%1$02d96',
            'protocol' => 'tcp',
          },
        ],
      },
      'livecache' => {
        'desc' => 'SAP liveCache Communication Ports',
        'ports' => [
          {
            'port' => '7200',
            'protocol' => 'tcp',
          },
          {
            'port' => '7210',
            'protocol' => 'tcp',
          },
          {
            'port' => '7269',
            'protocol' => 'tcp',
          },
          {
            'port' => '7270',
            'protocol' => 'tcp',
          },
        ],
      },
      'message_server' => {
        'desc' => 'SAP Message Server',
        'ports' => [
          {
            'port_pattern' => '36%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '39%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '81%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '444%02d',
            'protocol' => 'tcp',
          },
        ],
      },
      'sapctrl' => {
        'desc' => 'SAP HTTP/S Control',
        'ports' => [
          {
            'port_pattern' => '5%02d13',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '5%02d14',
            'protocol' => 'tcp',
          },
        ],
      },
      'webdisp' => {
        'desc' => 'SAP Web Dispatcher Services',
        'ports' => [
          {
            'port_pattern' => '80%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '443%02d',
            'protocol' => 'tcp',
          },
        ],
      },
      'web_icm' => {
        'desc' => 'SAP ICM Web Services',
        'ports' => [
          {
            'port_pattern' => '80%02d',
            'protocol' => 'tcp',
          },
          {
            'port_pattern' => '443%02d',
            'protocol' => 'tcp',
          },
          {
            'port' => '25',
            'protocol' => 'tcp',
          },
        ],
      },
    }
  end
  let(:inst_class_services) do
    {
      'as-abap' => [
        'dispatcher',
        'gateway',
        'web_icm',
        'sapctrl',
        'igs',
      ],
      'as-java' => [
        'java_as',
        'sapctrl',
        'igs',
      ],
      'as_dual' => [
        'dispatcher',
        'gateway',
        'web_icm',
        'sapctrl',
        'igs',
        'java_as',
      ],
      'cs-abap' => [
        'enqueue',
        'message_server',
        'gateway',
        'webdisp',
        'sapctrl',
      ],
      'cs-java' => [
        'enqueue',
        'message_server',
        'gateway',
        'webdisp',
        'sapctrl',
      ],
      'gateway' => [
        'gateway',
        'sapctrl',
      ],
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'service_map' do
        before(:each) do
          params.merge!(
            services: {},
            service_map: {
              'as-abap' => ['dispatcher'],
            },
          )
        end

        it 'maps an instance class to an undefined rule category' do
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall: 'as-abap' includes rule 'dispatcher' with no corresponding definition in \$services!})
        end
      end

      context 'services' do
        let(:rule_category) { 'dispatcher' }

        before(:each) do
          params.merge!(
            services: {
              'dispatcher' => {
                'desc' => 'SAP Dispatcher Ports',
                'ports' => [
                  {
                    'port_pattern' => '32%02d',
                    'protocol' => 'tcp',
                  },
                  {
                    'port_pattern' => '47%02d',
                    'protocol' => 'tcp',
                  },
                ],
              },
            },
            service_map: {},
          )
        end

        it 'compiles with valid parameters' do
          is_expected.to compile.with_all_deps
        end

        it 'fails when missing keys' do
          ['ports', 'desc'].each do |key|
            params[:services][rule_category].delete(key)
            is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': missing mandatory field '#{key}'!})
          end
        end

        it 'fails when desc is not a string' do
          key = 'desc'
          params[:services][rule_category][key] = []
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': field '#{key}' must be of type 'String'!})
        end

        it 'fails when ports is not Array[Sap::FirewallRulePort]' do
          key = 'ports'
          params[:services][rule_category][key] = 'not the right thing'
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': field '#{key}' must be of type 'Array\[Sap::FirewallRulePort\]'!})
        end

        it 'fails when missing required entries in ports' do
          key = 'ports'
          params[:services][rule_category][key] = [{}]
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': each entry in '#{key}' must specify a 'protocol'!})
          params[:services][rule_category][key] = [{ 'protocol' => 'tcp' }]
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': each entry in '#{key}' must specify one of either 'port' or 'port_pattern'!})
          params[:services][rule_category][key] = [
            {
              'protocol' => 'tcp',
              'port' => '7',
              'port_pattern' => '7%02d',
            },
          ]
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': an entry in '#{key}' cannot specify both 'port' and 'port_pattern'!})
        end

        it 'fails when ports entries are of the wrong type' do
          key = 'ports'
          params[:services][rule_category][key] = [
            {
              'protocol' => '7',
              'port' => 'tcp',
            },
          ]
          is_expected.to compile.with_all_deps.and_raise_error(%r{firewall '#{rule_category}': field 'protocol' of each entry in #{key} must be of type 'Enum\['tcp', 'udp'\]'!})
        end
      end

      context 'no local instances' do
        it 'does not create any firewall rules' do
          is_expected.to compile.with_all_deps
          is_expected.to have_firewalld__custom_service_resource_count(0)
          is_expected.to have_firewalld_service_resource_count(0)
        end
      end

      context 'standard application node' do
        before(:each) do
          facts.deep_merge!(
            sap: {
              sid_hash: {
                PRD: {
                  instances: {
                    '02' => {
                      type: 'ERS',
                      class: 'ers',
                    },
                    '01' => {
                      type: 'ASCS',
                      class: 'cs-abap',
                    },
                    '00' => {
                      type: 'DVEBMGS00',
                      class: 'as-abap',
                    },
                  },
                },
                SCP: {
                  instances: {
                    '02' => {
                      type: 'ERS',
                      class: 'ers',
                    },
                    '01' => {
                      type: 'ASCS',
                      class: 'cs-abap',
                    },
                    '00' => {
                      type: 'DVEBMGS00',
                      class: 'as-abap',
                    },
                  },
                },
                'LP0' => {
                  instances: {
                    '12' => {
                      type: 'ERS',
                      class: 'ers',
                    },
                    '14' => {
                      type: 'SCS',
                      class: 'cs-java',
                    },
                    '10' => {
                      type: 'J',
                      class: 'as-java',
                    },
                  },
                },
                GWP: {
                  instances: {
                    '60' => {
                      type: 'G',
                      class: 'gateway',
                    },
                  },
                },
              },
              inst_classes: [
                'ers',
                'cs-abap',
                'cs-java',
                'as-abap',
                'as-java',
                'db-livecache-client',
                'gateway',
                'scmopt',
              ],
            },
          )
        end

        it 'compiles' do
          is_expected.to compile.with_all_deps
        end

        it 'opens the saphostctrl ports' do
          is_expected.to contain_firewalld__custom_service('sap_saphostctrl').with(
            description: 'SAP Host Agent with SOAP/HTTP(S)',
            port: [
              { 'port' => '1128:1129', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Host Agent with SOAP/HTTP(S)').with(
            ensure: 'present',
            service: 'sap_saphostctrl',
            zone: 'public',
          )
        end

        it 'opens the niping port' do
          is_expected.to contain_firewalld__custom_service('sap_niping').with(
            description: 'SAP Connection Tester',
            port: [
              { 'port' => '3298', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Connection Tester').with(
            ensure: 'present',
            service: 'sap_niping',
            zone: 'public',
          )
        end

        it 'creates the relevant SAP firewall rules for each instance' do
          sid_hash = facts[:sap][:sid_hash]
          sid_hash.each do |sid, sid_data|
            instances = sid_data[:instances]
            instances.each do |inst_no, inst_data|
              inst_class = inst_data[:class]
              inst_type = inst_data[:type]
              next unless inst_class_services.key?(inst_class)
              service_list = inst_class_services[inst_class]
              service_list.each do |service_name|
                service = service_definitions[service_name]
                rule_name = "sap_#{sid}_#{inst_type}#{inst_no}_#{service_name}"
                rule_desc = "#{service['desc']} - #{sid} #{inst_type}#{inst_no}"
                ports = []
                service_ports = service['ports']
                service_ports.each do |service_port|
                  port_value = if service_port.key?('port_pattern')
                                 service_port['port_pattern'] % inst_no
                               else
                                 service_port['port']
                               end
                  ports << {
                    'port' => port_value,
                    'protocol' => service_port['protocol'],
                  }
                end

                is_expected.to contain_firewalld__custom_service(rule_name).with(
                  description: rule_desc,
                  port: ports,
                )
                is_expected.to contain_firewalld_service(rule_desc).with(
                  ensure: 'present',
                  service: rule_name,
                  zone: 'public',
                )
              end
            end
          end
        end
      end

      context 'db2 database node' do
        before(:each) do
          facts.deep_merge(
            sap: {
              sid_hash: {
                PRD: {
                  instances: {
                    'database' => {
                      type: 'db2',
                    },
                  },
                },
              },
              inst_classes: [
                'db-db2',
              ],
            },
          )
          params.deep_merge!(
            system_ids: {
              'DEV' => {
                'manage_profiles' => false,
                'components' => [],
                'databases' => {
                  'db-db2' => {
                    'com_port' => 5912,
                    'fcm_base_port' => 5914,
                    'ha_mode' => 'none',
                    'nodes' => [
                      'dev-dba.example.org',
                    ],
                  },
                },
              },
              'PRD' => {
                'manage_profiles' => false,
                'components' => [],
                'databases' => {
                  'db-db2' => {
                    'com_port' => 5922,
                    'fcm_base_port' => 5924,
                    'ha_mode' => 'hadr',
                    'hadr_base_port' => 55_000,
                    'nodes' => [
                      'prd-dba.example.org',
                      'prd-dbb.example.org',
                      'prd-dbc.example.org',
                      'prd-dbd.example.org',
                    ],
                  },
                },
              },
            },
          )
        end

        it 'compiles' do
          is_expected.to compile.with_all_deps
        end

        it 'opens the saphostctrl ports' do
          is_expected.to contain_firewalld__custom_service('sap_saphostctrl').with(
            description: 'SAP Host Agent with SOAP/HTTP(S)',
            port: [
              { 'port' => '1128:1129', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Host Agent with SOAP/HTTP(S)').with(
            ensure: 'present',
            service: 'sap_saphostctrl',
            zone: 'public',
          )
        end

        it 'opens the niping port' do
          is_expected.to contain_firewalld__custom_service('sap_niping').with(
            description: 'SAP Connection Tester',
            port: [
              { 'port' => '3298', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Connection Tester').with(
            ensure: 'present',
            service: 'sap_niping',
            zone: 'public',
          )
        end

        it 'defines the com port' do
          params[:system_ids].each do |sid, sid_data|
            next unless sid_data.key?('databases') && sid_data['databases'].key?('db-db2')
            db2_data = sid_data['databases']['db-db2']
            com_port = db2_data['com_port']
            service_name = "db2_db2#{sid.downcase}_com_port"
            description = "DB2 Communication Port - #{sid}"
            is_expected.to contain_firewalld__custom_service(service_name).with(
              description: description,
              port: [{ 'port' => com_port, 'protocol' => 'tcp' }],
            )
            is_expected.to contain_firewalld_service(description).with(
              ensure: 'present',
              service: service_name,
              zone: 'public',
            )
          end
        end

        it 'defines the baseline FCM ports' do
          params[:system_ids].each do |sid, sid_data|
            next unless sid_data.key?('databases') && sid_data['databases'].key?('db-db2')
            db2_data = sid_data['databases']['db-db2']
            fcm_base_port = Integer(db2_data['fcm_base_port'])
            fcm_last_port = if db2_data['nodes'].length > 1
                              fcm_base_port + db2_data['nodes'].length + 1
                            else
                              fcm_base_port + 3
                            end
            service_name = "db2_db2#{sid.downcase}_fcm"
            description = "DB2 Fast Communication Manager - #{sid}"
            is_expected.to contain_firewalld__custom_service(service_name).with(
              description: description,
              port: [{ 'port' => "#{fcm_base_port}:#{fcm_last_port}", 'protocol' => 'tcp' }],
            )
            is_expected.to contain_firewalld_service(description).with(
              ensure: 'present',
              service: service_name,
              zone: 'public',
            )
          end
        end

        it 'defines the HADR ports' do
          params[:system_ids].each do |sid, sid_data|
            next unless sid_data.key?('databases') && sid_data['databases'].key?('db-db2')
            db2_data = sid_data['databases']['db-db2']
            next unless db2_data['ha_mode'] == 'hadr'

            hadr_base_port = Integer(db2_data['hadr_base_port'])
            hadr_last_port = if db2_data['nodes'].length > 1
                               hadr_base_port + db2_data['nodes'].length - 1
                             else
                               hadr_base_port + 1
                             end

            service_name = "db2_db2#{sid.downcase}_hadr"
            description = "DB2 HADR Communication - #{sid}"
            is_expected.to contain_firewalld__custom_service(service_name).with(
              description: description,
              port: [{ 'port' => "#{hadr_base_port}:#{hadr_last_port}", 'protocol' => 'tcp' }],
            )
            is_expected.to contain_firewalld_service(description).with(
              ensure: 'present',
              service: service_name,
              zone: 'public',
            )
          end
        end
      end

      context 'livecache database server' do
        before(:each) do
          facts.deep_merge(
            sap: {
              sid_hash: {
                SCP: {
                  instances: {
                    'livecache' => {
                      type: 'livecache',
                      class: 'db-livecache',
                      databases: { 'SLP' => true },
                    },
                  },
                },
              },
              inst_classes: [
                'db-livecache',
                'db-livecache-client',
              ],
            },
          )
        end

        it 'compiles' do
          is_expected.to compile.with_all_deps
        end

        it 'opens the saphostctrl ports' do
          is_expected.to contain_firewalld__custom_service('sap_saphostctrl').with(
            description: 'SAP Host Agent with SOAP/HTTP(S)',
            port: [
              { 'port' => '1128:1129', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Host Agent with SOAP/HTTP(S)').with(
            ensure: 'present',
            service: 'sap_saphostctrl',
            zone: 'public',
          )
        end

        it 'opens the niping port' do
          is_expected.to contain_firewalld__custom_service('sap_niping').with(
            description: 'SAP Connection Tester',
            port: [
              { 'port' => '3298', 'protocol' => 'tcp' },
            ],
          )
          is_expected.to contain_firewalld_service('SAP Connection Tester').with(
            ensure: 'present',
            service: 'sap_niping',
            zone: 'public',
          )
        end

        it 'opens the livecache ports' do
          service = service_definitions['livecache']
          service_name = 'sap_livecache'
          is_expected.to contain_firewalld__custom_service(service_name).with(
            description: service['desc'],
            port: service['ports'],
          )
          is_expected.to contain_firewalld_service(service['desc']).with(
            ensure: 'present',
            service: service_name,
            zone: 'public',
          )
        end
      end
    end
  end
end
