type Sap::InstClass = Variant[
  Sap::InstClassSap,
  Sap::InstClassDb,
  Sap::InstClassAux,
]
