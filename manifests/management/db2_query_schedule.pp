# @summary Implementation detail - a query file & associate cron schedule
#
# @param sid
#   System ID for this query
#
# @param query_file_name
#   Basename of the file defining this query. Note that this file must be a bash
#   script which implements the function `execute_query`.
#
# @param query_content
#   URL to the content of the query file which will be deployed.
#
# @param cron_entry
#   Cron schedule entry defining the frequency at which this query will run
#
# @param query_directory
#   Location in which this (and all other) queries will be installed. Note that
#   this is set by the sap::management::db2_queries class
#
# @param schema
#   In some cases the schema name of a given system does not match the SID. This
#   is typically the case for redirected restores in QA environments. If this
#   field is specified it overrides the standard "SAP${sid}" schema name.
#
# @param output_target
#   Specifies the target result file of the scheduled query. Typically this only
#   applies to queries which perform some form of export statement.
#
# @param output_group
#   Operating system group which will be set as the owning group of the
#   output_target file after it is generated.
#
# @param monitor_alias
#   IPv4 address or DNS name representing a service IP that may be present on
#   one or more nodes. In the case of an HA/DR cluster execution of this query
#   will be restricted to the node which currently has this address.
define sap::management::db2_query_schedule (
  Sap::SID $sid,
  String[1] $query_file_name,
  Sap::SourceURL $query_content,
  Sap::ScheduleEntry $cron_entry,
  Stdlib::AbsolutePath $query_directory,
  Optional[Sap::Db2Schema] $schema              = undef,
  Optional[Stdlib::AbsolutePath] $output_target = undef,
  Optional[String[1]] $output_group             = undef,
  Optional[Stdlib::Host] $monitor_alias         = undef,
) {
  $sid_lower = downcase($sid)
  $db2adm = "db2${sid_lower}"

  # Deploy the query source
  $query_file = "${query_directory}/${query_file_name}"
  file { $query_file:
    ensure => 'file',
    owner  => $db2adm,
    group  => "db${sid_lower}adm",
    mode   => '0644',
    source => $query_content,
  }

  # Build the command
  $cmd_base = @("EOT"/L$)
    source "\${HOME}/.profile"; /usr/local/bin/db2_execute_sql \
    -d ${sid} -q ${query_file}
    |-EOT

  # Some queries generate an output file
  if $output_target {
    $cmd_output = " -t ${output_target}"
  } else {
    $cmd_output = ''
  }

  # Some queries generate an output file and need to change the group
  if $output_group {
    $cmd_group = " -g ${output_group}"
  } else {
    $cmd_group = ''
  }

  # Some queries generate an output file and need to change the group
  if $monitor_alias {
    $cmd_alias = " -a ${monitor_alias}"
  } else {
    $cmd_alias = ''
  }

  # Schema override
  if $schema {
    $cmd_schema = " -s ${schema}"
  } else {
    $cmd_schema = ''
  }

  $query_cmd_parts = [
    $cmd_base,
    $cmd_output,
    $cmd_alias,
    $cmd_group,
    $cmd_schema,
  ]
  $query_command = join($query_cmd_parts, '')

  # Schedule the cron job
  cron { "db2_query_schedule_${sid}_${query_file_name}":
    ensure  => present,
    user    => $db2adm,
    command => "${query_command} >/dev/null",
    require => File[$query_file],
    *       => $cron_entry,
  }
}
