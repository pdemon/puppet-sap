type Sap::Secinfo = Hash[
  Enum[
    'MODE', # mandatory
    'TP',   # mandatory
    'USER', # mandatory
    'HOST', # mandatory
    'USER-HOST',
  ],
  Variant[
    Sap::SecRuleMode,
    Sap::TargetProgram,
    Sap::SecUser,
    Array[Sap::HostRuleEntry],
  ],
]
