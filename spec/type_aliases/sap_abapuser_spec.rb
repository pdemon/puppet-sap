require 'spec_helper'

describe 'Sap::ABAPUser' do
  it 'accepts strings that are less than 12 characters' do
    is_expected.to allow_value('BG_EDI', 'BATMAN', 'SOME_USER', 'SOME9*', 'SAP*')
  end

  it 'rejects strings longer than 12 charcters' do
    is_expected.not_to allow_value('ABC0123456789')
  end

  it 'rejects strings which include lowercase characters' do
    is_expected.not_to allow_value('a00', 'cPt', '03p', 'Batmn03')
  end
end
