require 'spec_helper'

describe 'Sap::SID' do
  it 'accepts strings that are exactly 3 characters' do
    is_expected.to allow_value('A00')
  end

  it 'rejects strings longer than 3 charcters' do
    is_expected.not_to allow_value('A000')
  end

  it 'rejects strings shorter than 3 charcters' do
    is_expected.not_to allow_value('AB')
  end

  it 'rejects strings which include lowercase and special characters' do
    is_expected.not_to allow_value('a00', 'cPt', '03p', '03_')
  end
end
