# @summary Deploys font and XDC files to ADS systems
#
# @param system_ids
#   Sourced directly from the parent system_ids parameter on the SAP class. See
#   that class for further detail.
# 
# @param deployed_fonts
#   Hash with a list of fonts to deploy and their source locations. Each file in
#   this structure will be installed in 
#   `/usr/sap/${SAPSYSTEMNAME}/SYS/global/AdobeDocumentServices/FontManagerService/fonts/customer/`
#   for each local SID with the ADS component enabled.
#
# @param deployed_xdc
#   Hash with a list of xdc files to deploy and their source locations. Each file in
#   this structure will be installed in 
#   `/usr/sap/${sid}/SYS/global/AdobeDocumentServices/lib/XDC/Customer/`
#   for each local SID with the ADS component enabled.
class sap::management::ads_fonts (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids = $sap::system_ids,
  Sap::SourcedFile $deployed_fonts                = $sap::deployed_fonts,
  Sap::SourcedFile $deployed_xdc                  = $sap::deployed_xdc,
) {
  # Loop through all sids
  $system_ids.each |$sid, $sid_data| {
    $sid_lower = downcase($sid)

    # Skip non ADS systems
    unless('ads' in $sid_data['components']) { next() }

    # Deploy requested font
    $deployed_fonts.each |$font_file, $font_file_source| {
      file { "/usr/sap/${sid}/SYS/global/AdobeDocumentServices/FontManagerService/fonts/customer/${font_file}":
        ensure => 'file',
        source => $font_file_source,
        mode   => '0644',
        owner  => "${sid_lower}adm",
        group  => 'sapsys',
      }
    }

    # Deploy requested xdc files
    $deployed_xdc.each |$xdc_file, $xdc_file_source| {
      file { "/usr/sap/${sid}/SYS/global/AdobeDocumentServices/lib/XDC/Customer/${xdc_file}":
        ensure => 'file',
        source => $xdc_file_source,
        mode   => '0644',
        owner  => "${sid_lower}adm",
        group  => 'sapsys',
      }
    }
  }
}
