# General parameter definitions for the SAP Netweaver environment are defined
# within this module. Note that OS and Architecture specific variants are
# actually pulled from defaults in Hiera.
#
# @summary This module contains the parameters for SAP Netweaver
#
# @param config_redhat_release_conf
#   This should point to the path of the desired redhat release config file.
#
# @param service_uuidd
#   Name of the uuidd service. This is used to enable the daemon after the
#   installation completes.
#
# @param service_scc
#   SAP Cloud Connector service name used for enabling the service.
#
# @param service_saprouter
#   Name of the service corresponding to the SAP router process.
#
# @param config_saproutetab
#   File target for the sap router route-table should it be configure.
#
# @param config_saproutetab_template
#   Path within the module to the appropriate erb template to apply for the
#   saproutetab.
#
# @param config_saprouter_sysconfig
#   Target /etc/sysconfig file for the saprouter service
#   
# @param config_saprouter_sysconfig_template
#   Embedded ruby template to be applied to the sysconfig file
#
# @param config_sysctl
#   Similar to config_limits this provides a configurable set of directives to
#   include in /etc/sysctl.d/ on a per component basis.
#
# @param config_limits
#   A deeply nested hash containing the target /etc/security/limits.d/
#   entries for a given system. This is currently defined for baseline sap
#   instances and db2. The hash has the following structure
#   
#   ```puppet
#   'component-name' => {
#     path           => '/etc/security/limits.d', # this should be left alone - limits directory
#     sequence       => '00', # string ID prepended to the limits file. e.g.
#                             # /etc/security/limits.d/00-sap-base.conf
#     per_sid        => true, # Indicates whether the domain has a _sid_
#                             # substring that will need to be substituted with the actual system ID.
#     header_comment => [ # An array of comment lines which will appear at the
#                         #start of the file
#       'Comment line 1',
#       'Comment line 2',
#     ],
#     entries => { # Hash containing the actual 
#     }
#   ```
# 
# @param config_default_mount_options
#   Default options to provide to provide for the mount operation in the case
#   that they are not overridden locally. See 
#   https://puppet.com/docs/puppet/latest/types/mount.html#mount-attribute-options
#   for details. Note that this is a hash of hashes with an entry for each
#   supported resource type.
class sap::params (
  String $config_redhat_release_conf                          = undef,
  String $config_saproutetab                                  = undef,
  String $config_saproutetab_template                         = undef,
  String $config_saprouter_sysconfig                          = undef,
  String $config_saprouter_sysconfig_template                 = undef,
  String $service_uuidd                                       = undef,
  String $service_scc                                         = undef,
  String $service_saprouter                                   = undef,
  Hash[
    String,
    Hash[
      String,
      Variant[
        String,
        Integer,
        Array[String],
        Hash[
          String,
          Variant[
            Hash[
              String,
              Variant[
                String,
                Integer,
                Array[
                  Variant[
                    String,
                    Hash[
                      String,
                      Variant[
                        String,
                        Integer,
                      ]
                    ],
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]
  ] $config_sysctl = {},
  Hash[
    String,
    Hash[
      String,
      Variant[
        String,
        Boolean,
        Array[String],
        Hash[
          String,
          Hash[
            String,
            Hash[
              String,
              Variant[
                String,
                Integer,
              ]
            ]
          ]
        ]
      ]
    ]
  ] $config_limits = {},
  Hash[
    String,
    Hash[
      String,
      String,
    ]
  ] $config_default_mount_options            = {},
) {
  # These components depend on 'base' and 'base_extendend'
  $requires_base = ['base_extended', 'bo', 'ads', 'hana']
  $requires_base_extended = ['bo', 'ads', 'hana']

  # RHEL 7 only componetns
  $rhel7_components = ['bo', 'cloudconnector', 'hana']
}
