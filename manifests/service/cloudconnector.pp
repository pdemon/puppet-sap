# @summary SAP Cloud Connector service configuration
class sap::service::cloudconnector {
  # SAP Cloud Connector services
  service { $sap::params::service_scc:
    ensure => 'running',
    enable => true,
  }
}
