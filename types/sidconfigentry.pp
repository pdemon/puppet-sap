# @summary Configuration entry for a SID
#
# @param components
#   A list of associated sap applications to enable for this SID (mandatory).
#
# @param backend_databases
#   Types of databases to which this system connects (mandatory for SIDs with 'base' enabled)
#
# @param manage_profiles
#   Flag indicating whether the instance profiles for this SID should be
#   managed. When true, `instances`, `databases`, and `primary_backend_db`
#   become mandatory parameters!
#
# @param manage_default_profiles
#   Flag indicating whether DEFAULT.PFL for this SID should be managed. When
#   true, it is created, otherwise it is left absent.
#
# @param exclude_common_mount_points
#   Flag indicating if this SID should skip SID specific entries for the
#   'common' mountpoint. Some system types do not have /sapmnt directories
#   (e.g. a DAA instance or liveCache).
#
# @param pattern_map
#   A hash mapping pattern lookup keys to regular expressions which are
#   unique to this SID. Note that if one is added it must have a
#   corresponding value added in `pattern_values`.
#
# @param pattern_values
#   Mapping from a pattern lookup key to the value which should be
#   provided wherever the corresponding pattern is found.
#
# @param instances
#   Hash detailing the instances which are part of this SID. Entries
#   are keyed by `instance class` and contain the corresponding
#   [Sap::SidInstance](#sapsidinstance) detail.
#
# @param databases
#   Hash detailing the database parameters for this SID. Entries in
#   this hash are keyed by `instance class` and contain the parameters
#   described by [Sap::SidDatabase](#sapsiddatabase).
#
# @param primary_backend_db
#   Specifies the primary database backend used by the application
#   servers in this instance. This should map to an entry in `databases'
#
# @param default_pfl_entries
#   Hash of profile keys and the desired value which will be appended
#   to the end of the DEFAULT.PFL file for this instance. Currently not validated.
#
# @param sec_info_rules
#   Array of Sap::SecInfo entries to be included in the secinfo rule
#   file for this SID
#
# @param reg_info_rules
#   Array of Sap::RegInfo entries to be included in the reginfo rule
#   file for this SID.
#
# @param db2_query_schedules
#   Array of [Sap::Db2QuerySchedule](#sapdb2queryschedule) entries to be deployed
#   on each database node of this SID.
type Sap::SIDConfigEntry = Struct[
  components                            => Array[Sap::SapComponent],
  manage_profiles                       => Boolean,
  Optional[manage_default_profile]      => Boolean,
  Optional[backend_databases]           => Array[Sap::BackendDatabase],
  Optional[exclude_common_mount_points] => Boolean,
  Optional[pattern_map]                 => Hash[String, String],
  Optional[pattern_values]              => Hash[String, String],
  Optional[instances]                   => Hash[Sap::InstClassSap, Sap::SIDInstance],
  Optional[databases]                   => Hash[Sap::InstClassDb, Sap::SIDDatabase],
  Optional[primary_backend_db]          => Variant[Sap::InstClassDb, Enum['none']],
  Optional[default_pfl_entries]         => Hash[String, String],
  Optional[sec_info_rules]              => Array[Sap::SecInfo],
  Optional[reg_info_rules]              => Array[Sap::RegInfo],
  Optional[db2_query_schedules]         => Array[Sap::Db2QuerySchedule],
]
