require 'spec_helper'
require 'deep_merge'

describe 'sap::config::mount_point' do
  let(:params) do
    {
      per_sid: false,
      pattern_map: {},
      pattern_values: {},
      file_params: {
        owner: 'root',
        group: 'root',
        mode: '0755',
      },
    }
  end

  context 'missing mount type' do
    let(:title) { '/missing/mount/type' }

    before(:each) do
      params.merge!(
        mount_params: {
          managed: true,
          garbage: 'somegarbage',
        },
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: missing mount type for path '#{title}'!}) }
  end

  context 'invalid mount type' do
    let(:title) { '/bad/type' }

    before(:each) do
      params.merge!(
        mount_params: {
          managed: true,
          type: 'cool-bad-type',
        },
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: invalid mount type 'cool-bad-type' for path '#{title}'!}) }
  end

  context 'count missing substring' do
    let(:title) { '/missing/count/pattern' }

    before(:each) do
      params.merge!(
        count: 7,
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' specifies \$count but does not contain '_N_'!}) }
  end

  context 'unsupported file ensure type' do
    let(:title) { '/some/weird/file' }

    before(:each) do
      params.deep_merge!(
        file_params: {
          ensure: 'file',
        },
      )
    end

    it 'compiles and raises error' do
      is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' specifies unsupported 'ensure' value 'file'!})
    end
  end

  context 'link file type missing target' do
    let(:title) { '/some/link/file' }

    before(:each) do
      params.deep_merge!(
        file_params: {
          ensure: 'link',
        },
      )
    end

    it 'compiles and raises error' do
      is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' with file_params 'ensure' set to 'link' must also include 'target' parameter!})
    end
  end

  context 'count too small' do
    let(:title) { '/small/count/_N_' }

    before(:each) do
      params.merge!(
        mount_path: '/small/count/_N_',
        count: 0,
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' specifies \$count not >= 1!}) }
  end

  context 'sid specific' do
    let(:title) { '/sapmnt/_SID_' }

    before(:each) do
      params.merge!(
        per_sid: true,
        pattern_map: {
          sid_upper: '_SID_',
          sid_lower: '_sid_',
        },
        pattern_values: {
          sid_lower: 'ecp',
          sid_upper: 'ECP',
        },
      )
    end

    context 'pattern map missing sid_lower' do
      before(:each) do
        params.merge!(
          pattern_map: {
            sid_upper: '_SID_',
          },
          pattern_values: {
            sid_lower: 'ecp',
            sid_upper: 'ECP',
          },
        )
      end

      it 'errors' do
        is_expected.to compile.with_all_deps.and_raise_error(
          %r{mount_point: '#{title}' with per_sid mode '#{params[:per_sid]}' must include sid_upper and sid_lower entries in both [$]pattern_map and [$]pattern_values!},
        )
      end
    end

    context 'pattern map missing sid_upper' do
      before(:each) do
        params.merge!(
          pattern_map: {
            sid_upper: '_sid_',
          },
          pattern_values: {
            sid_lower: 'ecp',
            sid_upper: 'ECP',
          },
        )
      end

      it 'errors' do
        is_expected.to compile.with_all_deps.and_raise_error(
          %r{mount_point: '#{title}' with per_sid mode '#{params[:per_sid]}' must include sid_upper and sid_lower entries in both [$]pattern_map and [$]pattern_values!},
        )
      end
    end

    context 'pattern values missing sid_lower' do
      before(:each) do
        params.merge!(
          pattern_map: {
            sid_upper: '_SID_',
            sid_lower: '_sid_',
          },
          pattern_values: {
            sid_upper: 'ECP',
          },
        )
      end

      it 'errors' do
        is_expected.to compile.with_all_deps.and_raise_error(
          %r{mount_point: '#{title}' with per_sid mode '#{params[:per_sid]}' must include sid_upper and sid_lower entries in both [$]pattern_map and [$]pattern_values!},
        )
      end
    end

    context 'pattern values missing sid_upper' do
      before(:each) do
        params.merge!(
          pattern_map: {
            sid_upper: '_SID_',
            sid_lower: '_sid_',
          },
          pattern_values: {
            sid_lower: 'ecp',
          },
        )
      end

      it 'errors' do
        is_expected.to compile.with_all_deps.and_raise_error(
          %r{mount_point: '#{title}' with per_sid mode '#{params[:per_sid]}' must include sid_upper and sid_lower entries in both [$]pattern_map and [$]pattern_values!},
        )
      end
    end

    context 'path does not match sid pattern' do
      let(:title) { '/missing/sid/pattern' }

      it 'errors' do
        is_expected.to compile.with_all_deps.and_raise_error(
          %r{mount_point: SID specified for '#{title}' but does not match '/_SID_/' or '/_sid_/'!},
        )
      end
    end

    context 'nfsv4 missing server' do
      before(:each) do
        params.merge!(
          mount_params: {
            managed: true,
            type: 'nfsv4',
          },
        )
      end

      it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' type 'nfsv4' missing 'server'!}) }
    end

    context 'nfsv4 missing share' do
      before(:each) do
        params.merge!(
          mount_params: {
            managed: true,
            type: 'nfsv4',
            server: 'nfs.example.org',
          },
        )
      end

      it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '#{title}' type 'nfsv4' missing 'share'!}) }
    end

    context 'with valid arguments' do
      let(:title) { '/sapmnt/_SID_' }

      before(:each) do
        params.merge!(
          file_params: {
            owner: '_sid_adm',
            group: 'sapsys',
            mode: '0755',
          },
        )
      end

      it { is_expected.to compile.with_all_deps }

      it {
        is_expected.to contain_file('/sapmnt/ECP').with(
          ensure: 'directory',
          owner: 'ecpadm',
          group: 'sapsys',
          mode: '0755',
        )
      }
    end

    context 'with count' do
      let(:title) { '/db2/_SID_/sapdata_N_' }

      before(:each) do
        params.merge!(
          per_sid: true,
          count: 4,
          file_params: {
            owner: 'db2_sid_',
            group: 'db_sid_adm',
            mode: '0750',
          },
        )
      end

      it { is_expected.to compile.with_all_deps }

      it {
        is_expected.to contain_file('/db2/ECP/sapdata1').with(
          ensure: 'directory',
          owner: 'db2ecp',
          group: 'dbecpadm',
          mode: '0750',
        )
        is_expected.to contain_file('/db2/ECP/sapdata2').with(
          ensure: 'directory',
          owner: 'db2ecp',
          group: 'dbecpadm',
          mode: '0750',
        )
        is_expected.to contain_file('/db2/ECP/sapdata3').with(
          ensure: 'directory',
          owner: 'db2ecp',
          group: 'dbecpadm',
          mode: '0750',
        )
        is_expected.to contain_file('/db2/ECP/sapdata4').with(
          ensure: 'directory',
          owner: 'db2ecp',
          group: 'dbecpadm',
          mode: '0750',
        )
      }
    end
  end

  test_on = {
    hardwaremodels: ['x86_64', 'ppc64'],
    supported_os: [
      {
        'operatingsystem' => 'RedHat',
        'operatingsystemrelease' => ['7'],
      },
    ],
  }

  on_supported_os(test_on).each do |os, facts|
    context "on os #{os} sid specific nfs mount" do
      let(:title) { '/sapmnt/_SID_' }
      let(:facts) { facts }
      let(:pre_condition) do
        'class { nfs:
          server_enabled => false,
          client_enabled => true,
          nfs_v4_client => true,
          nfs_v4_idmap_domain => "example.com",
        }'
      end

      before(:each) do
        params.merge!(
          per_sid: true,
          pattern_map: {
            sid_upper: '_SID_',
            sid_lower: '_sid_',
          },
          pattern_values: {
            sid_lower: 'ecp',
            sid_upper: 'ECP',
          },
          file_params: {
            owner: '_sid_adm',
            group: 'sapsys',
            mode: '0755',
          },
          mount_params: {
            managed: true,
            type: 'nfsv4',
            share: '/srv/nfs/sapmnt/_SID_',
            server: 'nfs.example.com',
          },
          mount_defaults: {
            nfsv4: {
              options: 'rw,soft,noac,timeo=200,retans=3,proto=tcp',
            },
          },
        )
      end

      it { is_expected.to compile.with_all_deps }

      it {
        is_expected.to contain_nfs__client__mount('/sapmnt/ECP').with(
          ensure: 'mounted',
          server: 'nfs.example.com',
          share: '/srv/nfs/sapmnt/ECP',
          atboot: true,
          options_nfsv4: 'rw,soft,noac,timeo=200,retans=3,proto=tcp',
          owner: 'ecpadm',
          group: 'sapsys',
          mode: '0755',
        )
      }
    end
  end
end
