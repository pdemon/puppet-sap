# Higher level model for linux high-availability rules which captures the type
# and any subcomponents of each component
type Sap::ClusterComponent = Hash[
  Variant[Enum['type','data']],
  Variant[
    Sap::ClusterComponentType,
    Sap::ClusterScsErs,
    Sap::ClusterAs,
    Sap::ClusterLvm,
    Sap::ClusterDb2,
  ]
]
