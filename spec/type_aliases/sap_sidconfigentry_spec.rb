require 'spec_helper'

describe 'Sap::SIDConfigEntry' do
  it 'accepts fully configured SID entries' do
    is_expected.to allow_value(
      {
        manage_profiles: true,
        components: [
          'base',
        ],
        primary_backend_db: 'db-db2',
        backend_databases: [
          'db6',
          'liveCache',
        ],
        pattern_values: {
          lcid_upper: 'SLP',
        },
        databases: {
          'db-db2': {
            com_port: 5_912,
            fcm_base_port: 5_914,
            ha_mode: 'hadr',
            hadr_base_port: 55_000,
            host: 'scp-a-db',
            nodes: [
              'scp-a-dba.sap.arden',
              'scp-a-dbb.sap.arden',
            ],
          },
        },
        instances: {
          'as-abap': {
            number: '40',
            types: {
              DVEBMGS: [ 'scp-a-app00.sap.arden' ],
              D: [ 'scp-a-app01.sap.arden' ],
            },
            legacy_start_profile: true,
          },
          'cs-abap': {
            number: '41',
            types: {
              ASCS: [
                'scp-a-app00.sap.arden',
                'scp-a-app01.sap.arden',
              ],
            },
            host: 'scp-a-cs',
            legacy_start_profile: true,
          },
          ers: {
            number: '42',
            types: {
              ERS: [
                'scp-a-app00.sap.arden',
                'scp-a-app01.sap.arden',
              ],
            },
            host: 'scp-a-ers',
            legacy_start_profile: true,
          },
        },
        default_pfl_entries: {
          'rec/client': 'ALL',
          'rdisp/max_wprun_time': '7_200',
          'login/system_client': '400',
        },
      },
    )
  end

  it 'rejects entries missing manage_profiles' do
    is_expected.not_to allow_value(
      {
        components: [],
        exclude_common_mount_points: false,
      },
    )
  end

  it 'rejects entries missing components' do
    is_expected.not_to allow_value(
      {
        manage_profiles: false,
        exclude_common_mount_points: false,
      },
    )
  end

  it 'rejects invalid backend_databases' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        backend_databases: [
          false,
        ],
      },
    )
  end

  it 'rejects invalid exclude_common_mount_points' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        exclude_common_mount_points: {},
      },
    )
  end

  it 'rejects invalid pattern_map' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        pattern_map: [
          'someEntry',
        ],
      },
    )
  end

  it 'rejects invalid pattern_values' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        pattern_values: [
          'someEntry',
        ],
      },
    )
  end

  it 'rejects invalid instances' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        instances: {
          'Batman' => false,
        },
      },
    )
  end

  it 'rejects invalid databases' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        databases: {
          'Batman' => false,
        },
      },
    )
  end

  it 'rejects invalid primary_backend_db' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: false,
        primary_backend_db: false,
      },
    )
  end

  it 'rejects invalid manage_profiles' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: 'foo',
      },
    )
  end

  it 'rejects invalid default_pfl_entries' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: true,
        default_pfl_entries: {
          'entry' => {},
        },
      },
    )
  end

  it 'rejects invalid sec_info_rules' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: true,
        sec_info_rules: {
          'entry' => {},
        },
      },
    )
  end

  it 'rejects invalid reg_info_rules' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: true,
        reg_info_rules: {
          'entry' => {},
        },
      },
    )
  end

  it 'rejects invalid db2_query_schedules' do
    is_expected.not_to allow_value(
      {
        components: [],
        manage_profiles: true,
        db2_query_schedules: {
          'entry' => {},
        },
      },
    )
  end
end
