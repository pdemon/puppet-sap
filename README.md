# Sap

[![pipeline status](https://gitlab.com/pdemon/puppet-sap/badges/master/pipeline.svg)](https://gitlab.com/pdemon/puppet-sap/commits/master)
[![Version](https://img.shields.io/puppetforge/v/pdemon/sap.svg)](https://forge.puppet.com/pdemon/sap)
[![coverage report](https://gitlab.com/pdemon/puppet-sap/badges/master/coverage.svg)](https://gitlab.com/pdemon/puppet-sap/commits/master)

#### Table of Contents

1. [Overview](#overview)
1. [Module Description - What the module does and why it is useful](#module-description)
1. [Setup - The basics of getting started with sap](#setup)
    * [What sap affects](#what-sap-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with sap](#beginning-with-sap)
1. [Usage](#usage)
1. [Reference](#reference)
  1. [Data Types](#data-types)
1. [Limitations](#limitations)
1. [Development](#development)
1. [Contributors](#contributors)


## Overview

The sap module provides packages, settings and other requirements for installing
specific SAP products on this node.

## Module Description

This module takes care that the SAP recommendations for installing specific SAP
products on the Linux operating system are fulfilled on the current node. The
configuration is based on the corresponding SAP OSS notes describing the necessary
steps.

**Keep in mind, this module is _not_ related in any way to official SAP software
distribution nor is it supported in any way by official SAP contracts. This is
my private module to speed up the installation of SAP software in a controlled
puppetized way, so absolutely no warranty at all!**


## Setup

### What sap affects

* local packages
* local configuration files
* local service configurations

### Beginning with sap

There are two mandatory parameters for the environment:
* `enabled_components` - An array of target components that are enabled on this host. 
* `system_ids` - An array of the SIDs which will be present on this machine

The following declaration will enable baseline packages for an SAP instance named 'PR0':

```puppet
class { 'sap':
  system_ids => {'PR0'
    components        => ['base'],
    backend_databases => ['db6'],
  },
}
```

Alternatively these values can also be set in hiera:

```yaml
---
sap::system_ids:
  - PR0
    components:
    - 'base'
    backend_databases:
    - 'db6'
```

## Usage

This module is broken up into several classes which apply functionality to different stages of an SAP install. 

* **sap** - Central class which brings in all pre-install dependencies, defines the SAP facts, and manages mount-points
* **sap::management** - Configures an installed SAP instance including user profile changes, sap profile definitions, and other OS level application setup

All interaction with the sap module should be done through the classes listed above. Note that the base 'sap' class must be configured for the other parent classes to function.

## Reference

See the [references](./REFERENCES.md) file.

### Data Types

#### `Sap::SIDConfigEntry`

* `components` - a list of associated sap applications to enable for this SID (mandatory).
* `backend_databases` - types of databases to which this system connects (mandatory for SIDs with 'base' enabled)
* `manage_profiles` - Flag indicating whether the instance profiles for this SID should be managed. When true, `instances`, `databases`, and `primary_backend_db` become mandatory parameters!
* `manage_default_profile` - Flag indicating whether DEFAULT.PFL for this SID should be managed. When true, it is created, otherwise it is left absent.
* `exclude_common_mount_points` - flag indicating if this SID should skip SID specific entries for the 'common' mountpoint. Some system types do not have /sapmnt directories (e.g. a DAA instance or liveCache)
* `pattern_map` - A hash mapping pattern lookup keys to regular expressions which are unique to this SID. Note that if one is added it must have a corresponding value added in `pattern_values`.
* `pattern_values` - Mapping from a pattern lookup key to the value which should be provided wherever the corresponding pattern is found.
* `instances` - Hash detailing the instances which are part of this SID. Entries are keyed by `instance class` and contain the corresponding [Sap::SidInstance](#sapsidinstance) detail.
* `databases` - Hash detailing the database parameters for this SID. Entries in this hash are keyed by `instance class` and contain the parameters described by [Sap::SidDatabase](#sapsiddatabase).
* `primary_backend_db` - Specifies the primary database backend used by the application servers in this instance. This should map to an entry in `databases'
* `default_pfl_entries` - Hash of profile keys and the desired value which will be appended to the end of the DEFAULT.PFL file for this instance. Currently not validated.
* `sec_info_rules` - Array of Sap::SecInfo entries to be included in the secinfo rule file for this SID
* `reg_info_rules` - Array of Sap::RegInfo entries to be included in the reginfo rule file for this SID
* `db2_query_schedules` - Array of [Sap::Db2QuerySchedule](#sapdb2queryschedule) entries to be deployed on each database node of this SID.

#### `Sap::MessageServer`

A set of parameters defining the attributes required to construct the communication parameters for a given message server. Each entry corresponds to a single SID and is comprised of the following fields:

* `class` - Indication of the type of message server. Currently limited to the values described in [Sap::InstClassSapCs](SapInstClassSapCs). This is used to determine the range from which this message server port is allocated.
* `number` - Instance number corresponding to the Central Services component of this SID.
* `port_override` - Optional override field allowing an explicit port to be specified instead of the normal calculated port. This should be avoided.

Acceptable input example:

```puppet
{
  'class'  => 'cs-abap',
  'number' => '02',
}
```

#### `Sap::SIDDatabase`

Hash describing an instance which is part of a given SAP SID. For example, a DB2 database instance.

Each entry has four possible keys:
* `base_port` - The TCP/IP port used as a starting point for configuring this instance. It will be added to the `com_port` to determine the first communication port for this database.
* `com_port` - The TCP/IP port used to by clients to connect to this database instance.
* `ha_mode` - Indicates the HA mode used by this database. Valid values are described by [Sap::DbHaMode](#sapdbhamode)
* `nodes` - An array containing the trusted cert names of each node that is part of this database cluster.

Acceptable input example:

```puppet
{
  'base_port' => '5900',
  'com_port'  => '5912',
  'ha_mode'   => 'hadr',
  'nodes'     => [
    'prd-dba.example.org',
    'prd-dbb.example.org',
  ],
}
```

#### `Sap::SIDInstance`

Hash describing an instance which is part of a given SAP SID. For example, an ABAP Application Server.

Each entry has four possible keys:
* `number` - The [Sap::InstNumber](#sapinstnumber) which corresponds to this instance.
* `types` - A hash mapping the [Sap::InstType](#sapinsttype) to an array of hosts on which the given instance is present. Note that there may be different names for the same kind of instance.
* `host` - Instances which can move between hosts have a virtual host name or service IP which facilitates this process. This field specifies the name used to install this instance.
* `legacy_start_profile` - Older versions of SAP netweaver have separate `START` profiles which handle initialization. See sapnote 1751381 for detail. If unspecified this defaults to `false`.

Acceptable input example:

```puppet
{
  'number' => '00',
  'types'  => {
    'D'       => [ 'node2.example.com', 'node3.example.com' ],
    'DVEBMGS' => [ 'node1.example.com' ],
  },
  'legacy_start_profile' => true,
}
```

Alternate example describing a Central Services instance with a virtual hostname.

```puppet
{
  'number' => '01',
  'types'  => {
    'ASCS' => [ 'node1.example.com', 'node2.example.com' ],
  },
  'legacy_start_profile' => true,
}
```


#### `Sap::InstNumber`

A two digit number assigned to a given instance within an SID. For example, an SAP instance named `ASCS01` the InstNumber is `01`.

Acceptable input:

```shell
01

10

99
```

These must not be reused within an SID for different purposes.

#### `Sap::SecRuleMode`

A Indicates whether a given entry in sec_info or reg_info corresponds to a 'permit' or 'deny'. Only `P` and `D` are valid.

#### `Sap::TargetProgram`

Component of a sec_info or reg_info entry corresponding to a given target executable. Accepts only ANSI printable strings 64 characters in length or less. A wildcard may be included, however, it must appear at the end of the string.

Examples:
```shell
*            # true
/bin/*       # true
/bin/rsync   # true
*sync        # false
/bin/*sap    # false
```

#### `Sap::HostRuleEntry`

Entry used in the HOST, ACCESS, CANCEL, and USER-HOST components of sec_info and reg_info files. Entries can include any of the following
* `*` - A full wildcard - don't put anything else if you have this one!
* `prd-app00` - A valid hostname
* `prd-app00.example.org` - A valid fully qualified hostname
* `*.example.org` - A wildcard domain name - note that it is unclear whether subdomains are accepted
* `192.168.0.15` - An IPv4 address without CIDR/Netmask
* `192.168.0.*` - A wildcard can be substituted to catch all hosts in a /24 network. This can be extended for /16 and /8, however, a wildcard must be present at each level. E.g. `192.168.*.*` or `10.*.*.*`

#### `Sap::SecUser`

Represents a single user (or a wildcard user) for a rule entry in the sec_info file on a Netweaver application server. Single user names up to 11 characters in length are accepted *or* a `*` can be specified to allow any user.

Examples:
```shell
*                 # true
maxusername       # true
SAPUSER           # true
toolongusername   # false
sap*              # false
sapuser1,sapuser2 # false
```

#### `Sap::SecInfo`

A rule specifying which remote users are allowed to execute a given command or command pattern on a list of target hosts from a list of remote hosts. See [Sap Note 614971](https://launchpad.support.sap.com/#/notes/614971) for further details.

* `MODE` - is this a permit or deny rule?
* `TP` - the program which is attempting to execute. See [Sap::TargetProgram](#saptargetprogram) for detail.
* `USER` - which specific user is allowed to run this?. See [Sap::SecUser](#sapsecuser) for detail.
* `HOST` - An array of hosts on which TP might execute. See [Sap::HostRuleEntry](#saphostruleentry) for detail.
* `USER-HOST` - An array of hosts from which USER might remotely connect to HOST. See [Sap::HostRuleEntry](#saphostruleentry) for detail.

#### `Sap::RegInfo`

A rule specifying which target programs are allowed to register on what hosts. Additionally, the rule may constrain the remote hosts which can start or stop the target program. See [Sap Note 1069911](https://launchpad.support.sap.com/#/notes/1069911) for further details.

* `MODE` - is this a permit or deny rule?
* `TP` - the program which is attempting to execute. See [Sap::TargetProgram](#saptargetprogram) for detail.
* `HOST` - An array of hosts on which TP might execute. See [Sap::HostRuleEntry](#saphostruleentry) for detail.
* `ACCESS` - An array of hosts which may remotely connect to start TP on HOST. See [Sap::HostRuleEntry](#saphostruleentry) for detail.
* `CANCEL` - An array of hosts which may remotely connect to stop TP on HOST. See [Sap::HostRuleEntry](#saphostruleentry) for detail.

#### `Sap::Db2Schema`

Simple pattern matching type which enforces sane detection of standard SAP DB2 database schemas. These schemas always have the form `SAP${sid}`. For example, a system with SID `ECP` would have a schema named `SAPECP`.

Examples:
```shell
SAPEQ0            # true
SAPSCP            # true
SAPPRD            # true
SAPTOOLONG        # false
NOTSAP            # false
sapecp            # false
```

#### `Sap::Db2QuerySchedule`

Details a given DB2 query script along with it's execution frequency. This script assumes the schema matches.

* `query_file_name` - basename of the file which defines the query.
* `query_content` - URL pointing to the source file of the query which will be deployed.  See [shipment_history_extraction](./files/shipment_history_extraction) for implementation reference.
* `schema` - Optional override of the standard `SAP${sid}` schema name. See [Sap::Db2Schema](#sapdb2schema) for detail.
* `cron_entry` - A hash detailing the scheduling parameters for the cron job associated with this query. See [cron_core](https://forge.puppet.com/puppetlabs/cron_core/reference) for detail
* `output_target` - Absolute path used as the output file target by the query associated with this schedule entry.
* `output_group` - Ownership of the output_target is changed to this group after it is created.
* `monitor_alias` - Virtual IP or associated DNS name used to restrict execution in clustered environments. The query will only be executed if this entry is present.

#### `Sap::ClusterFenceDataLpar`

Details the arguments used to configure a fence_lpar STONITH agent.

* `ipaddr` - IP address or hostname of the fencing HMC
* `login` - Login username for the HMC
* `managed` - Name of the CEC or managed system which hosts the target LPAR(s)
* `identity_file` - Private key file used to connect to the HMC
* `pcmk_delay_max` - a random maximum delay for stonith actions to prevent double fencing
* `pcmk_host_map` - mapping of trusted host names to the LPAR definitions in the HMC

Note that the `managed` frames can be listed from the HMC shell via `lssyscfg -r sys -F name`

Actual LPAR names used within the `pcmk_host_map` parameter can be listed via `lssyscfg -m host -r lpar -F name | sort` where host is the name of a managed system.

```bash
# in the HMC rshell
for HOST in $(lssyscfg -r sys -F name); do echo $HOST; lssyscfg -r lpar -m $HOST -F name; echo ''; done
```

### SAP Base Class

#### I just want sap, what's the minimum I need

Declare your SID(s) and the 'base' component in hiera:

```yaml
---
sap::system_ids:
  - PR0
    components:
    - 'base'
    backend_databases:
    - 'db6'
```

Within your manifest include the SAP profile.

```puppet
include 'sap'
```

#### I just want ABAP stack, JAVA stack and ADS on target node.

This example configures the total pre-requisite stack for ADS.

```puppet
class { 'sap':
  system_ids         => ['AP0'],
  enabled_components => [
    'base',
    'base_extended',
    'ads'
  ],
}
class { 'sap':
  system_ids => {'AP0'
    components        => [
      'base',
      'base_extended',
      'ads',
    ],
    backend_databases => ['db6'],
  },
}
```

### SAP Clustering

Full cluster with fencing. Note that this is in a PowerVM environment.

```puppet
class { 'sap::cluster':
  enable_fencing => true,
  sid_rules      => {
    PR0 => {
      node_roles => {
        'pr0-app00.example.org' => [ 'app-standard' ],
        'pr0-app01.example.org' => [ 'app-standard' ],
        'pr0-dba.example.org'   => [ 'db-standard' ],
        'pr0-dbb.example.org'   => [ 'db-standard' ],
      },
      components => [
        {
          type => 'ScsErs',
          data => {
            ers_ip   => '10.0.0.10',
            ers_inst => {
              type   => 'ERS',
              number => '02',
              host   => 'prd0-ers.example.org',
            },
            scs_ip   => '10.0.0.11',
            scs_inst => {
              type   => 'ASCS',
              number => '01',
              host   => 'prd0-scs.example.org',
            },
          }
        },
      ],
    },
    fence_agents => [
      type => 'fence_lpar',
      data => {
        ipaddr         => 'hmc00.example.org',
        login          => 'service-fence',
        managed        => 'host0',
        identity_file  => '/root/.ssh/service_fence_rsa',
        pcmk_delay_max => 10,
        pcmk_host_map  => {
          'pr0-app00.example.org' => 'pr0-app00',
          'pr0-app01.example.org' => 'pr0-app01',
          'pr0-dba.example.org'   => 'pr0-dba',
          'pr0-dbb.example.org'   => 'pr0-dbb',
        },
      },
    ],
  },
}
```

Similar configuration in hiera, however, in this case additional filesystem resources have been configured on the database side which are needed for a Commvault configuration.

```yaml
sap::cluster::sid_rules:
  PR0:
    node_roles:
      'pr0-app00.example.org':
        - 'app-scs'
        - 'app-standard'
      'pr0-app01.example.org':
        - 'app-scs'
        - 'app-standard'
      'pr0-dba.example.org':
        - 'db-standard'
      'pr0-dbb.example.org':
        - 'db-standard'
    components:
      - type: 'ScsErs'
        data:
          ers_ip: '10.0.0.103'
          ers_inst:
            type: 'ERS'
            number: '42'
            host: 'pr0-ers'
            legacy_start_profile: true
          scs_ip: '10.0.0.101'
          scs_inst:
            type: 'ASCS'
            number: '41'
            host: 'pr0-cs'
            legacy_start_profile: true
      - type: 'As'
        data:
          as_instances:
            - type: 'DVEBMGS'
              number: '40'
              host: 'pr0-app00'
              legacy_start_profile: true
            - type: 'D'
              number: '40'
              host: 'pr0-app01'
              legacy_start_profile: true
      - type: 'Db2'
        data:
          db_ip: '10.0.0.102'
          mode: 'log_shipping'
          volumes:
            - volgrpname: 'db2cv'
              exclusive: true
          filesystems:
            - volume_group: 'db2cv'
              logical_volume: 'db2cvarch'
              directory: '/db2/PR0/commvault/archive'
              fstype: 'xfs'
            - volume_group: 'db2cv'
              logical_volume: 'db2cvaudit'
              directory: '/db2/PR0/commvault/audit'
              fstype: 'xfs'
            - volume_group: 'db2cv'
              logical_volume: 'db2cvretrieve'
              directory: '/db2/PR0/commvault/retrieve'
              fstype: 'xfs'
            - volume_group: 'db2cv'
              logical_volume: 'db2cvjobres'
              directory: '/db2/PR0/commvault/job_results'
              fstype: 'xfs'
sap::cluster::fence_agents:
  - type: 'fence_lpar'
    data:
      ipaddr: 'hmc00.example.org'
      login: 'service-fence'
      managed: 'host0'
      identity_file: '/root/.ssh/service_fence_rsa'
      pcmk_delay_max: 10
      pcmk_host_map:
        'pr0-app00.example.org': 'pr0-app00'
        'pr0-app01.example.org': 'pr0-app01'
        'pr0-dba.example.org': 'pr0-dba'
        'pr0-dbb.example.org': 'pr0-dbb'
```

## Limitations

This module has been built on and tested against Puppet 5.5 and higher.

The module has been tested on:

* RedHat Enterprise Linux 7

It should also work on:

* CentOS 6/7
* Oracle Enterprise Linux 6/7
* Scientific Linux 6/7

Testing on other platforms has been light and cannot be guaranteed.

## Development

If you like to add or improve this module, feel free to fork the module and send
me a merge request with the modification.

## Contributors

Check out the [contributor list](https://github.com/thbe/puppet-sap/graphs/contributors).
