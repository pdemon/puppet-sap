# Describes the message server information needed for an external connection to
# a given SID.
type Sap::MessageServer = Hash[
  Enum[
    'class',         # e.g. cs-abap or cs-java
    'number',        # e.g. '00'
    'port_override', # e.g. '3915', optional and should be used sparingly
  ],
  Variant[
    Sap::InstClassSapCs,
    Sap::InstNumber,
    Stdlib::Port,
  ],
]
