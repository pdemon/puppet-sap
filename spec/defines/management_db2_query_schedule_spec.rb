require 'spec_helper'
require 'deep_merge'
query_list = [
  {
    query_file_name: 'simple_query',
    query_content: 'puppet:///module_specific/sap/simple_query',
    cron_entry: {
      'hour' => '*',
      'minute' => '15',
    },
  },
  {
    query_file_name: 'complex_query',
    query_content: 'puppet:///module_specific/sap/complex_query',
    cron_entry: {
      'hour' => '2',
      'minute' => '30',
    },
    'schema' => 'SAPPRD',
    'output_target' => '/sapmnt/${SAPSYSTEMNAME}/interface/outbound/scm/complex_output.txt',
    'output_group' => 'sapsys',
    'monitor_alias' => 'qas-db.example.org',
  },
]
query_directory = '/db2/db2qas/queries'
optional_params = {
  'output_target' => '-t',
  'monitor_alias' => '-a',
  'output_group' => '-g',
  'schema' => '-s',
}

describe 'sap::management::db2_query_schedule' do
  let(:sid) { 'QAS' }
  let(:params) do
    {
      sid: sid,
      query_directory: query_directory,
    }
  end

  query_list.each_index do |idx|
    query_params = query_list[idx]
    name = query_params[:query_file_name]
    query_path = "#{query_directory}/#{name}"
    context "query #{name}" do
      let(:title) { "db2_#{sid}_schedule_#{idx}" }

      before(:each) do
        params.deep_merge(query_params)
      end

      it 'compiles without error' do
        is_expected.to compile.with_all_deps
      end

      it 'installs the query file' do
        is_expected.to contain_file(query_path).with(
          ensure: 'file',
          owner: "db2#{sid.downcase}",
          group: "db#{sid.downcase}adm",
          mode: '0644',
          source: query_params[:query_content],
        )
      end

      it 'schedules the query' do
        command_array = [
          'source "${HOME}/.profile"; /usr/local/bin/db2_execute_sql',
          " -d #{sid}",
          " -q #{query_path}",
        ]
        optional_params.each do |key, flag|
          command_array << " #{flag} #{query_params[key]}" if query_params.key?(key)
        end
        command_array << ' >/dev/null'

        is_expected.to contain_cron("db2_query_schedule_#{sid}_#{name}").with(
          query_params[:cron_entry].merge(
            ensure: 'present',
            user: "db2#{sid.downcase}",
            require: "File[#{query_path}]",
            command: command_array.join,
          ),
        )
      end
    end
  end
end
