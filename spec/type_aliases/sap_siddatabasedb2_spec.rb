require 'spec_helper'

describe 'Sap::SIDDatabaseDb2' do
  it 'accepts strings that are exactly 3 characters' do
    is_expected.to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        ha_mode: 'hadr',
        hadr_base_port: 55_000,
        host: 'scp-a-db',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects entries missing ha_mode' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects entries missing com_port' do
    is_expected.not_to allow_value(
      {
        fcm_base_port: 5_914,
        ha_mode: 'none',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects entries missing fcm_base_port' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        ha_mode: 'none',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects entries missing nodes' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        ha_mode: 'none',
      },
    )
  end

  it 'rejects invalid ha_mode entries' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        ha_mode: 'not-an-entry',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects invalid com_port entries' do
    is_expected.not_to allow_value(
      {
        com_port: 70_000,
        fcm_base_port: 5_914,
        ha_mode: 'none',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects invalid fcm_base_port entries' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 500_914,
        ha_mode: 'none',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects invalid host entries' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        ha_mode: 'hadr',
        host: [
          'scp-a-db',
        ],
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end

  it 'rejects invalid hadr_base_port entries' do
    is_expected.not_to allow_value(
      {
        com_port: 5_912,
        fcm_base_port: 5_914,
        ha_mode: 'hadr',
        host: 'scp-a-db',
        hadr_base_port: 'a_port',
        nodes: [
          'scp-a-dba.sap.arden',
          'scp-a-dbb.sap.arden',
        ],
      },
    )
  end
end
