require 'spec_helper'
require 'puppet/sap/facts'

# Stuff to stub out tests
class StatOutput
  def uid
    100
  end
end

Passwd = Struct.new(:name)

# Test code based on the following article
# https://github.com/voxpupuli/puppet-jenkins/blob/master/spec/unit/facter/plugins_spec.rb
describe Puppet::Sap::Facts::Instances do
  describe '.add_sid' do
    it 'does nothing when adding an existing SID' do
      described_class.instance_variable_set(:@sid_data, 'EPT' => 'garbage')
      described_class.add_sid('EPT')
      expect(described_class.sid_data['EPT']).to eq('garbage')
    end

    it 'inserts a the new SID when it is not persent' do
      described_class.instance_variable_set(:@sid_data, {})
      expect(described_class.sid_data).not_to include('EPT')
      described_class.add_sid('EPT')
      sid_data = described_class.sid_data
      expect(sid_data).to include('EPT')
      expect(sid_data['EPT']).to include(:instances)
      expect(sid_data['EPT'][:instances]).to eq({})
    end
  end

  describe '.check_scmopt' do
    subject(:check_scmopt) { described_class.check_scmopt }

    before(:each) do
      allow(Dir).to receive(:exists?).and_return(scmopt_exists)
      allow(described_class).to receive(:get_file_sid).and_return(target_sid)
      described_class.instance_variable_set(:@sid_data, {})
      described_class.instance_variable_set(:@inst_classes, [])
      allow(described_class).to receive(:list_dir).and_return(entries)
    end

    context "'/scmopt' does not exist" do
      let(:scmopt_exists) { false }
      let(:target_sid) { nil }
      let(:entries) { [] }

      it 'does nothing' do
        check_scmopt
        expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
        expect(described_class.instance_variable_get(:@sid_data)).to be_empty
      end
    end

    context "'/scmopt' exists" do
      let(:scmopt_exists) { true }
      let(:target_sid) { 'GWP' }
      let(:entries) do
        [
          'ctmsvr',
          'dsoptsvr',
          'libgurobi70.so',
          'libicudata.so.50',
          'libicui18n.so.50',
          'libicuuc.so.50',
          'libsapnwrfc.so',
          'mipopsvr',
          'mmpopsvr',
          'ncs.so',
          'sapcpp47.so',
          'seqopsvr',
          'snpopsvr',
          'vsoopsvr',
          'vsropsvr',
        ]
      end

      it "adds 'scmopt' to @inst_classes and updates @sid_data" do
        check_scmopt
        expect(described_class.instance_variable_get(:@inst_classes)).to include('scmopt')
        tarsid_data = {
          'GWP' => {
            instances: {
              scmopt: {
                type: 'scmopt',
                class: 'scmopt',
                optimizers: [
                  'ctmsvr',
                  'dsoptsvr',
                  'mipopsvr',
                  'mmpopsvr',
                  'seqopsvr',
                  'snpopsvr',
                  'vsoopsvr',
                  'vsropsvr',
                ],
              },
            },
          },
        }
        expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
      end
    end
  end

  describe '.check_db2' do
    before(:each) do
      allow(described_class).to receive(:list_dir).and_return(db2_entries)
      described_class.instance_variable_set(:@sid_data, {})
      described_class.instance_variable_set(:@inst_classes, [])
    end

    context '/db2 does not exist' do
      let(:db2_entries) { [] }

      it 'does nothing' do
        described_class.check_db2
        expect(described_class.instance_variable_get(:@sid_data)).to be_empty
        expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
      end
    end

    context '/db2 exists' do
      context 'and does not contain a valid SID folder' do
        let(:db2_entries) { ['db2scd', 'garbage', 'gbg'] }

        it 'does nothing' do
          described_class.check_db2
          expect(described_class.instance_variable_get(:@sid_data)).to be_empty
          expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
        end
      end

      context 'and contains a valid SID folder' do
        let(:db2_entries) { ['db2scd', 'SCD'] }

        it "adds 'db-db2' to @inst_classes" do
          described_class.check_db2
          expect(described_class.instance_variable_get(:@inst_classes)).to include('db-db2')
        end

        it 'adds the SID to @sid_data with the db2 database instance' do
          described_class.check_db2
          tarsid_data = {
            'SCD' => {
              instances: {
                database: {
                  type: 'db2',
                  class: 'db-db2',
                },
              },
            },
          }
          expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
        end
      end

      context 'and contains multiple SID folders' do
        let(:db2_entries) { ['db2scd', 'MP0', 'MP1'] }

        it 'records both database entries' do
          described_class.check_db2
          tarsid_data = {
            'MP0' => {
              instances: {
                database: {
                  type: 'db2',
                  class: 'db-db2',
                },
              },
            },
            'MP1' => {
              instances: {
                database: {
                  type: 'db2',
                  class: 'db-db2',
                },
              },
            },
          }
          tarinst_classes = [
            'db-db2',
            'db-db2',
          ]
          expect(described_class.instance_variable_get(:@inst_classes)).to match_array tarinst_classes
          expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
        end
      end
    end
  end

  describe '.check_livecache' do
    before(:each) do
      allow(described_class).to receive(:list_dir).and_return(lc_entries)
      allow(described_class).to receive(:get_file_sid).and_return(target_sid)
      described_class.instance_variable_set(:@sid_data, {})
      described_class.instance_variable_set(:@inst_classes, [])
    end

    context 'when /sapdb does not exist' do
      let(:lc_entries) { [] }
      let(:target_sid) { nil }

      it 'nothing is added' do
        described_class.check_livecache
        expect(described_class.instance_variable_get(:@sid_data)).to be_empty
        expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
      end
    end

    context 'when /sapdb exists' do
      let(:target_sid) { 'SCD' }

      context 'but the sid cannot be deterimend' do
        let(:lc_entries) { ['garbage'] }
        let(:target_sid) { nil }

        it 'nothing is added' do
          described_class.check_livecache
          expect(described_class.instance_variable_get(:@sid_data)).to be_empty
          expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
        end
      end

      context 'and partially contains the client' do
        let(:lc_entries) { ['data'] }

        it 'livecache_client entries are not added' do
          described_class.check_livecache
          tarsid_data = {
            'SCD' => {
              instances: {
                livecache: {
                  type: 'livecache',
                  class: 'db-livecache-client',
                },
              },
            },
          }
          expect(described_class.instance_variable_get(:@inst_classes)).not_to include('db-livecache-client')
          expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
        end
      end

      context 'contains the client' do
        let(:lc_entries) { ['data', 'programs'] }

        it 'adds the client information' do
          described_class.check_livecache
          tarsid_data = {
            'SCD' => {
              instances: {
                livecache: {
                  type: 'livecache',
                  class: 'db-livecache-client',
                  client: true,
                },
              },
            },
          }
          expect(described_class.instance_variable_get(:@inst_classes)).to include('db-livecache-client')
          expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
        end
      end

      context 'contains a database' do
        let(:lc_entries) { ['SLD'] }

        it 'adds the database entry' do
          described_class.check_livecache
          tarsid_data = {
            'SCD' => {
              instances: {
                livecache: {
                  type: 'livecache',
                  class: 'db-livecache',
                  databases: { 'SLD' => true },
                },
              },
            },
          }
          expect(described_class.instance_variable_get(:@inst_classes)).not_to include('db-livecache-client')
          expect(described_class.instance_variable_get(:@inst_classes)).to include('db-livecache')
          expect(described_class.instance_variable_get(:@sid_data)).to eq(tarsid_data)
        end
      end
    end
  end

  describe '.check_sap_sid' do
    let(:sid) { 'SCD' }
    let(:dual_dir_exists) { false }

    before(:each) do
      allow(described_class).to receive(:list_dir).and_return(entries)
      allow(Dir).to receive(:exist?).and_return(dual_dir_exists)
      described_class.instance_variable_set(:@sid_data, {})
      described_class.instance_variable_set(:@inst_classes, [])
    end

    context 'SID dir contains no matching entries' do
      let(:entries) { ['SYS'] }

      it 'returns an empty hash' do
        expect(described_class.check_sap_sid(sid)).to eq({})
        expect(described_class.instance_variable_get(:@inst_classes)).to eq([])
      end
    end

    context 'SID dir is empty' do
      let(:entries) { [] }

      it 'returns an empty hash' do
        expect(described_class.check_sap_sid(sid)).to eq({})
        expect(described_class.instance_variable_get(:@inst_classes)).to eq([])
      end
    end

    context 'SID dir includes every standard type' do
      let(:entries) do
        [
          'DVEBMGS00',
          'D10',
          'J20',
          'ASCS01',
          'SCS11',
          'W03',
          'G60',
          'ERS02',
        ]
      end
      let(:dual_dir_exists) { false }

      it 'detects the standard types' do
        tarinst_classes = [
          'as-abap',
          'as-abap',
          'as-java',
          'cs-abap',
          'cs-java',
          'webdisp',
          'gateway',
          'ers',
        ]
        result = {
          '00' => {
            type: 'DVEBMGS',
            class: 'as-abap',
            profiles: [],
          },
          '10' => {
            type: 'D',
            class: 'as-abap',
            profiles: [],
          },
          '20' => {
            type: 'J',
            class: 'as-java',
            profiles: [],
          },
          '01' => {
            type: 'ASCS',
            class: 'cs-abap',
            profiles: [],
          },
          '11' => {
            type: 'SCS',
            class: 'cs-java',
            profiles: [],
          },
          '03' => {
            type: 'W',
            class: 'webdisp',
            profiles: [],
          },
          '60' => {
            type: 'G',
            class: 'gateway',
            profiles: [],
          },
          '02' => {
            type: 'ERS',
            class: 'ers',
            profiles: [],
          },
        }
        expect(described_class.check_sap_sid(sid)).to eq(result)
        expect(described_class.instance_variable_get(:@inst_classes)).to eq(tarinst_classes)
      end

      context 'AS Dual' do
        let(:entries) { ['DVEBMGS00'] }
        let(:dual_dir_exists) { true }

        it 'detects DVEBMGS00 as a dual stack' do
          tarinst_classes = ['as-dual']
          result = {
            '00' => {
              type: 'DVEBMGS',
              class: 'as-dual',
              profiles: [],
            },
          }
          expect(described_class.check_sap_sid(sid)).to eq(result)
          expect(described_class.instance_variable_get(:@inst_classes)).to eq(tarinst_classes)
        end
      end
    end
  end

  describe '.check_profiles' do
    let(:sid) { 'SCD' }
    let(:insttype) { 'DVEBMGS' }
    let(:instnum) { '40' }

    before(:each) do
      allow(Dir).to receive(:exist?).and_return(dir_exists)
      allow(described_class).to receive(:list_dir).and_return(entries)
    end

    context 'and profile directory does not exist' do
      let(:dir_exists) { false }
      let(:entries) { [] }

      it 'returns an empty array' do
        expect(described_class.check_profiles(sid, insttype, instnum)).to eq([])
      end
    end

    context 'and profile directory exists' do
      let(:dir_exists) { true }

      context 'but is empty' do
        let(:entries) { [] }

        it 'returns an empty array' do
          expect(described_class.check_profiles(sid, insttype, instnum)).to eq([])
        end
      end

      context 'and contains matching entries' do
        let(:entries) do
          [
            'DEFAULT.1.PFL',
            'DEFAULT.2.PFL',
            'DEFAULT.3.PFL',
            'DEFAULT.4.PFL',
            'DEFAULT.PFL',
            'SCD_ASCS41_scd-a-cs',
            'SCD_ASCS41_scd-a-cs.1',
            'SCD_DVEBMGS40_scd-a-app00',
            'SCD_DVEBMGS40_scd-a-app00.1',
            'SCD_ERS42_scd-a-ers',
            'SCD_ERS42_scd-a-ers.lst',
            'START_ASCS41_scd-a-cs',
            'START_DVEBMGS40_scd-a-app00',
            'START_ERS42_scd-a-ers',
          ]
        end
        let(:dir) { '/usr/sap/SCD/SYS/profile/' }

        it 'returns the list of matching files' do
          matching_profiles = [
            'SCD_DVEBMGS40_scd-a-app00',
            'SCD_DVEBMGS40_scd-a-app00.1',
            'START_DVEBMGS40_scd-a-app00',
          ]
          full_profiles = matching_profiles.map { |s| s.prepend(dir) }
          expect(described_class.check_profiles(sid, insttype, instnum)).to eq(full_profiles)
        end
      end
    end
  end

  describe '.check_sap_instances' do
    before(:each) do
      allow(described_class).to receive(:list_dir).and_return(inst_dirs)
      described_class.instance_variable_set(:@sid_data, {})
      described_class.instance_variable_set(:@inst_classes, [])
    end

    context "'/usr/sap' is empty" do
      let(:inst_dirs) { [] }

      it 'does nothing' do
        described_class.check_sap_instances
        expect(described_class.instance_variable_get(:@sid_data)).to be_empty
        expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
      end
    end

    context "'/usr/sap' contains non SID entries" do
      let(:inst_dirs) { ['tmp', 'trans', 'hostcontrol', 'ccms'] }

      it 'does nothing' do
        described_class.check_sap_instances
        expect(described_class.instance_variable_get(:@sid_data)).to be_empty
        expect(described_class.instance_variable_get(:@inst_classes)).to be_empty
      end
    end

    context "'/usr/sap' contains SID entries" do
      let(:inst_dirs) { ['tmp', 'trans', 'hostcontrol', 'ccms', 'GWD', 'SCD', 'DAA'] }

      it 'and adds each valid SID' do
        described_class.check_sap_instances
        sid_data = described_class.instance_variable_get(:@sid_data)
        tarsid_data = {
          'GWD' => {
            instances: {},
          },
          'SCD' => {
            instances: {},
          },
          'DAA' => {
            instances: {},
          },
        }
        expect(sid_data).to eq(tarsid_data)
      end
    end
  end

  describe '.determine_file_sid' do
    let(:target) { '/sapdb' }

    before(:each) do
      allow(File).to receive(:exist?).and_return(target_exists)
      allow(File).to receive(:stat).and_return(StatOutput.new)
    end

    context 'and target does not exist' do
      let(:target_exists) { false }

      it 'returns nil' do
        expect(described_class.get_file_sid(target)).to be_nil
      end
    end

    context 'and target exists' do
      let(:target_exists) { true }

      before(:each) do
        allow(Etc).to receive(:getpwuid).and_return(Passwd.new(username))
      end

      context 'lowercase username' do
        let(:username) { 'scpdb' }

        it 'returns the first three characters in uppercase' do
          expect(described_class.get_file_sid(target)).to eq('SCP')
        end
      end

      context 'uppercase username' do
        let(:username) { 'SCPDB' }

        it 'returns the first three characters in uppercase' do
          expect(described_class.get_file_sid(target)).to eq('SCP')
        end
      end

      context 'fewer than 3 character username' do
        let(:username) { 'SB' }

        it 'returns nil' do
          expect(described_class.get_file_sid(target)).to be_nil
        end
      end
    end
  end

  describe '.inst_classes' do
    context 'class list contains duplicates' do
      before(:each) do
        target_classes = [
          'db-db2',
          'db-db2',
          'as-abap',
          'as-java',
          'cs-abap',
          'cs-java',
        ]
        described_class.instance_variable_set(:@inst_classes, target_classes)
      end

      it 'returns only unique entries' do
        unique_classes = [
          'db-db2',
          'as-abap',
          'as-java',
          'cs-abap',
          'cs-java',
        ]
        expect(described_class.inst_classes).to eq(unique_classes)
      end
    end
  end
end
