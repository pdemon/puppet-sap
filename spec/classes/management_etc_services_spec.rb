require 'spec_helper'
require 'deep_merge'

# SAP Instance Port classes
sap_port_clases = {
  'disp' => {
    'comment' => 'SAP System Dispatcher Port',
    'base_value' => 3200,
    'name_pattern' => 'sapdp%02d',
  },
  'gw' => {
    'comment' => 'SAP System Gateway Port',
    'base_value' => 3300,
    'name_pattern' => 'sapgw%02d',
    'prior_port' => 'sapdp99',
  },
  'disp-sec' => {
    'comment' => 'SAP System Dispatcher Security Port',
    'base_value' => 4700,
    'name_pattern' => 'sapdp%02ds',
    'prior_port' => 'sapgw99',
  },
  'gw-sec' => {
    'comment' => 'SAP System Gateway Security Port',
    'base_value' => 4800,
    'name_pattern' => 'sapgw%02ds',
    'prior_port' => 'sapdp99s',
  },
  'cs-abap' => {
    'comment' => 'SAP System Message Server ABAP Port',
    'base_value' => 3600,
    'name_pattern' => 'sapms%02d',
    'prior_port' => 'sapgw99s',
  },
  'cs-java' => {
    'comment' => 'SAP System Message Server Internal Port',
    'base_value' => 3900,
    'name_pattern' => 'sapms%02di',
    'prior_port' => 'sapms99',
  },
  'as-ctrl' => {
    'comment' => 'SAP HTTP Control Port',
    'base_value' => 50_013,
    'name_pattern' => 'sapctrl%02d',
    'prior_port' => 'sapms99i',
    'port_multiplier' => 100,
  },
  'as-ctrls' => {
    'comment' => 'SAP HTTPS Control Port',
    'base_value' => 50_014,
    'name_pattern' => 'sapctrl%02ds',
    'prior_port' => 'sapctrl99',
    'port_multiplier' => 100,
  },
}

describe 'sap::management::etc_services' do
  let(:params) do
    {
      message_servers: {},
      system_ids: {},
    }
  end

  context 'no local instances' do
    it 'does not create etc_services entries' do
      is_expected.to compile.with_all_deps
      is_expected.to have_etc_services_resource_count(0)
    end
  end

  context 'standard application node' do
    let(:facts) do
      {
        sap: {
          sid_hash: {
            PRD: {
              instances: {
                '01' => {
                  type: 'ASCS',
                },
                '00' => {
                  type: 'D',
                },
              },
            },
          },
          inst_classes: [
            'cs-abap',
            'as-abap',
          ],
        },
      }
    end
    let(:params) do
      {
        message_servers: {
          'ECP' => {
            'class'  => 'cs-abap',
            'number' => '01',
          },
          'EQ0' => {
            'class'  => 'cs-abap',
            'number' => '01',
          },
          'SCP' => {
            'class' => 'cs-abap',
            'number' => '41',
          },
          'LP0' => {
            'class' => 'cs-java',
            'number' => '14',
          },
        },
        system_ids: {
          'PRD' => {
            'manage_profiles' => false,
            'components' => [],
            'databases' => {
              'db-db2' => {
                'com_port' => 5912,
                'fcm_base_port' => 5914,
                'ha_mode' => 'none',
                'nodes' => ['prd-dba'],
              },
            },
          },
        },
      }
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the standard SAP entries' do
      (0..99).each do |port|
        sap_port_clases.each do |port_class, data|
          port_offset = if data.key?('port_multiplier')
                          port * data['port_multiplier']
                        else
                          port
                        end
          port_number = data['base_value'] + port_offset
          port_fixed_width = '%02d' % port
          name = data['name_pattern'] % port
          base_parameters = {
            ensure: 'present',
            protocols: { 'tcp' => port_number },
            comment: data['comment'],
          }

          prior_port = if port > 0
                         data['name_pattern'] % (port - 1)
                       elsif data.key?('prior_port')
                         data['prior_port']
                       else
                         nil
                       end
          requires = if !prior_port.nil?
                       { require: "Etc_services[#{prior_port}]" }
                     else
                       {}
                     end

          aliases = { aliases: [] }
          params[:message_servers].each do |sid, sid_data|
            sid_class = sid_data['class']
            ms_num = sid_data['number']
            next unless sid_class == port_class && port_fixed_width == ms_num
            alias_name = case port_class
                         when 'cs-abap'
                           'sapms%s' % sid
                         when 'cs-java'
                           'sapms%sj' % sid
                         else
                           nil
                         end
            aliases[:aliases] << alias_name unless alias_name.nil?
          end

          parameters = base_parameters.merge(requires)
          parameters = parameters.merge(aliases)
          is_expected.to contain_etc_services(name).with(parameters)
        end
      end
    end

    it 'comments out the standard SAP entries' do
      sap_port_clases.each do |_port_class, data|
        base_value = data['base_value']
        base_value_string = base_value.to_s

        line_pattern = data['name_pattern'].sub(%r{%02d}, '[0-9]{2}')
        port_pattern = if base_value < 10_000
                         "#{base_value_string[0..1]}[0-9]{2}"
                       else
                         "#{base_value_string[0]}[0-9]{2}#{base_value_string[3..4]}"
                       end

        is_expected.to contain_exec("sed -i -r '/^#{line_pattern}/! s/^([a-zA-Z0-9_-]+\\s+#{port_pattern}\\/\\w{,4}\\s+.*)/#&/' /etc/services").with(
          path: '/sbin:/bin:/usr/sbin:/usr/bin',
          onlyif: [
            "test 0 -eq $(grep -vE '^#{line_pattern}' /etc/services | grep -E '^[a-zA-Z0-9_-]+\\s+#{port_pattern}\\/\\w{,4}\\s+.*' >/dev/null; echo $?)",
          ],
        )
      end
    end

    it 'creates the hostctrl entries' do
      is_expected.to contain_etc_services('saphostctrl').with(
        ensure: 'present',
        comment: 'SAP Host Agent with SOAP/HTTP',
        protocols: { 'tcp' => 1128 },
      )
      is_expected.to contain_etc_services('saphostctrls').with(
        ensure: 'present',
        comment: 'SAP Host Agent with SOAP/HTTPS',
        protocols: { 'tcp' => 1129 },
      )
    end

    context 'livecache client' do
      before(:each) do
        facts.merge!(
          sap: {
            sid_hash: {
              PRD: {
                instances: {
                  'livecache' => {
                    type: 'livecache',
                    client: true,
                  },
                },
              },
            },
            inst_classes: [
              'db-livecache-client',
            ],
          },
        )
      end

      it 'compiles' do
        is_expected.to compile.with_all_deps
      end

      it 'creates the livecache services' do
        is_expected.to contain_etc_services('sql30').with(
          ensure: 'present',
          comment: 'MaxDB/liveCache Connection Port',
          protocols: { 'tcp' => 7200 },
        )
        is_expected.to contain_etc_services('sql6').with(
          ensure: 'present',
          comment: 'MaxDB/liveCache Connection Port',
          protocols: { 'tcp' => 7210 },
        )
        is_expected.to contain_etc_services('sapdbni72').with(
          ensure: 'present',
          comment: 'MaxDB/liveCache Port',
          protocols: { 'tcp' => 7269 },
        )
        is_expected.to contain_etc_services('sdbnissl76').with(
          ensure: 'present',
          comment: 'MaxDB/liveCache Port',
          protocols: { 'tcp' => 7270 },
        )
        is_expected.to contain_etc_services('SDB').with(
          ensure: 'present',
          comment: 'MaxDB/liveCache Webtools Port',
          enforce_syntax: false,
          protocols: { 'tcp' => 7575 },
        )
      end
    end

    context 'database connection with db2 backend' do
      it 'creates the connection service' do
        is_expected.to contain_etc_services('sapdb2PRD').with(
          ensure: 'present',
          comment: 'SAP DB2 Communication Port',
          protocols: { 'tcp' => 5912 },
        )
      end
    end
  end

  context 'livecache database server node' do
    let(:facts) do
      {
        sap: {
          sid_hash: {
            PRD: {
              instances: {
                'livecache' => {
                  type: 'livecache',
                  client: true,
                  databases: {
                    'ELP' => true,
                  },
                },
              },
            },
          },
          inst_classes: [
            'db-livecache',
            'db-livecache-client',
          ],
        },
      }
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the livecache services' do
      is_expected.to contain_etc_services('sql30').with(
        ensure: 'present',
        comment: 'MaxDB/liveCache Connection Port',
        protocols: { 'tcp' => 7200 },
      )
      is_expected.to contain_etc_services('sql6').with(
        ensure: 'present',
        comment: 'MaxDB/liveCache Connection Port',
        protocols: { 'tcp' => 7210 },
      )
      is_expected.to contain_etc_services('sapdbni72').with(
        ensure: 'present',
        comment: 'MaxDB/liveCache Port',
        protocols: { 'tcp' => 7269 },
      )
      is_expected.to contain_etc_services('sdbnissl76').with(
        ensure: 'present',
        comment: 'MaxDB/liveCache Port',
        protocols: { 'tcp' => 7270 },
      )
      is_expected.to contain_etc_services('SDB').with(
        ensure: 'present',
        comment: 'MaxDB/liveCache Webtools Port',
        enforce_syntax: false,
        protocols: { 'tcp' => 7575 },
      )
    end
  end

  context 'database server node' do
    let(:facts) do
      {
        sap: {
          sid_hash: {
            PRD: {
              instances: {
                'database' => {
                  type: 'db2',

                },
              },
            },
          },
          inst_classes: [
            'db-db2',
          ],
        },
      }
    end

    before(:each) do
      params.merge!(
        system_ids: {
          'PRD' => {
            'manage_profiles' => false,
            'components' => [],
            'databases' => {
              'db-db2' => {
                'com_port' => 5912,
                'fcm_base_port' => 5914,
                'ha_mode' => 'hadr',
                'hadr_base_port' => 55_000,
                'nodes' => [
                  'prd-dba.example.com',
                  'prd-dbb.example.com',
                  'prd-dbc.example.com',
                  'prd-dbd.example.com',
                ],
              },
            },
          },
          'DEV' => {
            'manage_profiles' => false,
            'components' => [],
            'databases' => {
              'db-db2' => {
                'com_port' => 5922,
                'fcm_base_port' => 5924,
                'ha_mode' => 'none',
                'nodes' => [
                  'dev-db.example.com',
                ],
              },
            },
          },
        },
      )
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the connection service port' do
      params[:system_ids].each do |sid, data|
        next unless data['databases'].key?('db-db2')

        db2_data = data['databases']['db-db2']
        com_port = db2_data['com_port']
        is_expected.to contain_etc_services("sapdb2#{sid}").with(
          ensure: 'present',
          comment: 'SAP DB2 Communication Port',
          protocols: { 'tcp' => com_port },
        )
      end
    end

    it 'defines the internal FCM ports' do
      params[:system_ids].each do |sid, data|
        next unless data['databases'].key?('db-db2')

        db2_data = data['databases']['db-db2']
        inst = "db2#{sid.downcase}"
        base_port = db2_data['fcm_base_port']
        ha_mode = db2_data['ha_mode']
        nodes = db2_data['nodes']

        is_expected.to contain_etc_services("DB2_#{inst}").with(
          ensure: 'present',
          comment: "DB2 FCM Instance Port (Range Start): #{sid}",
          enforce_syntax: false,
          protocols: { 'tcp' => base_port },
          before: "Etc_services[DB2_#{inst}_1]",
        )
        fcm_last = if nodes.length > 1
                     nodes.length
                   else
                     2
                   end
        (1..fcm_last).each do |idx|
          params_base = {
            ensure: 'present',
            comment: "DB2 FCM Instance Port #{idx}: #{sid}",
            enforce_syntax: false,
            protocols: { 'tcp' => base_port + idx },
          }
          params_require = if idx > 1
                             { require: "Etc_services[DB2_#{inst}_#{idx - 1}]" }
                           else
                             { require: "Etc_services[DB2_#{inst}]" }
                           end
          params = params_base.merge(params_require)
          is_expected.to contain_etc_services("DB2_#{inst}_#{idx}").with(params)
        end
        is_expected.to contain_etc_services("DB2_#{inst}_END").with(
          ensure: 'present',
          comment: "DB2 FCM Instance Port (Range End): #{sid}",
          enforce_syntax: false,
          protocols: { 'tcp' => base_port + fcm_last + 1 },
          require: "Etc_services[DB2_#{inst}_#{fcm_last}]",
        )

        case ha_mode
        when 'hadr'
          hadr_range = (0..nodes.length - 1)
          hadr_base_port = db2_data['hadr_base_port']
        else
          hadr_range = []
        end

        hadr_range.each do |idx|
          node = nodes[idx]
          params_base = {
            ensure: 'present',
            comment: "HADR port for #{node}",
            enforce_syntax: false,
            protocols: { 'tcp' => hadr_base_port + idx },
          }
          params_require = if idx > 0
                             { require: "Etc_services[#{sid}_HADR_#{idx}]" }
                           else
                             { require: "Etc_services[DB2_#{inst}_END]" }
                           end
          params = params_base.merge(params_require)
          idx_id = idx + 1
          is_expected.to contain_etc_services("#{sid}_HADR_#{idx_id}").with(params)
        end
      end
    end
  end
end
