# Packages are installed based on the selections provided to the main sap class
# using sane defaults configured in hiera.
#
# @summary Installs the packages associated with the selected sets
#
class sap::install {
  # Install packages required by enabled components
  $install_components = ['common'] + $sap::enabled_components
  $not_rhel_7x = $facts['os']['release']['major'] != '7'
  $install_components.each |$component| {
    # Skip entries with no components
    if ! $component in $sap::component_packages {
      notify { "sap::install: no package list for ${component}": }
      next
    }

    # Skip components which require RHEL 7.x if we're not on 7.x
    if $component in $sap::params::rhel7_components and $not_rhel_7x {
      notify { "sap::install: ${component} only supported on RHEL 7.x!": }
      next
    }

    # Install the package set if a list was actually provided
    $packages = $sap::component_packages[$component]
    unless(empty($packages)) {
      ensure_packages($packages)
    }
  }

  # Install database backend required packages
  unless(empty($sap::backend_databases)) {
    $sap::backend_databases.each | $backend | {
      if ! $backend in $sap::backenddb_packages {
        notify { "sap::install: no package list for ${backend} database type": }
        next
      }

      $packages = $sap::backenddb_packages[$backend]
      unless(empty($packages)) {
        ensure_packages($packages)
      }
    }
  }

  # Mount point dependencies
  if $sap::manage_mount_dependencies {
    contain sap::install::mount_dependencies
  }
}
