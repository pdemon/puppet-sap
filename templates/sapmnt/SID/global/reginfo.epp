 <%- |  Sap::SID $sid,
        Boolean $support_internal_keyword,
        Array[Stdlib::Host] $internal_hosts,
        Array[Sap::RegInfo] $rules,
| -%>
#VERSION=2
# !!! Managed by Puppet - Do not modify directly as changes will be overwritten !!!
#
# Controls the registration of external programs in this gateway
#
# General Information:
# - File is evaluated top to bottom, first matching rule wins
# - Fields:
#   * P/D:    Permit and deny respectively
#   * TP:     program name, 64 non-Unicode characters, no blanks
#   * NO:     Integer representing the number of instances which can register
#             for TP. This is only relevant when TP does not contain a wildcard.
#   The following rules can specify IPs, Domain Names, or a combination of both
#   * HOST:   Machine(s) on which $TP will run
#   * ACCESS: Remote machines which are allowed to run $TP on $HOST
#   * CANCEL: Remote machines which are allowed to cancel $TP on $HOST
#
# Note that the first matching rule applies read top to bottom.
#
# See SAP Note 1408081 for an introduction
# Syntax detail in SAP Note 1069911
<%
$rules.each |$rule| {
  $mandatory = ['MODE', 'TP']
  $type_map = {
    'MODE' => {
      'type' => Sap::SecRuleMode,
      'name' => 'Sap::SecRuleMode',
    },
    'TP' => {
      'type' => Sap::TargetProgram,
      'name' => 'Sap::TargetProgram',
    },
    'NO' => {
      'type' => Integer,
      'name' => 'Integer',
    },
    'HOST' => {
      'type' => Array[Sap::HostRuleEntry],
      'name' => 'Array[Sap::HostRuleEntry]',
    },
    'ACCESS' => {
      'type' => Array[Sap::HostRuleEntry],
      'name' => 'Array[Sap::HostRuleEntry]',
    },
    'CANCEL' => {
      'type' => Array[Sap::HostRuleEntry],
      'name' => 'Array[Sap::HostRuleEntry]',
    },
  }
  # Ensure all mandatory keys are present
  $mandatory.each |$key| {
    unless($key in $rule) {
      $message = @("EOT"/L)
        '${sid}' reginfo: rule entry missing mandatory parameter \
        '${key}'!
        |-EOT
      fail($message)
    }
  }
  # Check the content of all present keys
  $rule.each |$key, $value| {
    $type = $type_map[$key]['type']
    $type_name = $type_map[$key]['name']
    unless($value =~ $type) {
      $message = @("EOT"/L)
        '${sid}' reginfo: rule entry parameter '${key}' must be \
        type '${type_name}'!
        |-EOT
      fail($message)
    }
  }

  # Construct the full rule line
  $mode = $rule['MODE']
  $tp = $rule['TP']
  $base_rule = "${mode} TP=${tp}"
  if 'NO' in $rule {
    $rule_no = " NO=${rule['NO']}"
  } else {
    $rule_no = ''
  }

  if 'NO' in $rule and $tp =~ /[*]/ {
    $message = @("EOT"/L)
      '${sid}' reginfo: instance count '${rule['NO']}' cannot be specified with wildcard \
      target program! Rule for '${rule['TP']}'!
      |-EOT
    fail($message)
  }

  if 'HOST' in $rule {
    $rule_host = " HOST=${rule['HOST'].join(',')}"
  } else {
    $rule_host = ''
  }

  if 'ACCESS' in $rule {
    $rule_access = " ACCESS=${rule['ACCESS'].join(',')}"
  } else {
    $rule_access = ''
  }

  if 'CANCEL' in $rule {
    $rule_cancel = " CANCEL=${rule['CANCEL'].join(',')}"
  } else {
    $rule_cancel = ''
  }
  $full_rule = "${base_rule}${rule_no}${rule_host}${rule_access}${rule_cancel}"
-%>
<%= $full_rule %>
<% } -%>
# Ending Ruleset
<%
if $support_internal_keyword { -%>
P TP=* HOST=internal,local CANCEL=internal,local ACCESS=internal,local
<% } else { -%>
P TP=* HOST=<%= $internal_hosts.join(',') %> CANCEL=<%= $internal_hosts.join(',') %> ACCESS=<%= $internal_hosts.join(',') %>
P TP=* HOST=local CANCEL=<%= $internal_hosts.join(',') %> ACCESS=<%= $internal_hosts.join(',') %>
<% } -%>
