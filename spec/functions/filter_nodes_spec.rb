require 'spec_helper'

# Note that testing parameters to functions is weird.
# * It does not want the name of the parameter!
# * Weird ruby nonsense means we don't need to put the nodes in a 'hash'

describe 'sap::cluster::filter_nodes' do
  context 'two nodes' do
    it 'returns only the matching node' do
      is_expected.to run.with_params(
        'db-standard',
        'node1' => [
          'app-standard',
        ],
        'node2' => [
          'app-standard',
          'db-standard',
        ],
      ).and_return(['node2'])
    end

    it 'returns an empty array when no node matches' do
      is_expected.to run.with_params(
        'app-apo-opt',
        'node1' => [
          'app-standard',
        ],
        'node2' => [
          'db-standard',
        ],
      ).and_return([])
    end

    it 'returns both nodes when they match' do
      is_expected.to run.with_params(
        'app-standard',
        'node1' => [
          'app-standard',
        ],
        'node2' => [
          'app-standard',
          'db-standard',
        ],
      ).and_return(['node1', 'node2'])
    end
  end

  context 'asymetrical node list' do
    it 'returns the matching subset' do
      is_expected.to run.with_params(
        'db-standard',
        'appnode1' => [
          'app-standard',
        ],
        'appnode2' => [
          'app-standard',
        ],
        'dbnode1' => [
          'db-standard',
        ],
        'dbnode2' => [
          'db-standard',
        ],
      ).and_return(['dbnode1', 'dbnode2'])
    end
  end
end
