# @summary Creates Corosync/Pacemaker cluster rules for a filesystem resource.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally.
#
# @param sid
#   System ID this instance is associated with
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
#
# @param lvm_primitives
#   Array containing the list of lvm primitives managed by the parent type. It
#   must contain an entry which matches the expected primitive described by the
#   volume_group parameter for this filesystem.
#
# @param volume_group
#   Name of the LVM volume group to be administered. Used to generate the
#   device parameter.
#
# @param logical_volume
#   Name of the logical volume containing this filesystem. This is used to
#   generate the device parameter.
#
# @param fstype
#   Format of the filesystem.
#
# @param directory
#   Mountpoint where this filesystem is attached.
#
# @param device
#   Path to the underlying block device on which this filesystem resides.
#   Typically this is constructed with the volume group name and the logical
#   volume name.
#
# @example Definition for a shared commvault volume
#   sap::cluster::as_inst { 'db2pr0_lvm_0':
#     sid            => 'PR0',
#     cib_name       => 'sap',
#     volume_group   => 'db2cv',
#     logical_volume => 'db2cvarch,
#     fstype         => 'xfs',
#     directory      => '/db2/PR0/commvault/archive',
#   }
#
define sap::cluster::filesystem (
  Sap::SID $sid,
  String $cib_name,
  Array[String] $lvm_primitives,
  Sap::VgLvName $volume_group,
  Sap::VgLvName $logical_volume,
  Sap::FsType $fstype,
  Stdlib::AbsolutePath $directory,
  Stdlib::AbsolutePath $device = "/dev/${volume_group}/${logical_volume}"
) {
  # Ensure a matching lvm primitive has been calculated.
  $expected_lvm_primitive = "prm_${sid}_${volume_group}_LVM"
  unless($expected_lvm_primitive in $lvm_primitives) {
    fail("The parent LVM primitive resource describing the ${volume_group} vg must be defined for filesystem ${directory}!")
  }

  # Define the standard operation latencies
  $operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '60s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '60s',
      },
    },
    {
      'monitor' => {
        'interval' => '20s',
        'timeout'  => '40s',
      },
    },
    {
      'notify' => {
        'interval' => '0s',
        'timeout'  => '60s',
      },
    },
  ]

  $parameters = {
    'device'    => $device,
    'directory' => $directory,
    'fstype'    => $fstype,
  }

  # Construct the primitive name
  $underscore_path = regsubst($directory, '/', '_', 'G')
  # Assuming this is an absolute path there should be a leading `/` so we don't
  # need a separaiton underscore after the filesystem term 
  $primitive_name = "prm_${sid}_filesystem${underscore_path}"

  # Create the Filesystem primitive
  cs_primitive { $primitive_name:
    primitive_class => 'ocf',
    primitive_type  => 'Filesystem',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => $parameters,
    operations      => $operations,
  }
}
