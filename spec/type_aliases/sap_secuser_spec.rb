require 'spec_helper'

# See https://launchpad.support.sap.com/#/notes/614971 for detail

describe 'Sap::SecUser' do
  it 'accepts a wildcard' do
    is_expected.to allow_value('*')
  end

  it 'accepts strings that are exactly 11 characters' do
    is_expected.to allow_value('maxusername')
  end

  it 'rejects strings longer than 11 charcters' do
    is_expected.not_to allow_value('maxusername1')
  end

  it "rejects strings which include but a '*'" do
    is_expected.not_to allow_value('*aoeuaoeu', 'aoeu*aoeu')
  end
end
