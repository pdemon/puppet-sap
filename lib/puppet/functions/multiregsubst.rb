# Performs a series of substitutions on the same target string
#
# @example Two patterns
#   $pattern_map = {/_sid_/ => 'scp', /_SID_/ => 'SCP'}
#   $target = '/db2/_SID_/db2_sid_'
#   $result = multiregsubst($target, $pattern_map)
#   # The output string will be equivalent to the following
#   # $result = '/db2/SCP/db2scp'
#
Puppet::Functions.create_function(:multiregsubst) do
  # Iteratively substitutes each pattern/value pairing in target.
  # @param target The string to be substituted
  # @param substmap Hash mapping regexp to target value
  # @return [String] Returns target after all substitutions
  dispatch :multiregsubst do
    param 'String', :target
    param 'Hash[Regexp, String]', :substmap
    return_type 'String'
  end

  def multiregsubst(target, substmap)
    result = target
    substmap.each do |pattern, value|
      result = result.gsub(pattern, value)
    end
    # rubocop:disable Style/RedundantReturn:
    return result
    # rubocop:enable Style/RedundantReturn:
  end
end
