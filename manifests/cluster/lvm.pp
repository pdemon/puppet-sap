# @summary Creates Corosync/Pacemaker cluster rules for an LVM resource.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param sid
#   System ID this instance is associated with
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param volgrpname
#   Name of the LVM volume group to be administered.
#
# @param exclusive
#   When enabled the specified volume group will be exclusively activated.
#   see `pcs resource describe LVM` for details.
#
# @param partial_activation
#   When enabled corosync/pcs will attempt to activate the volume even if some
#   disks are missing. This can be extremely useful when LVM mirroring is
#   configured.
#
# @example Definition for a shared commvault volume
#   sap::cluster::as_inst { 'db2pr0_lvm_0':
#     sid                => 'PR0',
#     cib_name           => 'sap',
#     volgrpname         => 'db2cv',
#     exclusive          => true,
#     partial_activation => false,
#   }
#
define sap::cluster::lvm (
  Sap::SID $sid,
  String $cib_name,
  Sap::VgLvName $volgrpname,
  Boolean $exclusive          = true,
  Boolean $partial_activation = false,
) {
  # Define the standard operation latencies
  $operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '30s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '30s',
      },
    },
    {
      'monitor' => {
        'interval' => '10s',
        'timeout'  => '30s',
      },
    },
    {
      'methods' => {
        'interval' => '0s',
        'timeout'  => '5s',
      },
    },
  ]

  $parameters = {
    'exclusive'          => String($exclusive),
    'partial_activation' => String($partial_activation),
    'volgrpname'         => $volgrpname,
  }

  # Create the LVM primitive
  cs_primitive { "prm_${sid}_${volgrpname}_LVM":
    primitive_class => 'ocf',
    primitive_type  => 'LVM',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => $parameters,
    operations      => $operations,
  }
}
