# This class is an implementation detail and should not be called directly.
#
# @summary Creates all relevant firewall rules for this system
#
# @param system_ids
#   Sourced directly from the parent system_ids parameter on the SAP class. See
#   that class for further detail.
#
# @param service_map
#   Mapping from instance class types to the corresponding SAP firewall service
#   names. If a given instance class is detected on the server it will present
#   configure the corresponding firewall services.
#
# @param services
#   Hash defining each firewall service entry 
class sap::management::firewall (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids,
  Hash[Sap::InstClass, Array[Sap::FirewallServices]] $service_map,
  Hash[Sap::FirewallServices, Sap::FirewallRule] $services,
) {
  if $facts['sap'] != undef {
    $sid_hash = $facts['sap']['sid_hash']
    $instance_classes = $facts['sap']['inst_classes']
  } else {
    $sid_hash = {}
    $instance_classes = []
  }

  include firewalld

  # Validate firewall/inst mappings
  $service_map.each |$inst_class, $inst_services| {
    $inst_services.each |$rule_category| {
      unless($rule_category in $services) {
        $message = @("EOT"/L$)
          firewall: '${inst_class}' includes rule '${rule_category}' with no \
          corresponding definition in \$services!
          |-EOT
        fail($message)
      }
    }
  }

  # Validate firewall rule definitions
  $services.each |$rule_category, $rule_detail| {
    ['desc', 'ports'].each |$key| {
      unless($key in $rule_detail) {
        $message = @("EOT"/L$)
          firewall '${rule_category}': missing mandatory field '${key}'!
          |-EOT
        fail($message)
      }

      case $key {
        'desc': {
          $type = String
          $type_name = 'String'
        }
        'ports': {
          $type = Array[Sap::FirewallRulePort]
          $type_name = 'Array[Sap::FirewallRulePort]'
        }
        default: {} # Impossible to hit
      }

      $value = $rule_detail[$key]
      unless($value =~ $type) {
        $message = @("EOT"/L$)
          firewall '${rule_category}': field '${key}' must be of type \
          '${type_name}'!
          |-EOT
        fail($message)
      }
    }

    $ports = $rule_detail['ports']
    $ports.each |$port_hash| {
      unless('protocol' in $port_hash) {
        $message = @("EOT"/L$)
          firewall '${rule_category}': each entry in 'ports' must specify \
          a 'protocol'!
          |-EOT
        fail($message)
      }

      unless('port' in $port_hash or 'port_pattern' in $port_hash) {
        $message = @("EOT"/L$)
          firewall '${rule_category}': each entry in 'ports' must specify \
          one of either 'port' or 'port_pattern'!
          |-EOT
        fail($message)
      }

      if 'port' in $port_hash and 'port_pattern' in $port_hash {
        $message = @("EOT"/L$)
          firewall '${rule_category}': an entry in 'ports' cannot specify \
          both 'port' and 'port_pattern'!
          |-EOT
        fail($message)
      }

      $port_hash.each |$key, $value| {
        case $key {
          'protocol': {
            $type = Enum['tcp', 'udp']
            $type_name = "Enum['tcp', 'udp']"
          }
          'port_pattern', 'port': {
            $type = String
            $type_name = 'String'
            # TODO: ensure patterns with multiple occurances include the
            # position
          }
          default: {} # Impossible to hit
        }
        unless($value =~ $type) {
          $message = @("EOT"/L$)
            firewall '${rule_category}': field '${key}' of each entry in ports \
            must be of type '${type_name}'!
            |-EOT
          fail($message)
        }
      }
    }
  }

  # Rules which are not instance specific
  unless(empty($instance_classes)) {
    firewalld::custom_service { 'sap_saphostctrl':
      description => 'SAP Host Agent with SOAP/HTTP(S)',
      port        => [
        {
          'port'     => '1128:1129',
          'protocol' => 'tcp',
        },
      ],
    }
    firewalld_service { 'SAP Host Agent with SOAP/HTTP(S)':
      ensure  => 'present',
      service => 'sap_saphostctrl',
      zone    => 'public',
    }
    firewalld::custom_service { 'sap_niping':
      description => 'SAP Connection Tester',
      port        => [
        {
          'port'     => '3298',
          'protocol' => 'tcp',
        },
      ],
    }
    firewalld_service { 'SAP Connection Tester':
      ensure  => 'present',
      service => 'sap_niping',
      zone    => 'public',
    }
  }

  # Livecache is at the instance level
  if 'db-livecache' in $instance_classes {
    $lc_rule = $services['livecache']
    $lc_rule_name = 'sap_livecache'

    firewalld::custom_service { $lc_rule_name:
      description => $lc_rule['desc'],
      port        => $lc_rule['ports'],
    }

    firewalld_service { $lc_rule['desc']:
      ensure  => 'present',
      service => $lc_rule_name,
      zone    => 'public',
    }
  }

  # SAP Instances
  $sid_hash.each |$sid, $sid_data| {
    unless('instances' in $sid_data) { next() }
    $instances = $sid_data['instances']
    $instances.each |$inst, $inst_data| {
      $inst_type = $inst_data['type']
      $inst_class = $inst_data['class']
      $rules = $service_map[$inst_class]
      unless($inst_class in $service_map) { next() }
      $rules.each |$rule_category| {
        $rule = $services[$rule_category]

        $rule_name = "sap_${sid}_${inst_type}${inst}_${rule_category}"
        $description = "${rule['desc']} - ${sid} ${inst_type}${inst}"
        $ports_tmp = $rule['ports'].map |$entry| {
          $protocol = $entry['protocol']
          if 'port' in $entry {
            $port = $entry['port']
          } else {
            $port = sprintf($entry['port_pattern'], $inst)
          }

          [
            {
              'port'     => $port,
              'protocol' => $protocol,
            },
          ]
        }
        $ports = flatten($ports_tmp)

        firewalld::custom_service { $rule_name:
          description => $description,
          port        => $ports,
        }
        firewalld_service { $description:
          ensure  => 'present',
          service => $rule_name,
          zone    => 'public',
        }
      }
    }
  }

  if Sap::InstClassDb in $instance_classes {
    $database_sid_hash = $sid_hash.filter |$entry| {
      'database' in $entry[1]['instances']
    }
    $sid_list = $database_sid_hash.map |$entry| { $entry[0] }

    # Loop through the SIDs defined for this system
    $system_ids.each |$sid, $entry| {
      unless('databases' in $entry) { next() }
      $sid_lower = downcase($sid)

      $databases = $entry['databases']
      $databases.each |$db_class, $db_instance| {
        unless($db_class in $instance_classes) { next() }

        case $db_class {
          'db-db2': {
            $instance = "db2${sid_lower}"
            $com_port = Integer.new($db_instance['com_port'])
            $base_port = Integer.new($db_instance['fcm_base_port'])
            $node_count = length($db_instance['nodes'])

            # DB2 Communication Port
            firewalld::custom_service { "db2_${instance}_com_port":
              description => "DB2 Communication Port - ${sid}",
              port        => [{ port => $com_port, protocol => 'tcp' }],
            }
            firewalld_service { "DB2 Communication Port - ${sid}":
              ensure  => 'present',
              service => "db2_${instance}_com_port",
              zone    => 'public',
            }

            # DB2 Fast communication manager connections
            if $node_count > 1 {
              $fcm_end = $base_port + $node_count + 1
            } else {
              $fcm_end = $base_port + 3
            }

            firewalld::custom_service { "db2_${instance}_fcm":
              description => "DB2 Fast Communication Manager - ${sid}",
              port        => [{ port => "${base_port}:${fcm_end}", protocol => 'tcp' }],
            }
            firewalld_service { "DB2 Fast Communication Manager - ${sid}":
              ensure  => 'present',
              service => "db2_${instance}_fcm",
              zone    => 'public',
            }

            # HA related communication
            case $db_instance['ha_mode'] {
              'hadr': {
                $hadr_base_port = $db_instance['hadr_base_port']
                if $node_count > 1 {
                  $hadr_end = $hadr_base_port + $node_count - 1
                } else {
                  $hadr_end = $hadr_base_port + 1
                }

                firewalld::custom_service { "db2_${instance}_hadr":
                  description => "DB2 HADR Communication - ${sid}",
                  port        => [{ port => "${hadr_base_port}:${hadr_end}", protocol => 'tcp' }],
                }
                firewalld_service { "DB2 HADR Communication - ${sid}":
                  ensure  => 'present',
                  service => "db2_${instance}_hadr",
                  zone    => 'public',
                }
              }
              default: {} # Do nothing for unknown modes
            }
          }
          default: {} # Ignore other databases for now
        }
      }
    }
  }
}
