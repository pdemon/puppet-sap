# This type is an implementation detail and should not be used directly!
#
# @summary Declares a single directory or mountpoint.
#
# @param mount_path
#   The path to the mountpoint. Note that this is a namevar.
#
# @param file_params
#   Any valid parameter which could be provided to a File resource type can be
#   included in this hash. Note that ensure is always set to `directory`.
#
# @param count
#   Indicates that this path should be created $count times. The path must
#   contain the string `_N_` which will be substituted with the count.
#   For example `/db2/ECP/sapdata_N_` 
#   
# @param mount_params
#   TODO - fill out this section
#
# @param required_files
#   Ordered list of files which this mount point should require.
#
# @param mount_defaults
#   Hash of hashes containing mount-type specific defaults. Currently this is
#   used exclusively to provide default parameters to the NFSv4 client mounts
#   for SAP related connections.
#
# @param per_sid
#   Controls the validation behavior for the module via three possible states
#   `true` - the path is SID specific and an instance can be created for each
#   `false` - only one occurance of the path can exist on a system
#   `user` - the entry has SID specific attributes but the path is unique
#
# @param pattern_map
#   Hash mapping potential `pattern_values` to the regular expression patterns
#   which they should replace. The value retrieved by the same key in
#   `pattern_values` will be substituted into the mount_path, owner, and group
#   of the target file wherever the regular expression matches. Note that the
#   patterns should be provided as strings, however, they will be converted to
#   regexp for use via `Regexp($value)`
#
# @param pattern_values
#   Hash mapping substitution targets with their corresponding values for this
#   mountpoint instance. Typically substitutions are SID specific and are
#   specified globally via the `$sap::system_ids['pattern_map'] parameter.
define sap::config::mount_point (
  Hash[String, String] $file_params,
  Sap::MountPointSIDMode $per_sid,
  Hash[String, String] $pattern_map,
  Hash[String, String] $pattern_values,
  String $mount_path                   = $name,
  Hash $mount_params                   = {},
  Hash $mount_defaults                 = {},
  Array[String] $required_files        = [],
  Optional[Integer] $count             = undef,
) {
  # Convert the values in pattern_map to regular expressions
  $pattern_map_temp = $pattern_map.map |$entry| {
    [$entry[0], Regexp($entry[1])]
  }
  $regex_map = Hash.new(flatten($pattern_map_temp))

  # Ensure name contains '_N_' when count is specified
  if $count and $mount_path !~ /_N_/ {
    fail("mount_point: '${mount_path}' specifies \$count but does not contain '_N_'!")
  } elsif $count and $count < 1 {
    fail("mount_point: '${mount_path}' specifies \$count not >= 1!")
  }

  # Ensure we actually provided the sid_upper/sid_lower patterns and mappings
  if ($per_sid == true or $per_sid == 'user') and (!('sid_upper' in $regex_map) or
    !('sid_upper' in $pattern_values) or
    !('sid_lower' in $regex_map) or
  !('sid_lower' in $pattern_values)) {
    $message = @("EOT"/L$)
      mount_point: '${mount_path}' with per_sid mode '${per_sid}' must \
      include sid_upper and sid_lower entries in both \$pattern_map and \
      \$pattern_values!
      |-EOT

    fail($message)
  }

  # Check the file parameters specified to see if 'file type' was provided
  # If it was, ensure the parameters were configured correctly
  if 'ensure' in $file_params {
    $ensure_type = $file_params['ensure']
    case $ensure_type {
      'directory': {} # nothing special
      'link': {
        unless('target' in $file_params) {
          $message = @("EOT"/L$)
            mount_point: '${mount_path}' with file_params 'ensure' set to \
            'link' must also include 'target' parameter!
            |-EOT
          fail($message)
        }
      }
      default: {
        $message = @("EOT"/L$)
          mount_point: '${mount_path}' specifies unsupported 'ensure' value \
          '${ensure_type}'!
          |-EOT
        fail($message)
      }
    }
  } else {
    $ensure_type = 'directory'
  }

  # Ensure that the mount_path contains a _SID_ or _sid_ if an SID is specified
  if $per_sid == true and
  ($mount_path !~ $regex_map['sid_upper']) and
  ($mount_path !~ $regex_map['sid_lower']) {
    $message = @("EOT"/L)
      mount_point: SID specified for '${mount_path}' but does not match \
      '${regex_map['sid_upper']}' or '${regex_map['sid_lower']}'!
      |-EOT

    fail($message)
  }

  # Validate the provided mount parameters
  unless(empty($mount_params)) {
    unless('managed' in $mount_params) {
      fail("mount_point: '${mount_path}' does not specify 'managed'!")
    }

    # If the mount is meant to be managed retrieve the specified parameters
    if $mount_params['managed'] {
      unless('type' in $mount_params) {
        fail("mount_point: missing mount type for path '${mount_path}'!")
      }
      $mount_type = $mount_params['type']

      # If it's a valid mount type determine the required attributes
      case $mount_type {
        'nfsv4': {
          # Check for nfsv4 mandatory parameters
          $required_attributes = ['server', 'share']
        }
        default: {
          fail("mount_point: invalid mount type '${mount_type}' for path '${mount_path}'!")
        }
      }

      # Fail if required attributes are missing
      $required_attributes.each | $attribute | {
        unless($attribute in $mount_params) {
          fail("mount_point: '${mount_path}' type '${mount_type}' missing '${attribute}'!")
        }
      }
    } else {
      $mount_type = 'unmanaged'
    }
  } else {
    $mount_type = 'none'
  }

  # Build the mapping of parameters to source variables which require pattern
  # substitution.
  $subst_params_std = {
    path  => $mount_path,
    owner => $file_params['owner'],
    group => $file_params['group'],
  }
  case $mount_type {
    'nfsv4': {
      $subst_params_mount_type = {
        share => $mount_params['share'],
      }
    }
    default: {
      $subst_params_mount_type = {}
    }
  }
  case $ensure_type {
    'link': {
      $subst_params_ensure_type = { target => $file_params['target'] }
    }
    default: {
      $subst_params_ensure_type = {}
    }
  }
  $subst_params = $subst_params_std + $subst_params_mount_type + $subst_params_ensure_type

  # Build the mapping of regexp to values
  $subst_map_temp = $regex_map.map |$entry| {
    unless($entry[0] in $pattern_values) {
      fail("${name}: Unable to find value for pattern key '${entry[0]}'!")
    }
    [$entry[1], $pattern_values[$entry[0]]]
  }
  $subst_map = Hash.new(flatten($subst_map_temp))

  # Iteratively perform the substitution for each pattern
  $tmp_updated_params = $subst_params.map |$entry| {
    [$entry[0], multiregsubst($entry[1], $subst_map)]
  }
  $updated_params = Hash.new(flatten($tmp_updated_params))

  # Also perform the substitution for each required file entry
  unless(empty($required_files)) {
    $updated_required_files = $required_files.map |$entry| {
      multiregsubst($entry, $subst_map)
    }
  } else {
    $updated_required_files = []
  }

  # Construct the parameters
  case $mount_type {
    'nfsv4': {
      if 'options_nfsv4' in $mount_params {
        $mount_options_nfsv4 = $mount_params['options_nfsv4']
      } else {
        $mount_options_nfsv4 = $mount_defaults['nfsv4']['options']
      }

      # Build the NFS Mount parameters
      $params = {
        ensure        => 'mounted',
        server        => $mount_params['server'],
        atboot        => true,
        options_nfsv4 => $mount_options_nfsv4,
        mode          => $file_params['mode'],
      } + $updated_params + { mount => $updated_params['path'] } - 'path'
      # Note that Nfs::Client::Mount uses 'mount' instead of 'path'
      $resource_type = 'nfs::client::mount'
    }
    default: {
      # File entries (non-mount)
      $params = {
        ensure => $ensure_type,
        mode   => $file_params['mode'],
      } + $updated_params
      $resource_type = 'file'
    }
  }

  # Create the directory as specified or create "count" instances
  unless($count) {
    Resource[$resource_type] { $updated_params['path']:
      *      => $params,
    }

    # Dependencies - might be redundant due to auto-depend
    $updated_required_files.each | $file | {
      File[$file] -> Resource[$resource_type][$updated_params['path']]
    }
  } else {
    Integer[1, $count].each | $idx | {
      $idx_string = String.new($idx)
      $mount_path_idx = regsubst($updated_params['path'], '_N_', $idx_string, 'G')
      $count_params = $params - 'path' - 'mount'
      Resource[$resource_type] { $mount_path_idx:
        *      => $count_params,
      }

      # Count specific dependencies
      $updated_required_files.each | $file | {
        File[$file] -> Resource[$resource_type][$mount_path_idx]
      }
    }
  }
}
