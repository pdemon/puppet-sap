require 'spec_helper'
require 'deep_merge'

def db2_query_schedule_script
  <<-HEREDOC
#!/bin/bash

# File and Directory Paths -------------------------
BASENAME=$(basename "$0")

# Local Constants ----------------------------------
IPV4_PATTERN='\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b'
DB_PATTERN='\\b[A-Z0-9]+\\b'

#-------- Print Error Message -------------------------------------------------
function echoerr()
{
    printf "%s\\n" "$*" >&2;
    exit 1
}

#-------- Parse Arguments -----------------------------------------------------
# Shamelessly stolen from
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# Note that the variables in this function are intentionally globals
function parse_arguments()
{
    # Collect parameters
    PARAMS=""
    while (( "$#" ));
    do
        case "$1" in
            -h|--help)
                # Print usage and quit
                echo "${USAGE}"
                exit 0
                ;;
            -d|--database)
                # Database Name
                DATABASE=$2
                shift 2
                ;;
            -s|--schema)
                # Database Schema Name
                SCHEMA=$2
                shift 2
                ;;
            -q|--query)
                QUERY=$2
                shift 2
                ;;
            -g|--group)
                # Potentially used by query
                # shellcheck disable=SC2034
                GROUP=$2
                shift 2
                ;;
            -t|--target)
                # Potentially used by query
                # shellcheck disable=SC2034
                TARGET=$2
                shift 2
                ;;
            -a|--alias)
                CONNECTION_ALIAS=$2
                shift 2
                ;;
            --) # end argument parsing
                shift
                break
                ;;
            -*) # unsupported flags
                echoerr "${BASENAME}: Unsupported flag ${1}"
                ;;
            *) # preserve positional arguments
                PARAMS="$PARAMS $1"
                shift
                ;;
        esac
    done

    # Record positional parameters (there are none)
    eval set -- "${PARAMS}"

    # Check mandatory parameters
    if [ -z "${DATABASE}" ]; then
        echoerr "${BASENAME}: DATABASE is mandatory!"
    fi

    if [ -z "${QUERY}" ]; then
        echoerr "${BASENAME}: QUERY is mandatory!"
    fi

    # Process optional parameters
    if [ -z "${SCHEMA}" ]; then
        SCHEMA="SAP${DATABASE}"
    fi
}

#-------- Verify Environment --------------------------------------------------
function verify_environment()
{
    # Fix the path
    if ! [[ "${PATH}" =~ (^|:)/usr/local/bin(:|$) ]]; then
        export PATH="/usr/local/bin:${PATH}"
    fi

    if ! [[ "${PATH}" =~ (^|:)/usr/sbin(:|$) ]]; then
        export PATH="/usr/sbin:${PATH}"
    fi

    # Command Definitions
    DB2=$(command -v db2)
    GETENT=$(command -v getent)
    IPROUTE=$(command -v ip)

    # Ensure all required binaries are present
    if ! [ -x "${DB2}" ]; then
        echoerr "${BASENAME}: db2 must be executable!"
    elif ! [ -x "${GETENT}" ]; then
        echoerr "${BASENAME}: getent must be executable!"
    elif ! [ -x "${IPROUTE}" ]; then
        echoerr "${BASENAME}: ip must be executable!"
    fi

    # Ensure that the specified script file exists
    if ! [ -r "${QUERY}" ]; then
        echoerr "${BASENAME}: the specified script is unreadable!"
    fi

    # We're ensuring this file exists and is readable earlier in the script
    # shellcheck disable=SC1090
    source "${QUERY}" || echoerr \\
        "${BASENAME}: failed to source script '${QUERY}'!"

    # Ensure the specified database pattern is all upper case
    if ! [[ "${DATABASE}" =~ $DB_PATTERN ]]; then
        echoerr "${BASENAME}: Invalid DATABASE: '${DATABASE}'"
    fi

    # Ensure we have either an IPv4 address or a hostname that can be resolved
    # to one unless the parameter is unset
    if [ -z "${CONNECTION_ALIAS}" ]; then
        IP_ALIAS='false'
    elif [[ "${CONNECTION_ALIAS}" =~ $IPV4_PATTERN ]]; then
        IP_ALIAS="${CONNECTION_ALIAS}"
    elif RESOLVE="$("${GETENT}" hosts "${CONNECTION_ALIAS}")"; then
        IP_ALIAS=$(echo "${RESOLVE}" | awk '{print $1}')
    else
        echoerr "${BASENAME}: Invalid IP_ALIAS: '${IP_ALIAS}'"
    fi

    # Ensure the 'execute_query' function is defined
    if ! LC_ALL=C type -t execute_query | grep -q function; then
        echoerr "${BASENAME}: script '${QUERY}' must declare a function" \\
            "'execute_query'!"
    fi

    # If a target file was specified, ensure it is writable
    if [ -n "${TARGET}" ]; then
        check_target_file
    fi
}

#-------- Check IP Alias ------------------------------------------------------
# Skip if IP Alias is Missing
function check_ip_alias()
{
    local ALIAS=$1
    # Do nothing if the alias is false
    if [ "${ALIAS}" == 'false' ]; then
        return
    fi

    if ! "${IPROUTE}" -4 addr | grep "${ALIAS}" >/dev/null; then
        echo "${BASENAME}: alias '${ALIAS}' not present, skipping!"
        exit 0
    fi
}

#-------- Check IP Alias ------------------------------------------------------
# Used by execute_query implementations to verify that the target file is
# writable. This isn't always necessary, but it can be.
function check_target_file()
{
    local TARGET_DIR=""
    TARGET_DIR=$(dirname "${TARGET}")

    # Ensure we can write to the directory containing the target file
    if ! [ -w "${TARGET}" ]; then
        # If the target file exists and it's not writable we have a problem
        if [ -f "${TARGET}" ]; then
            echoerr "${BASENAME}: target '${TARGET}' must be writable!"
        fi

        # If target doesn't exist, but the parent directory does and is writable
        # we're OK, else probs
        if [ ! -d "${TARGET_DIR}" ] && [ ! -w "${TARGET_DIR}" ]; then
            echoerr "${BASENAME}: target dir '${TARGET_DIR}' must exist and be" \\
                "writable!"
        fi
    fi
}

#======== Main control block ==================================================
# Record the parameters - intentionally multiple words
# shellcheck disable=SC2068
parse_arguments $@

verify_environment

# Check for the IP Alias
check_ip_alias "${IP_ALIAS}"

"${DB2}" connect to "${DATABASE}" || echoerr \\
    "${BASENAME}: failed to connect to db '${DATABASE}'"

execute_query

"${DB2}" terminate || echoerr \\
    "${BASENAME}: failed to terminate connection to db '${DATABASE}'"
  HEREDOC
end

describe 'sap::management::db2_queries' do
  let(:bin_dir) { '/usr/local/bin' }
  let(:query_schedules) do
    [
      {
        query_file_name: 'simple_query',
        query_content: 'puppet:///module_specific/sap/simple_query',
        cron_entry: {
          'hour' => '*',
          'minute' => '15',
        },
      },
      {
        query_file_name: 'complex_query',
        query_content: 'puppet:///module_specific/sap/complex_query',
        cron_entry: {
          'hour' => '2',
          'minute' => '30',
        },
        schema: 'SAPPRD',
        output_target: '/sapmnt/${SAPSYSTEMNAME}/interface/outbound/scm/complex_output.txt',
        output_group: 'sapsys',
        monitor_alias: 'qas-db.example.org',
      },
    ]
  end
  let(:db2_server_sidconfig) do
    {
      components: [
        'db2',
      ],
      manage_profiles: false,
      db2_query_schedules: query_schedules,
    }
  end
  let(:non_db2_server) do
    {
      components: [
        'base',
      ],
      manage_profiles: false,
      db2_query_schedules: query_schedules,
    }
  end
  let(:params) do
    {
      system_ids: {
        'QAS' => db2_server_sidconfig,
        'DAA' => non_db2_server,
      },
    }
  end

  it 'deploys the primary script' do
    is_expected.to contain_file("#{bin_dir}/db2_execute_sql").with(
      ensure: 'file',
      mode: '0755',
      content: db2_query_schedule_script,
      owner: 'root',
      group: 'root',
    )
  end

  it 'deploys the queries for db2 relevant SIDs' do
    sids = params[:system_ids]
    sids.each do |sid, sid_data|
      next unless sid_data.key?(:db2_query_schedules)
      local_db2 = sid_data[:components].include?('db2')
      schedule_base = "db2_#{sid}_schedule"
      query_directory = "/db2/db2#{sid.downcase}/queries"
      if local_db2
        is_expected.to contain_file(query_directory).with(
          ensure: 'directory',
          owner: "db2#{sid.downcase}",
          group: "db#{sid.downcase}adm",
        )
      else
        is_expected.not_to contain_file(query_directory)
      end

      sid_data[:db2_query_schedules].each_index do |idx|
        schedule = sid_data[:db2_query_schedules][idx]
        if local_db2
          is_expected.to contain_sap__management__db2_query_schedule("#{schedule_base}_#{idx}").with(
            schedule.merge(
              sid: sid,
              query_directory: query_directory,
              require: "File[#{query_directory}]",
            ),
          )
        else
          is_expected.not_to contain_sap__management__db2_query_schedule("#{schedule_base}_#{idx}")
        end
      end
    end
  end
end
