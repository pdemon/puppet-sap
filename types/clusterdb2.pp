# Used to model linux high availability rules for a HADR based DB2 cluster
# Note that this assumes a single SID per instance. (SAP typically assumes this
# too!)
type Sap::ClusterDb2 = Hash[
  Enum[
    'db_ip',            # Service IP corresponding to the database cluster
    'mode',             # Type of DB2 cluster to configure.
    'hadr_peer_window', # Peer window in seconds used by HADR
    'volumes',          # Array of LVM volume group resources colocated w/ the master
    'filesystems',      # Array of filesystem resources colocated w/ the master
  ],
  Variant[
    Stdlib::IP::Address::Nosubnet,
    Sap::ClusterDbMode,
    Integer,
    Array[Sap::ClusterLvm],
    Array[Sap::ClusterFilesystem],
  ],
]
