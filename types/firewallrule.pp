type Sap::FirewallRule = Hash[
  Enum[
    'desc',
    'ports',
  ],
  Variant[
    String,
    Array[Sap::FirewallRulePort],
  ],
]
