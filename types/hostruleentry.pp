type Sap::HostRuleEntry = Variant[
  Stdlib::IP::Address::V4::Nosubnet, # Valid IPv4 addresses without CIDR
  Stdlib::Fqdn,                      # Valid hostnames
  Pattern[/\A[*]\z/],                # Full Wildcard
  # Partial IPv4 Addresses
  Pattern[/\A([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){2}(\.[*]){1}\z/],
  Pattern[/\A([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){1}(\.[*]){2}\z/],
  Pattern[/\A([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\.[*]){3}\z/],
  # Domains with leading wildcards
  Pattern[/\A[*](\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]))*\z/], # 
]
