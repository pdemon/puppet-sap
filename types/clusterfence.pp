type Sap::ClusterFence = Hash[
  Enum[
    'type',
    'data',
  ],
  Variant[
    Sap::ClusterFenceType,
    Sap::ClusterFenceData,
  ]
]
