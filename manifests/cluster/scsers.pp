# @summary Creates Corosync/Pacemaker cluster rules for an (A)SCS/ERS instance.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param sid
#   System ID this instance is associated with
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param nodes
#   Mapping containing each a list of relevant roles for each node in the
#   cluster.
#
# @param ers_ip
#   IP address assigned to the ERS virtual host.
#
# @param ers_inst
#   Structure providing details for the ERS instance. There are up to four
#   components. Note that the first three are mandatory, however, the fourth is
#   optional.
#   * `type` - Type of ERS instance. This should pretty much always be 'ERS'
#   * `number` - Two digit instance ID 
#   * `host` - short-name of the ERS virtual host. 
#   * `legacy_start_profile` - Boolean value indicating whether this is a sap
#      instance with 'START_' profiles separate from the instance ala-pre NW 7.10
#
# @param scs_ip
#   IP address assigned to the SCS virtual host.
#
# @param scs_inst
#   Structure providing details for the SCS instance. There are up to four
#   components. Note that the first three are mandatory, however, the fourth is
#   optional.
#   * `type` - Type of SCS instance. Could be either SCS or ASCS
#   * `number` - Two digit instance ID 
#   * `host` - short-name of the SCS virtual host. 
#   * `legacy_start_profile` - Boolean value indicating whether this is a sap
#      instance with 'START_' profiles separate from the instance ala-pre NW 7.10
#
# @example Asymmetric 4-node cluster with two app relevant nodes
#   sap::cluster::scsers { 'PR0_ScsErs':
#     sid       => 'PR0',
#     cib_name  => 'sap',
#     nodes     => {
#       'pr0-app00.example.org' => ['app-scs'],
#       'pr0-app01.example.org' => ['app-scs'],
#       'pr0-dba.example.org'   => ['db-standard'],
#       'pr0-dbb.example.org'   => ['db-standard'],
#     },
#     ers_ip    => '10.0.0.11'
#     ers_inst  => {
#       'type'   => 'ERS',
#       'number' => '02',
#       'host'   => 'pr0-ers',
#     },
#     scs_ip    => '10.0.0.10'
#     scs_inst  => {
#       'type'   => 'ASCS',
#       'number' => '01',
#       'host'   => 'pr0-cs',
#     },
#   }
#
define sap::cluster::scsers (
  Sap::SID $sid,
  String $cib_name,
  Sap::ClusterNodeRoles $nodes,
  Stdlib::IP::Address::Nosubnet $ers_ip,
  Sap::InstData $ers_inst,
  Stdlib::IP::Address::Nosubnet $scs_ip,
  Sap::InstData $scs_inst,
) {
  # Constants
  $profile_dir = "/sapmnt/${sid}/profile"
  $relevant_node_role = 'app-scs'

  $relevant_nodes = sap::cluster::filter_nodes($relevant_node_role, $nodes)
  if empty($relevant_nodes) {
    fail("ClusterErsScs: ${title} no nodes with ${relevant_node_role} role specified!")
  }

  # Collect the instance dataset
  $scs_type = $scs_inst['type']
  $scs_number = $scs_inst['number']
  $scs_host = $scs_inst['host']
  $scs_instname = "${sid}_${scs_type}${scs_number}_${scs_host}"
  if $scs_inst['legacy_start_profile'] {
    $scs_start_profile =
      "${profile_dir}/START_${scs_type}${scs_number}_${scs_host}"
  } else {
    $scs_start_profile = "${profile_dir}/${scs_instname}"
  }

  $ers_type = $ers_inst['type']
  $ers_number = $ers_inst['number']
  $ers_host = $ers_inst['host']
  $ers_instname = "${sid}_${ers_type}${ers_number}_${ers_host}"
  if $ers_inst['legacy_start_profile'] {
    $ers_start_profile =
      "${profile_dir}/START_${ers_type}${ers_number}_${ers_host}"
  } else {
    $ers_start_profile = "${profile_dir}/${ers_instname}"
  }

  # Service IP resources
  $ip_operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '20s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '20s',
      },
    },
    {
      'monitor'  => {
        'interval' => '10s',
        'timeout'  => '20s',
      },
    },
  ]
  $scs_ip_prm = "prm_${sid}_${scs_type}_IP"
  cs_primitive { $scs_ip_prm:
    primitive_class => 'ocf',
    primitive_type  => 'IPaddr2',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => {
      'ip' => $scs_ip,
    },
    operations      => $ip_operations,
  }

  $ers_ip_prm = "prm_${sid}_${ers_type}_IP"
  cs_primitive { $ers_ip_prm:
    primitive_class => 'ocf',
    primitive_type  => 'IPaddr2',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => {
      'ip' => $ers_ip,
    },
    operations      => $ip_operations,
  }

  # Core resource
  $scs_operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '180s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '240s',
      },
    },
    {
      'monitor' => {
        'interval' => '30s',
        'timeout'  => '60s',
      },
    },
    {
      'monitor' => {
        'interval' => '31s',
        'timeout'  => '60s',
        'role'     => 'Slave',
      },
    },
    {
      'monitor' => {
        'interval' => '29s',
        'timeout'  => '60s',
        'role'     => 'Master',
      },
    },
    {
      'promote' => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'demote' => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'methods' => {
        'interval' => '0s',
        'timeout'  => '5s',
      },
    },
  ]
  $scs_ms_metadata = {
    'master-max'     => '1',
    'clone-max'      => '2',
    'notify'         => true,
    'interleave'     => true,
    # Possibly un-necessary, however, the SAPInstance script checks this 
    'clone-node-max' => '1',
    'master-node-max' => '1',
  }
  $scs_parameters = {
    'InstanceName'      => $scs_instname,
    'START_PROFILE'     => $scs_start_profile,
    'ERS_InstanceName'  => $ers_instname,
    'ERS_START_PROFILE' => $ers_start_profile,
  }

  $scs_primitive = "prm_${sid}_SCS"
  cs_primitive { $scs_primitive:
    primitive_class => 'ocf',
    primitive_type  => 'SAPInstance',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    promotable      => true, # Indicates this is a master/slave
    ms_metadata     => $scs_ms_metadata,
    parameters      => $scs_parameters,
    operations      => $scs_operations,
  }

  # Ensure the IPs are colocated with appropriate maste/slave resource
  cs_colocation { "colocate-${scs_ip_prm}-with-ms_${scs_primitive}_master":
    cib        => $cib_name,
    score      => '2000',
    primitives => [
      "ms_${scs_primitive}:Master",
      $scs_ip_prm,
    ],
    require    => [
      Cs_primitive[$scs_primitive],
      Cs_primitive[$scs_ip_prm],
    ],
  }
  cs_colocation { "colocate-${ers_ip_prm}-with-ms_${scs_primitive}_slave":
    cib        => $cib_name,
    score      => '2000',
    primitives => [
      "ms_${scs_primitive}:Slave",
      $ers_ip_prm,
    ],
    require    => [
      Cs_primitive[$scs_primitive],
      Cs_primitive[$ers_ip_prm],
    ],
  }

  # Restrict these resouces to the relevant nodes
  $relevant_nodes.each | $node | {
    cs_location { "locate-ms_${scs_primitive}-${node}":
      cib       => $cib_name,
      primitive => "ms_${scs_primitive}",
      node_name => $node,
      score     => '200',
      require   => Cs_primitive[$scs_primitive],
    }

    cs_location { "locate-${ers_ip_prm}-${node}":
      cib       => $cib_name,
      primitive => $ers_ip_prm,
      node_name => $node,
      score     => '200',
      require   => Cs_primitive[$ers_ip_prm],
    }

    cs_location { "locate-${scs_ip_prm}-${node}":
      cib       => $cib_name,
      primitive => $scs_ip_prm,
      node_name => $node,
      score     => '200',
      require   => Cs_primitive[$scs_ip_prm],
    }
  }
}
