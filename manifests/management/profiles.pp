# This class is an implementation detail and should not be called directly.
# Currently only the DEFAULT.PFL file is managed by this class.
#
# @summary Creates the profiles files in /sapmnt/SID/profile for each SID
#
# @param system_ids
#   Sourced directly from the parent system_ids parameter on the SAP class. See
#   that class for further detail.
#
class sap::management::profiles (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids,
) {
  unless($facts['sap'] == undef) {
    $instance_classes = $facts['sap']['inst_classes']
  } else {
    $instance_classes = []
  }

  # Make no attempt to manage profiles on nodes which do not have SAP components
  if Sap::InstClassSap in $instance_classes {
    $system_ids.each |$sid, $sid_data| {
      # Skip SIDs who's profiles are not supposed to be managed
      unless($sid_data['manage_profiles']) { next() }

      $sid_lower = downcase($sid)
      $sidadm = "${sid_lower}adm"
      $default_pfl_params = {
        'sid' => $sid,
        'sid_config' => $sid_data,
      }

      if 'manage_default_profile' in $sid_data {
        $manage_default_profile = $sid_data['manage_default_profile']
      } else {
        $manage_default_profile = false
      }

      if $manage_default_profile {
        file { "/sapmnt/${sid}/profile/DEFAULT.PFL":
          ensure  => file,
          owner   => $sidadm,
          group   => 'sapsys',
          mode    => '0644',
          content => epp('sap/sapmnt/SID/profile/DEFAULT_PFL.epp', $default_pfl_params),
        }
      }

      # Reginfo / Secinfo
      # TODO: detect abap version and use local/internal when possible
      # TODO: Handle old J2EE / Gateway instances that are too old to have the
      # See https://launchpad.support.sap.com/#/notes/1809896 for detail
      $instances = $sid_data['instances']
      if 'cs-abap' in $instances {
        $support_internal_keyword = false
      } else {
        $support_internal_keyword = true
      }

      # Build a list off all internal hosts for this SID
      $internal_hosts_tmp = $instances.map |$inst| {
        $types = $inst[1]['types']
        $node_list = $types.map |$type| {
          $type[1]
        }
      }
      $internal_hosts = unique(flatten($internal_hosts_tmp))

      if 'sec_info_rules' in $sid_data {
        $sec_info_rules = $sid_data['sec_info_rules']
      } else {
        $sec_info_rules = []
      }
      $sec_info_params = {
        'sid' => $sid,
        'support_internal_keyword' => $support_internal_keyword,
        'internal_hosts' => $internal_hosts,
        'rules' => $sec_info_rules,
      }

      file { "/sapmnt/${sid}/global/secinfo":
        ensure  => file,
        owner   => $sidadm,
        group   => 'sapsys',
        mode    => '0644',
        content => epp('sap/sapmnt/SID/global/secinfo.epp', $sec_info_params),
      }

      if 'reg_info_rules' in $sid_data {
        $reg_info_rules = $sid_data['reg_info_rules']
      } else {
        $reg_info_rules = []
      }

      $reg_info_params = {
        'sid' => $sid,
        'support_internal_keyword' => $support_internal_keyword,
        'internal_hosts' => $internal_hosts,
        'rules' => $reg_info_rules,
      }
      file { "/sapmnt/${sid}/global/reginfo":
        ensure  => file,
        owner   => $sidadm,
        group   => 'sapsys',
        mode    => '0644',
        content => epp('sap/sapmnt/SID/global/reginfo.epp', $reg_info_params),
      }
    }
  }
}
