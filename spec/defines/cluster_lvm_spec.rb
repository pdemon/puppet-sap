require 'spec_helper'

describe 'sap::cluster::lvm' do
  let(:title) { 'db2pr0_lvm_0' }
  let(:params) do
    {
      sid: 'PR0',
      cib_name: 'sap',
      volgrpname: 'db2cv',
    }
  end

  it 'constructs the primitive as expected' do
    operations = [
      {
        'start' => {
          'interval' => '0s',
          'timeout'  => '30s',
        },
      },
      {
        'stop' => {
          'interval' => '0s',
          'timeout'  => '30s',
        },
      },
      {
        'monitor' => {
          'interval' => '10s',
          'timeout'  => '30s',
        },
      },
      {
        'methods' => {
          'interval' => '0s',
          'timeout'  => '5s',
        },
      },
    ]
    is_expected.to contain_cs_primitive('prm_PR0_db2cv_LVM').with(
      primitive_class: 'ocf',
      primitive_type: 'LVM',
      provided_by: 'heartbeat',
      cib: 'sap',
      parameters: [
        'volgrpname' => 'db2cv',
        'exclusive' => true,
        'partial_activation' => false,
      ],
      operations: operations,
    )
  end
end
