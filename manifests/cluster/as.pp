# @summary Creates Corosync/Pacemaker cluster rules for an AS instance.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param sid
#   System ID this instance is associated with

# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param nodes
#   Mapping containing each a list of relevant roles for each node in the
#   cluster.
#
# @param as_instances
#   Each entry in this array corresponds to a unique standalone instance which
#   is linked to a home host. Entries must have the following three values:
#   * `type' - 'Name' of the instance type. For example 'D', 'DVEBMGS', or 'J'
#   * `number' - two digit code assigned to the instance.
#   * `host' - short hostname used to install the instance.
#
# @param db_mode
#   Specifies the type of clustering used for the underlying database. This
#   information is use to construct the start-after relationship on the core
#   database PCS resource.
#
# @example Asymmetric 4-node cluster defining the AS resource for each app node
#   sap::cluster::as { 'PR0_As':
#     sid          => 'PR0',
#     cib_name     => 'sap',
#     nodes    => {
#       'pr0-app00.example.org' => ['app-standard'],
#       'pr0-app01.example.org' => ['app-standard'],
#       'pr0-dba.example.org'   => ['db-standard'],
#       'pr0-dbb.example.org'   => ['db-standard'],
#     },
#     as_instances => [
#       {
#         'type'   => 'DVEBMGS',
#         'number' => '00',
#         'host'   => 'pr0-app00',
#       },
#       {
#         'type'   => 'D',
#         'number' => '00',
#         'host'   => 'pr0-app01',
#       },
#     ],
#   }
#
define sap::cluster::as (
  Sap::SID $sid,
  String $cib_name,
  Sap::ClusterNodeRoles $nodes,
  Sap::ClusterDbMode $db_mode,
  Array[Sap::InstData] $as_instances,
) {
  # Constants
  $profile_dir = "/sapmnt/${sid}/profile"
  $relevant_node_role = 'app-standard'

  $relevant_nodes = sap::cluster::filter_nodes($relevant_node_role, $nodes)
  if empty($relevant_nodes) {
    fail("ClusterAs: ${title} no nodes with ${relevant_node_role} role specified!")
  }

  # Calculate the SCS instance resource name so we can create the start/stop
  # relationship between each application server and the target node
  $scs_primitive = "ms_prm_${sid}_SCS"

  # For each specified application server instance
  unless(empty($as_instances)) {
    $as_instances.each | $index, $as_inst | {
      sap::cluster::as_inst { "as_inst_${sid}_${index}":
        sid            => $sid,
        cib_name       => $cib_name,
        db_mode        => $db_mode,
        profile_dir    => $profile_dir,
        relevant_nodes => $relevant_nodes,
        *              => $as_inst,
      }
    }
  } else {
    fail("ClusterAs: ${title} at least one AS instance must be specified!")
  }
}
