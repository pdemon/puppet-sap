# @summary Configures a node with the appropriate SAP cluster resources
#
# Cluster class which configures a PCS/Corosync based ha-environment. Note that
# it is assumed that the puppet-corosync class is defined and the cluster is
# configured before this class is configured.
#
# Additionally, the cluster must be an Asymmetrical "Opt-In" cluster. This
# configuration operates under that assumption and sets cluster level parameters
# appropriately.
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
#
# @param resource_stickiness
#   Global configuration setting controlling how much a given resource wants to
#   'stay' where it is as opposed to moving to a 'home' node.
# 
# @param enable_fencing
#   Controls whether the cluster assumes Stonith should be enabled or not.
#
# @param sid_rules
#   A hash mapping each cluster relevant SID to two mandatory components:
#   *node_roles*: a complete list of PCS nodes mapped to their roles
#   *components*: cluster elements for this instance with their data
#
# @param fence_agents
#   An array of Sap::ClusterFence definitions which provide fencing coverage for
#   all nodes defined by this cluster. Note that this parameter becomes
#   mandatory when enable_fencing is true. Additionally, every node referenced
#   within the sid_rules structure must have exactly one corresponding entry in
#   the pcmk_host_map structure of a single agent defined here.
class sap::cluster (
  String $cib_name,
  Integer $resource_stickiness           = 500,
  Boolean $enable_fencing                = false,
  Sap::SIDClusterData $sid_rules         = {},
  Array[Sap::ClusterFence] $fence_agents = [],
) {
  $cluster_sid_list = $sid_rules.map |$entry| { $entry[0] }

  # Validate the SID Rules structure and build a list of nodes
  $node_list_tmp = $sid_rules.map | $entry | {
    $sid = $entry[0]
    $sid_data = $entry[1]

    # Ensure we have the necessary parameters
    ['node_roles', 'components'].each |$key| {
      unless( $key in $sid_data ) {
        fail("cluster ${sid}: '${key}' parameter is mandatory!")
      }
    }
    $sid_data['node_roles'].keys()
  }
  $node_list = unique(flatten($node_list_tmp))

  class { 'sap::cluster::prepare':
    cluster_sid_list => $cluster_sid_list,
  }
  contain 'sap::cluster::prepare'

  # Start building the cluster information base
  cs_shadow { $cib_name: }

  # Opt-in cluster - resources can only run on nodes we explicitly specify
  cs_property { 'symmetric-cluster':
    value => 0,
    cib   => $cib_name,
  }

  # Management of STONITH for fencing
  if $enable_fencing {
    $stonith_enabled_value = 'true' # lint:ignore:quoted_booleans

    # Ensure at least one agent is defined
    if empty($fence_agents) {
      $message = @("EOT"/L)
        cluster: at least one fence agent must be defined when fencing is \
        enabled!
        |-EOT
      fail($message)
    }

    # Ensure that every node is fenced by exactly one agent
    $fence_node_tmp = $fence_agents.map |$entry| {
      # Ensure both the type and data are present
      ['type', 'data'].each |$key| {
        unless($key in $entry) {
          $message = @("EOT"/L)
            cluster: fence_agent entry missing '${key}'!
            |-EOT
          fail($message)
        }
      }

      unless('pcmk_host_map' in $entry['data']) {
        $message = @("EOT"/L)
          cluster: all currently supported fence agents require \
          'pcmk_host_map' to be defined!
          |-EOT
        fail($message)
      }
      $entry['data']['pcmk_host_map'].keys()
    }
    $fence_node_list = flatten($fence_node_tmp)

    $node_list.each |$node| {
      $node_count = count($fence_node_list, $node)
      if $node_count == 0 {
        $fail = true
        $message = @("EOT"/L)
          cluster: node '${node}' must be targeted by exactly one fence agent!
          |-EOT
      } elsif $node_count > 1 {
        $fail = true
        $message = @("EOT"/L)
          cluster: node '${node}' cannot be targeted by more than one fence \
          agent!
          |-EOT
      } else {
        $fail = false
      }
      if $fail { fail($message) }
    }

    # Assuming everything is good, construct the fencing!
    $fence_agents.each |$index, $entry| {
      $type = $entry['type']
      $data = $entry['data']
      case $type {
        'fence_lpar': {
          sap::cluster::fence_lpar { "${type}_${index}":
            cib_name  => $cib_name,
            node_list => $node_list,
            *         => $data,
          }
        }
        default: {} # do nothing if we don't understand - this is impossible
      }
    }
  } else {
    $stonith_enabled_value = 'false' # lint:ignore:quoted_booleans
  }

  cs_property { 'stonith-enabled':
    value => $stonith_enabled_value,
    cib   => $cib_name,
  }

  # Configure how much resources want to stick in place by default
  cs_rsc_defaults { 'resource-stickiness':
    value => $resource_stickiness,
    cib   => $cib_name,
  }

  # Construct the appropriate ruleset for each SID
  unless(empty($sid_rules)) {
    $sid_rules.each | $sid, $sid_data | {
      $node_roles = $sid_data['node_roles']
      $components = $sid_data['components']

      # Ensure we have the required roles defined
      $defined_cluster_types = $components.map |$entry| {
        # Every component must have a type!
        if ! ( 'type' in $entry) {
          fail("cluster ${sid}: component missing 'type'!")
        }
        $entry['type']
      }
      # Validate AS dependencies
      if 'As' in $defined_cluster_types {
        if ! ('ScsErs' in $defined_cluster_types) {
          fail("cluster ${sid}: when As type is defined ScsErs is mandatory!")
        } elsif ! ('Db2' in $defined_cluster_types) {
          fail("cluster ${sid}: when As type is defined Db2 is mandatory!")
        }
      }

      unless(empty($components)) {
        $components.each | $component | {
          # Every component must have corresponding data
          if ! ( 'data' in $component ) {
            fail("cluster ${sid}: component missing 'data'!")
          }

          $comp_type = $component['type']
          $comp_data = $component['data']

          case $comp_type {
            'ScsErs': {
              sap::cluster::scsers { "${sid}_ScsErs":
                sid      => $sid,
                cib_name => $cib_name,
                nodes    => $node_roles,
                before   => Cs_commit[$cib_name],
                *        => $comp_data,
              }
            }
            'As': {
              # Retrieve the DB detail and pass it's mode to the As resources
              $db_component = $components.filter |$component| {
                $component['type'] =~ /Db2/
              }
              $db_cluster_mode = $db_component[0]['data']['mode']
              sap::cluster::as { "${sid}_As":
                sid      => $sid,
                cib_name => $cib_name,
                nodes    => $node_roles,
                before   => Cs_commit[$cib_name],
                db_mode  => $db_cluster_mode,
                *        => $comp_data,
              }
            }
            'Db2': {
              sap::cluster::db2 { "${sid}_Db2":
                sid      => $sid,
                cib_name => $cib_name,
                nodes    => $node_roles,
                before   => Cs_commit[$cib_name],
                *        => $comp_data,
              }
            }
            default: {}
          }
        }
      }
    }
  }

  # Finalize all CIB changes
  # Ensure the prepare steps happen before these changes are merged
  cs_commit { $cib_name:
    require => Class['sap::cluster::prepare'],
  }
}
