# @summary Configures the SAP Router service
class sap::service::router {
  # SAP Router service configuration
  service { $sap::params::service_saprouter:
    ensure => 'running',
    enable => true,
  }
}
