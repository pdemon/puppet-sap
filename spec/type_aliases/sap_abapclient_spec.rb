require 'spec_helper'

describe 'Sap::AbapClient' do
  it 'accepts strings that are exactly 3 characters' do
    is_expected.to allow_value('400')
  end

  it 'rejects strings longer than 3 charcters' do
    is_expected.not_to allow_value('4000')
  end

  it 'rejects strings shorter than 3 charcters' do
    is_expected.not_to allow_value('40')
  end

  it 'rejects strings which include non-numeric characters' do
    is_expected.not_to allow_value('a00', 'cat', '03p', '03_')
  end
end
