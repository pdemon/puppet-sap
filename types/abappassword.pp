# Ideally: type Sap::ABAPPassword = Sensitive[Pattern[/\A[[:graph:]]{1,40}\z/]]
type Sap::ABAPPassword = Sensitive[String]
