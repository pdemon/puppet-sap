require 'facter'

# Module containing code for constructing facts about the SAP components on this
# system. Note that this structure is primarily a mechanism to allow rspec based
# testing of the fact code.
module Puppet::Sap
  # COntains the control code needed to actually add facts. Note that the fact
  # collection code is primarily located in the inner module `Instances`.
  module Facts
    # Calls the Facter DSL and dynamically adds the local facts.
    #
    # This helps facilitate testing of the ruby code presented by this module
    #
    # @return [NilClass]
    def self.install
      Puppet::Sap::Facts::Instances.initialize
      sid_data = Puppet::Sap::Facts::Instances.sid_data
      inst_classes = Puppet::Sap::Facts::Instances.inst_classes

      Facter.add(:sap, type: :aggregate) do
        confine kernel: 'Linux'

        unless sid_data.empty?
          chunk(:sid_hash) do
            { sid_hash: sid_data }
          end
        end

        unless inst_classes.empty?
          chunk(:inst_classes) do
            { inst_classes: inst_classes }
          end
        end
      end
    end

    # Honestly, I shamelessly stole this structure from the puppet-jenkins module.
    # This component contains all of the actual code generating fact data for a
    # given host.
    module Instances
      @inst_classes = []
      @sid_data = {}

      # Constant paths
      SAP_DIR = '/usr/sap'.freeze
      DB2_DIR = '/db2'.freeze
      SCMOPT_DIR = '/scmopt'.freeze
      LC_DIR = '/sapdb'.freeze
      LC_DATA_DIR = '/sapdb/data'.freeze

      # Constant Patterns
      def self.sid?(str)
        str =~ %r{[A-Z0-9]{3}}
      end

      # Bastardized initialization method which triggers all of the data
      # extractions for this fact.
      def self.initialize
        check_sap_instances
        check_db2
        check_livecache
        check_scmopt
      end

      # Add the specified SID to the current structure unless it's already
      # present
      def self.add_sid(sid)
        return if @sid_data.key?(sid)
        inst = {}
        inst[:instances] = {}
        @sid_data[sid] = inst
      end

      # Identify all local db2 instances and collect relevant details
      def self.check_db2
        entries = list_dir(DB2_DIR)
        entries.each do |entry|
          next unless sid?(entry)
          sid = entry.strip
          inst_data = {}

          inst_data[:type] = 'db2'
          inst_data[:class] = 'db-db2'
          @inst_classes << inst_data[:class]

          # TODO: get the version of db2 from /db2/db2#{sid}/sqllib/bin/db2level

          add_sid(sid)
          @sid_data[sid][:instances][:database] = inst_data
        end
      end

      # Indentify all local liveCache components (sapdb) and return relevant
      # details
      def self.check_livecache
        entries = list_dir(LC_DIR)
        return if entries.empty?

        # Determine the SID for this local livecache data
        sid = get_file_sid(LC_DATA_DIR)
        return if sid.nil?
        add_sid(sid)
        inst_data = {}
        inst_data[:type] = 'livecache'

        # Check to see if the livecache client is present
        if entries.include?('programs') && entries.include?('data')
          # TODO: capture the version via the following
          # /sapdb/programs/bin/sdbverify -V

          @inst_classes << 'db-livecache-client'
          inst_data[:client] = true
        end

        # Detect the presence of a database instance
        databases = {}
        entries.each do |entry|
          next unless sid?(entry)

          # TODO: capture the version via the following
          # /sapdb/$inst/db/pgm/kernel -V

          @inst_classes << 'db-livecache'
          databases[entry] = true
        end

        inst_data[:class] = if databases.empty?
                              'db-livecache-client'
                            else
                              'db-livecache'
                            end

        inst_data[:databases] = databases unless databases.empty?
        @sid_data[sid][:instances][:livecache] = inst_data
      end

      # Check for the presence of an SCM optimizer install
      def self.check_scmopt
        dir = SCMOPT_DIR
        # Capture the SID
        sid = get_file_sid(dir)
        return if sid.nil?

        # Record the class presence
        inst_class = 'scmopt'
        @inst_classes << inst_class

        # Capture the instance details
        add_sid(sid)
        inst_data = {}
        inst_data[:type] = 'scmopt'
        inst_data[:class] = inst_class
        entries = list_dir(dir)
        optimizers = []
        entries.each do |entry|
          next unless entry.match?(%r{svr$})
          optimizers.push(entry.strip)
        end
        inst_data[:optimizers] = optimizers
        @sid_data[sid][:instances][:scmopt] = inst_data
      end

      # Constructs a listing of all local instances via the contents of the
      # /usr/sap directory.
      #
      # @return [Array[String]] Array listing the contents
      def self.check_sap_instances
        entries = list_dir(SAP_DIR)
        entries.each do |entry|
          add_sid(entry.strip) if sid?(entry)
        end

        @sid_data.each do |sid, data|
          data[:instances] = check_sap_sid(sid)
        end
      end

      # Determine the local instance details for a given SAP SID
      # Returns a hash containing the following detail for each instance number
      # * `type` - SAP instance type name, e.g. 'ERS'
      # * `profiles` - Array of Absolute Paths to the profile files for this
      #
      # return [Hash[String, Hash]] Keyed by instance number
      def self.check_sap_sid(sid)
        instances = {}
        sid_dir = File.join(SAP_DIR, sid)
        entries = list_dir(sid_dir)
        entries.each do |instdir|
          inst_data = {}

          # If the entry doesn't match the format skip it
          unless %r!^(?<insttype>[A-Z]+)(?<instnum>[0-9]{2})$! =~ instdir
            next
          end

          # Capture the raw instance type
          inst_data[:type] = insttype.strip

          # Determine the component category of this instance
          inst_data[:class] = case inst_data[:type]
                              when %r{^D}
                                dual_dir = File.join(sid_dir,
                                                     instdir,
                                                     'j2ee')
                                if Dir.exist?(dual_dir)
                                  'as-dual'
                                else
                                  'as-abap'
                                end
                              when %r{^J}
                                'as-java'
                              when %r{ASCS}
                                'cs-abap'
                              when %r{^SCS}
                                'cs-java'
                              when %r{^W}
                                'webdisp'
                              when %r{^G}
                                'gateway'
                              when %r{ERS}
                                'ers'
                              end

          @inst_classes << inst_data[:class]

          # Retrieve the profile data
          inst_data[:profiles] = check_profiles(sid,
                                                insttype.strip,
                                                instnum.strip)

          # Record the instance entry
          instances[instnum.strip] = inst_data
        end

        # rubocop:disable Style/RedundantReturn
        return instances
        # rubocop:enable Style/RedundantReturn
      end

      # Identifies the profile files corresponding to the current instance
      #
      # @return [Array[String]] Array containing the absolute path to each
      # profile file
      def self.check_profiles(sid, insttype, instnum)
        files = []
        dir = File.join(SAP_DIR, sid, '/SYS/profile')
        return files unless Dir.exist?(dir)
        entries = list_dir(dir)
        entries.each do |entry|
          next unless entry.match?(%r{_#{insttype}#{instnum}_})
          files.push(File.join(dir, entry.strip))
        end

        # rubocop:disable Style/RedundantReturn
        return files
        # rubocop:enable Style/RedundantReturn
      end

      # Lists the contents of the specified directory if it exists
      #
      # @return [Array[String]] Array listing the contents
      def self.list_dir(dir)
        return [] unless Dir.exist?(dir)
        Dir.entries(dir)
      end

      # Determine SID from file owner. This works for owners which match the
      # pattern ^[a-z0-9]{3}
      #
      # @return [String] Uppercase SID based on the owner of the target
      def self.get_file_sid(file)
        return nil unless File.exist?(file)
        uid = File.stat(file).uid
        sid = Etc.getpwuid(uid).name[0, 3].upcase

        if sid?(sid)
          sid
        else
          nil
        end
      end

      # Returns a sorted unique array listing the SAP component classes present
      # on this node
      #
      # @return [Array[String]] Array listing the SAP components
      def self.inst_classes
        @inst_classes.uniq
      end

      # Returns the hash of SID data reflected by this node.
      #
      # @return [Hash[String, Hash]] SID specific hash containing details for
      # that SID
      def self.sid_data
        @sid_data
      end
    end
  end
end
