type Sap::Reginfo = Hash[
  Enum[
    'MODE', # mandatory
    'TP',   # mandatory
    'NO',
    'HOST',
    'ACCESS',
    'CANCEL',
  ],
  Variant[
    Sap::SecRuleMode,
    Sap::TargetProgram,
    Integer,
    Array[Sap::HostRuleEntry],
  ],
]
