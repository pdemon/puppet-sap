# Mapping from each node to an array listing all of the roles this node fulfills
# within this cluster. For example
# {
#   'pr0-ap00' => [ 'app-standard', 'db-standard'],
#   'pr0-ap01' => [ 'app-standard', 'db-standard'],
# }
#
type Sap::ClusterNodeRoles = Hash[
  Stdlib::Fqdn,
  Array[Sap::NodeRole],
]
