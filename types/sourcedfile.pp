type Sap::SourcedFile = Hash[
  String,
  Sap::SourceURL,
]
