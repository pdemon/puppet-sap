require 'spec_helper'

describe 'sap::cluster::scsers' do
  let(:title) { 'PR0_ScsErs' }
  let(:params) do
    {
      sid: 'PR0',
      cib_name: 'sap',
      nodes: {
        'pr0-app00.example.org' => ['app-scs'],
        'pr0-app01.example.org' => ['app-scs'],
        'pr0-dba.example.org' => ['db-standard'],
        'pr0-dbb.example.org' => ['db-standard'],
      },
    }
  end

  context 'no relevant node included' do
    before(:each) do
      params.merge!(
        ers_ip: '10.0.0.10',
        ers_inst: {
          type: 'ERS',
          number: '02',
          host: 'pr0-ers',
        },
        scs_ip: '10.0.0.11',
        scs_inst: {
          type: 'ASCS',
          number: '01',
          host: 'pr0-cs',
        },
        nodes: {
          'pr0-dbb.example.org' => [
            'db-standard',
          ],
          'pr0-dba.example.org' => [
            'db-standard',
          ],
        },
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{ClusterErsScs: #{title} no nodes with app-scs role specified!}) }
  end

  context 'valid four node cluster' do
    before(:each) do
      params.merge!(
        ers_ip: '10.0.0.10',
        ers_inst: {
          type: 'ERS',
          number: '02',
          host: 'pr0-ers',
        },
        scs_ip: '10.0.0.11',
        scs_inst: {
          type: 'ASCS',
          number: '01',
          host: 'pr0-cs',
        },
      )
    end

    it 'compiles without error' do
      is_expected.to compile.with_all_deps
    end

    it 'declares the three primitives' do
      ip_operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout'  => '20s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout'  => '20s',
          },
        },
        {
          'monitor' => {
            'interval' => '10s',
            'timeout'  => '20s',
          },
        },
      ]
      scs_operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout'  => '180s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout'  => '240s',
          },
        },
        {
          'monitor' => {
            'interval' => '30s',
            'timeout'  => '60s',
          },
        },
        {
          'monitor' => {
            'interval' => '31s',
            'timeout'  => '60s',
            'role'     => 'Slave',
          },
        },
        {
          'monitor' => {
            'interval' => '29s',
            'timeout'  => '60s',
            'role'     => 'Master',
          },
        },
        {
          'promote' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'demote' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'methods' => {
            'interval' => '0s',
            'timeout'  => '5s',
          },
        },
      ]
      is_expected.to have_cs_primitive_resource_count(3)
      is_expected.to contain_cs_primitive('prm_PR0_ASCS_IP').with(
        primitive_class: 'ocf',
        primitive_type: 'IPaddr2',
        provided_by: 'heartbeat',
        cib: 'sap',
        parameters: {
          'ip' => '10.0.0.11',
        },
        operations: ip_operations,
      )
      is_expected.to contain_cs_primitive('prm_PR0_ERS_IP').with(
        primitive_class: 'ocf',
        primitive_type: 'IPaddr2',
        provided_by: 'heartbeat',
        cib: 'sap',
        parameters: {
          'ip' => '10.0.0.10',
        },
        operations: ip_operations,
      )
      is_expected.to contain_cs_primitive('prm_PR0_SCS').with(
        primitive_class: 'ocf',
        primitive_type: 'SAPInstance',
        provided_by: 'heartbeat',
        cib: 'sap',
        promotable: true,
        ms_metadata: {
          'master-max' => '1',
          'clone-max' => '2',
          'notify' => true,
          'interleave' => true,
          'clone-node-max' => '1',
          'master-node-max' => '1',
        },
        parameters: {
          'InstanceName' => 'PR0_ASCS01_pr0-cs',
          'START_PROFILE' => '/sapmnt/PR0/profile/PR0_ASCS01_pr0-cs',
          'ERS_InstanceName' => 'PR0_ERS02_pr0-ers',
          'ERS_START_PROFILE' => '/sapmnt/PR0/profile/PR0_ERS02_pr0-ers',
        },
        operations: scs_operations,
      )
    end

    context 'with legacy start profiles' do
      before(:each) do
        params[:ers_inst].merge!(
          type: 'ERS',
          number: '02',
          host: 'pr0-ers',
          legacy_start_profile: true,
        )

        params[:scs_inst].merge!(
          type: 'ASCS',
          number: '01',
          host: 'pr0-cs',
          legacy_start_profile: true,
        )
      end

      it 'uses the other start profile format' do
        is_expected.to contain_cs_primitive('prm_PR0_SCS').with(
          parameters: {
            'InstanceName' => 'PR0_ASCS01_pr0-cs',
            'START_PROFILE' => '/sapmnt/PR0/profile/START_ASCS01_pr0-cs',
            'ERS_InstanceName' => 'PR0_ERS02_pr0-ers',
            'ERS_START_PROFILE' => '/sapmnt/PR0/profile/START_ERS02_pr0-ers',
          },
        )
      end
    end

    it 'colocates the VIP primitives with the appropriate master/slave' do
      is_expected.to have_cs_colocation_resource_count(2)
      base = "prm_#{params[:sid]}"
      prim_scs = "#{base}_SCS"
      prim_scs_ip = "#{base}_ASCS_IP"
      prim_ers_ip = "#{base}_ERS_IP"
      is_expected.to contain_cs_colocation("colocate-#{prim_scs_ip}-with-ms_#{prim_scs}_master").with(
        cib: 'sap',
        score: '2000',
        primitives: [
          "ms_#{prim_scs}:Master",
          prim_scs_ip,
        ],
      ).that_requires(["Cs_primitive[#{prim_scs}]", "Cs_primitive[#{prim_scs_ip}]"])
      is_expected.to contain_cs_colocation("colocate-#{prim_ers_ip}-with-ms_#{prim_scs}_slave").with(
        cib: 'sap',
        score: '2000',
        primitives: [
          "ms_#{prim_scs}:Slave",
          prim_ers_ip,
        ],
      ).that_requires(["Cs_primitive[#{prim_scs}]", "Cs_primitive[#{prim_ers_ip}]"])
    end

    it 'locates the primitives on the app server nodes' do
      is_expected.to have_cs_location_resource_count(6)
      base = "prm_#{params[:sid]}"
      prim_scs = "#{base}_SCS"
      prim_scs_ip = "#{base}_ASCS_IP"
      prim_ers_ip = "#{base}_ERS_IP"
      params[:nodes].each do |node, roles|
        if roles.include? 'app-scs'
          is_expected.to contain_cs_location("locate-ms_#{prim_scs}-#{node}").with(
            cib: params[:cib_name],
            primitive: "ms_#{prim_scs}",
            node_name: node,
            score: '200',
          ).that_requires("Cs_primitive[#{prim_scs}]")
          is_expected.to contain_cs_location("locate-#{prim_scs_ip}-#{node}").with(
            cib: params[:cib_name],
            primitive: prim_scs_ip,
            node_name: node,
            score: '200',
          ).that_requires("Cs_primitive[#{prim_scs_ip}]")
          is_expected.to contain_cs_location("locate-#{prim_ers_ip}-#{node}").with(
            cib: params[:cib_name],
            primitive: prim_ers_ip,
            node_name: node,
            score: '200',
          ).that_requires("Cs_primitive[#{prim_ers_ip}]")
        else
          is_expected.not_to contain_cs_location("locate-ms_#{prim_scs}-#{node}")
          is_expected.not_to contain_cs_location("locate-#{prim_scs_ip}-#{node}")
          is_expected.not_to contain_cs_location("locate-#{prim_ers_ip}-#{node}")
        end
      end
    end
  end
end
