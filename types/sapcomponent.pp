# Structure representing each valid component which could be installed on an SAP
# server. The 
# * `ads`            - Adobe Document Services
# * `base`           - Catch-all for Standard SAP instance functionality
# * `base_extended`  - Extra requirements which are shared by several advanced
#                     types
# * `bo`             - Business Objects
# * `cloudconnector` - Cloud Connector relating to HANA somehow
# * `common`         - Underlying requirements shared by all instances
# * `db2`            - Database server node hosting an IBM DB2 instance
# * `experimental`   - special features which are in development
# * 'hana'           - Database node hosting a SAP in-memory HANA instance
# * 'maxdb'          - Node hosting a maxdb database instance
# * 'livecache'      - Database node hosting a liveCache instance
# * 'router'         - Node hosting a SAP-router process
type Sap::SapComponent = Enum[
  'ads',
  'base',
  'base_extended',
  'bo',
  'cloudconnector',
  'common',
  'db2',
  'experimental',
  'hana',
  'maxdb',
  'liveCache',
  'liveCache_client',
  'router',
  'edi',
]
