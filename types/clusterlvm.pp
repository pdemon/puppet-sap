# Parameters for an LVM volume 
type Sap::ClusterLvm = Hash[
  Enum[
    'volgrpname',         # Volume group to manage
    'exclusive',          # If set the volume will be activated on only one node
    'partial_activation', # When set, the volume group will activate even if some of the physical devices are missing
  ],
  Variant[
    Sap::VgLvName,
    Boolean,
  ],
]
