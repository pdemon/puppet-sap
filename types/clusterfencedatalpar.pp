type Sap::ClusterFenceDataLpar = Hash[
  Enum[
    'identity_file',
    'ipaddr',
    'login',
    'managed',
    'pcmk_delay_max',
    'pcmk_host_map',
  ],
  Variant[
    Stdlib::AbsolutePath,
    Stdlib::Host,
    String,
    Integer,
    Sap::ClusterFenceHostMap,
  ],
]
