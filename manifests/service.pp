# @summary Configures OS level services required for SAP Netweaver instances
#
class sap::service {
  # SAP Netweaver service configuration
  service { $sap::params::service_uuidd:
    ensure => 'running',
    enable => true,
  }

  if 'cloudconnector' in $sap::enabled_components and $facts['os']['release']['major'] == '7' {
    contain sap::service::cloudconnector
  }

  if 'router' in $sap::enabled_components {
    contain sap::service::router
  }
}
