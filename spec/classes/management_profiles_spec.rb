require 'spec_helper'
require 'deep_merge'

describe 'sap::management::profiles' do
  let(:instances_abap) do
    {
      'as-abap' => {
        'number' => '00',
        'types' => {
          'D' => ['prd-app01.example.org'],
          'DVEBMGS' => ['prd-app00.example.org'],
        },
      },
      'cs-abap' => {
        'number' => '01',
        'types' => {
          'ASCS' => ['prd-app00.example.org', 'prd-app01.example.org'],
        },
        'host' => 'prd-cs.example.org',
      },
    }
  end
  let(:instances_java) do
    {
      'as-java' => {
        'number' => '10',
        'types' => {
          'J' => ['lp0-app00.example.org', 'lp0-app01.example.org'],
        },
      },
      'cs-java' => {
        'number' => '14',
        'types' => {
          'SCS' => ['lp0-app00.example.org', 'lp0-app01.example.org'],
        },
        'host' => 'lp0-cs.example.org',
      },
    }
  end
  let(:instances_gateway) do
    {
      'gateway' => {
        'number' => '60',
        'types' => {
          'G' => ['gw0-app00.example.org', 'gw0-app01.example.org'],
        },
      },
    }
  end
  let(:databases_abap) do
    {
      'db-db2' => {
        'com_port' => 5912,
        'fcm_base_port' => 5914,
        'hadr_base_port' => 55_000,
        'nodes' => [
          'prd-dba.example.org',
          'prd-dbb.example.org',
        ],
        'host' => 'prd-db.example.org',
        'ha_mode' => 'hadr',
      },
    }
  end
  let(:databases_java) do
    {
      'db-db2' => {
        'com_port' => 5912,
        'fcm_base_port' => 5914,
        'hadr_base_port' => 55_000,
        'nodes' => [
          'lp0-dba.example.org',
          'lp0-dbb.example.org',
        ],
        'host' => 'lp0-db.example.org',
        'ha_mode' => 'hadr',
      },
    }
  end
  let(:default_pfl_entries_abap) do
    {
      'em/global_area_MB' => '450',
      'rspo/global_shm/job_list' => '1500',
    }
  end
  let(:reg_info) do
    [
      {
        'MODE' => 'P',
        'TP' => '*',
        'HOST' => ['*.example.org'],
        'ACCESS' => ['*.example.org'],
        'CANCEL' => ['*.example.org'],
      },
    ]
  end
  let(:sec_info) do
    [
      {
        'MODE' => 'P',
        'TP' => '*',
        'USER' => '*',
        'HOST' => ['*.example.org'],
        'USER-HOST' => ['*.example.org'],
      },
      {
        'MODE' => 'P',
        'TP' => '*',
        'USER' => '*',
        'HOST' => ['10.48.*.*'],
        'USER-HOST' => ['10.48.*.*'],
      },
    ]
  end
  let(:params) do
    {
      system_ids: {
        'PR0' => {
          'components' => ['base'],
          'backend_databases' => ['db6'],
          'exclude_common_mount_points' => false,
          'pattern_map' => {},
          'pattern_values' => {},
          'databases' => databases_abap,
          'instances' => instances_abap,
          'primary_backend_db' => 'db-db2',
          'manage_profiles' => true,
          'manage_default_profile' => true,
          'sec_info_rules' => sec_info,
          'reg_info_rules' => reg_info,
        },
        'LP0' => {
          'components' => [
            'base',
            'base_extended',
            'ads',
          ],
          'backend_databases' => ['db6'],
          'exclude_common_mount_points' => false,
          'pattern_map' => {},
          'pattern_values' => {},
          'instances' => instances_java,
          'databases' => databases_java,
          'primary_backend_db' => 'db-db2',
          'manage_profiles' => true,
          'manage_default_profile' => true,
        },
        'GWP' => {
          'components' => ['base'],
          'backend_databases' => ['db6'],
          'exclude_common_mount_points' => false,
          'pattern_map' => {},
          'pattern_values' => {},
          'instances' => instances_gateway,
          'manage_profiles' => true,
          'manage_default_profile' => true,
        },
      },
    }
  end
  let(:facts) do
    {
      sap: {
        inst_classes: [
          'as-abap',
          'cs-abap',
        ],
      },
      networking: {
        domain: 'example.org',
      },
    }
  end
  let(:content_acl) do
    [
      "\ngw/acl_mode = 1\n",
      "\ngw/reg_info = [$][(]DIR_GLOBAL[)][$][(]DIR_SEP[)]reginfo\n",
      "\ngw/sec_info = [$][(]DIR_GLOBAL[)][$][(]DIR_SEP[)]secinfo\n",
    ]
  end
  let(:content_gateway) do
    [
      "\nsystem/type = \n",
      "\ngw/start_in_homedir = 0\n",
      "\nOS_UNICODE = uc\n",
    ]
  end
  let(:content_secinfo_ending_new) do
    [
      "\nP USER=[*] USER-HOST=internal,local HOST=internal,local TP=[*]\n",
    ]
  end
  let(:content_secinfo_ending_old) { [] }
  let(:content_reginfo_ending_new) do
    [
      "\nP TP=[*] HOST=internal,local CANCEL=internal,local ACCESS=internal,local\n",
    ]
  end
  let(:content_reginfo_ending_old) { [] }

  context 'manage_profiles is false' do
    before(:each) do
      params[:system_ids].each do |sid, _sid_data|
        params[:system_ids][sid]['manage_profiles'] = false
      end
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'does not create the DEFAULT.PFL' do
      params[:system_ids].each do |sid, _sid_data|
        is_expected.not_to contain_file("/sapmnt/#{sid}/profile/DEFAULT.PFL")
      end
    end

    it 'does not create the sec or reg info files' do
      params[:system_ids].each do |sid, _sid_data|
        is_expected.not_to contain_file("/sapmnt/#{sid}/global/secinfo")
        is_expected.not_to contain_file("/sapmnt/#{sid}/global/reginfo")
      end
    end
  end

  context 'manage_default_profile is false' do
    before(:each) do
      params[:system_ids].each do |sid, _sid_data|
        params[:system_ids][sid]['manage_default_profile'] = false
      end
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'does not create the DEFAULT.PFL' do
      params[:system_ids].each do |sid, _sid_data|
        is_expected.not_to contain_file("/sapmnt/#{sid}/profile/DEFAULT.PFL")
      end
    end

    it 'creates the reg and sec info files with the appropriate permissions' do
      params[:system_ids].each do |sid, _sid_data|
        ['secinfo', 'reginfo'].each do |file|
          info_file = "/sapmnt/#{sid}/global/#{file}"
          is_expected.to contain_file(info_file).with(
            ensure: 'file',
            mode: '0644',
            owner: "#{sid.downcase}adm",
            group: 'sapsys',
          )
        end
      end
    end
  end

  context 'manage_default_profile is unset' do
    before(:each) do
      params[:system_ids].each do |sid, _sid_data|
        params[:system_ids][sid].delete('manage_default_profile')
      end
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'does not create the DEFAULT.PFL' do
      params[:system_ids].each do |sid, _sid_data|
        is_expected.not_to contain_file("/sapmnt/#{sid}/profile/DEFAULT.PFL")
      end
    end
  end

  context 'standard Default PFL' do
    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the DEFAULT.PFL with shared content' do
      params[:system_ids].each do |sid, _sid_data|
        default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
        is_expected.to contain_file(default_pfl_file).with(
          ensure: 'file',
          mode: '0644',
          owner: "#{sid.downcase}adm",
          group: 'sapsys',
        )
        content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
        content_acl.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
        expect(content).to match(%r{\nSAPSYSTEMNAME = #{sid}\n})
      end
    end

    it 'creates the reg and sec info files with the appropriate permissions' do
      params[:system_ids].each do |sid, _sid_data|
        ['secinfo', 'reginfo'].each do |file|
          info_file = "/sapmnt/#{sid}/global/#{file}"
          is_expected.to contain_file(info_file).with(
            ensure: 'file',
            mode: '0644',
            owner: "#{sid.downcase}adm",
            group: 'sapsys',
          )
        end
      end
    end

    context 'database only node' do
      before(:each) do
        facts[:sap][:inst_classes] = ['db-db2']
      end

      it 'compiles' do
        is_expected.to compile.with_all_deps
      end

      it 'does not create the pfl, secinfo, or reginfo files' do
        params[:system_ids].each do |sid, _sid_data|
          is_expected.not_to contain_file("/sapmnt/#{sid}/profile/DEFAULT.PFL")
          ['secinfo', 'reginfo'].each do |file|
            is_expected.not_to contain_file("/sapmnt/#{sid}/global/#{file}")
          end
        end
      end
    end

    context 'Java system' do
      let(:sid) { 'LP0' }

      it 'includes all Java exclusive content' do
        mshost = params[:system_ids][sid]['instances']['cs-java']['host'].match(%r{(.+?)(?=\.)})[1]
        msinst = params[:system_ids][sid]['instances']['cs-java']['number']
        java_content = [
          "\nj2ee/scs/host = #{mshost}\n",
          "\nj2ee/scs/system = #{msinst}\n",
          "\nj2ee/ms/port = 39#{msinst}\n",
          "\nOS_UNICODE = uc\n",
          "\nsystem/type = J2EE\n",
          "\nservice/protectedwebmethods = SDEFAULT\n",
          "\nicm/HTTP/mod_0 = PREFIX=/,FILE=[$][(]DIR_GLOBAL[)]/security/data/icm_filter_rules.txt\n",
        ]
        default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
        content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
        java_content.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end
    end

    context 'Gateway system' do
      let(:sid) { 'GWP' }

      it 'includes all Gateway exclusive content' do
        default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
        content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
        content_gateway.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end
    end

    context 'ABAP system' do
      let(:sid) { 'PR0' }

      it 'includes all ABAP exclusive content' do
        mshost = params[:system_ids][sid]['instances']['cs-abap']['host'].match(%r{(.+?)(?=\.)})[1]
        msinst = params[:system_ids][sid]['instances']['cs-abap']['number']
        abap_content = [
          "\nrdisp/mshost = #{mshost}\n",
          "\nrdisp/msserv = sapms#{sid}\n",
          "\nrdisp/msserv_internal = 39#{msinst}\n",
          "\nenque/process_location = REMOTESA\n",
          "\nenque/serverhost = #{mshost}\n",
          "\nenque/serverinst = #{msinst}\n",
          "\nssf/name = SAPSECULIB\n",
          "\nsystem/type = ABAP\n",
          "\nrsec/ssfs_datapath = [$][(]DIR_GLOBAL[)][$][(]DIR_SEP[)]security[$][(]DIR_SEP[)]rsecssfs[$][(]DIR_SEP[)]data\n",
          "\nrsec/ssfs_keypath = [$][(]DIR_GLOBAL[)][$][(]DIR_SEP[)]security[$][(]DIR_SEP[)]rsecssfs[$][(]DIR_SEP[)]key\n",
        ]
        default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
        content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
        abap_content.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end

      it 'secinfo uses the old syntax' do
        local_hosts = []
        params[:system_ids][sid]['instances'].each do |_inst_class, inst_data|
          inst_data['types'].each do |_type, nodes|
            local_hosts += nodes
          end
        end
        local_hosts = local_hosts.uniq.join(',')
        info_file = "/sapmnt/#{sid}/global/secinfo"
        content = catalogue.resource('file', info_file).send(:parameters)[:content]
        content_secinfo_ending_new.each do |pattern|
          expect(content).not_to match(%r{#{pattern}})
        end
        content_secinfo_ending_old = [
          "\nP USER=[*] USER-HOST=#{local_hosts} HOST=local TP=[*]\n",
          "\nP USER=[*] USER-HOST=local HOST=#{local_hosts} TP=[*]\n",
          "\nP USER=[*] USER-HOST=local HOST=local TP=[*]\n",
        ]
        content_secinfo_ending_old.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end

      it 'reginfo uses the old syntax' do
        local_hosts = []
        params[:system_ids][sid]['instances'].each do |_inst_class, inst_data|
          inst_data['types'].each do |_type, nodes|
            local_hosts += nodes
          end
        end
        local_hosts = local_hosts.uniq.join(',')

        info_file = "/sapmnt/#{sid}/global/reginfo"
        content = catalogue.resource('file', info_file).send(:parameters)[:content]
        content_reginfo_ending_new.each do |pattern|
          expect(content).not_to match(%r{#{pattern}})
        end
        content_reginfo_ending_old = [
          "\nP TP=[*] HOST=#{local_hosts} CANCEL=#{local_hosts} ACCESS=#{local_hosts}\n",
          "\nP TP=[*] HOST=local CANCEL=#{local_hosts} ACCESS=#{local_hosts}\n",
        ]
        content_reginfo_ending_old.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end
    end

    context 'ABAP or Java system' do
      ['PR0', 'LP0'].each do |sid|
        it 'includes all content common to both abap & java systems' do
          pdb = params[:system_ids][sid]['primary_backend_db']
          dbhost = params[:system_ids][sid]['databases'][pdb]['host'].match(%r{(.+?)(?=\.)})[1]
          shared_content = [
            "\nSAPFQDN = #{facts[:networking][:domain]}\n",
            "\nSAPLOCALHOSTFULL = [$][(]SAPLOCALHOST[)].[$][(]SAPFQDN[)]\n",
            "\nenque/deque_wait_answer = TRUE\n",
            "\nSAPDBHOST = #{dbhost}\n",
            "\ndbms/type = db6\n",
            "\nj2ee/dbtype = db6\n",
            "\nj2ee/dbname = #{sid}\n",
            "\nj2ee/dbhost = #{dbhost}\n",
          ]
          default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
          content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
          shared_content.each do |pattern|
            expect(content).to match(%r{#{pattern}})
          end
        end
      end
    end

    context 'Java or Gateway System' do
      ['LP0', 'GWP'].each do |sid|
        it "#{sid} secinfo uses the new internal/local syntax" do
          local_hosts = []
          params[:system_ids][sid]['instances'].each do |_inst_class, inst_data|
            inst_data['types'].each do |_type, nodes|
              local_hosts += nodes
            end
          end
          local_hosts = local_hosts.uniq.join(',')
          info_file = "/sapmnt/#{sid}/global/secinfo"
          content = catalogue.resource('file', info_file).send(:parameters)[:content]
          content_secinfo_ending_new.each do |pattern|
            expect(content).to match(%r{#{pattern}})
          end
          content_secinfo_ending_old = [
            "\nP USER=[*] USER-HOST=#{local_hosts} HOST=local TP=[*]\n",
            "\nP USER=[*] USER-HOST=local HOST=#{local_hosts} TP=[*]\n",
            "\nP USER=[*] USER-HOST=local HOST=local TP=[*]\n",
          ]
          content_secinfo_ending_old.each do |pattern|
            expect(content).not_to match(%r{#{pattern}})
          end
        end

        it "#{sid} reginfo uses the new internal/local syntax" do
          local_hosts = []
          params[:system_ids][sid]['instances'].each do |_inst_class, inst_data|
            inst_data['types'].each do |_type, nodes|
              local_hosts += nodes
            end
          end
          local_hosts = local_hosts.uniq.join(',')

          info_file = "/sapmnt/#{sid}/global/reginfo"
          content = catalogue.resource('file', info_file).send(:parameters)[:content]
          content_reginfo_ending_new.each do |pattern|
            expect(content).to match(%r{#{pattern}})
          end
          content_reginfo_ending_old = [
            "\nP TP=[*] HOST=#{local_hosts} CANCEL=#{local_hosts} ACCESS=#{local_hosts}\n",
            "\nP TP=[*] HOST=local CANCEL=#{local_hosts} ACCESS=#{local_hosts}\n",
          ]
          content_reginfo_ending_old.each do |pattern|
            expect(content).not_to match(%r{#{pattern}})
          end
        end
      end
    end

    context 'includes additional lines' do
      let(:sid) { 'PR0' }

      before(:each) do
        params.merge(
          system_ids: {
            'PR0' => {
              'default_pfl_entries' => default_pfl_entries_abap,
            },
          },
        )
      end

      it 'includes the entries specified in default_pfl_entries' do
        default_pfl_file = "/sapmnt/#{sid}/profile/DEFAULT.PFL"
        content = catalogue.resource('file', default_pfl_file).send(:parameters)[:content]
        default_pfl_entries_abap.each do |pattern|
          expect(content).to match(%r{#{pattern}})
        end
      end
    end

    context 'addition reg/sec info lines' do
      let(:sid) { 'PR0' }

      it 'compiles succesfully with valid data' do
        is_expected.to compile.with_all_deps
      end

      context 'secinfo validation' do
        data_types = {
          'MODE' => {
            'name' => 'Sap::SecRuleMode',
            'bad_value' => '*',
          },
          'TP' => {
            'name' => 'Sap::TargetProgram',
            'bad_value' => ['*'],
          },
          'USER' => {
            'name' => 'Sap::SecUser',
            'bad_value' => ['*'],
          },
          'HOST' => {
            'name' => 'Array\[Sap::HostRuleEntry\]',
            'bad_value' => 'P',
          },
          'USER-HOST' => {
            'name' => 'Array\[Sap::HostRuleEntry\]',
            'bad_value' => 'SAPUSER',
          },
        }
        data_types.each do |key, key_data|
          it "fails when '#{key}' is not '#{key_data['name']}'" do
            params[:system_ids][sid]['sec_info_rules'][0][key] = key_data['bad_value']
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' secinfo: entry parameter '#{key}' must be type '#{key_data['name']}'!})
          end
        end

        ['MODE', 'TP', 'USER', 'HOST'].each do |key|
          it "fails when mandatory key '#{key}' is missing" do
            params[:system_ids][sid]['sec_info_rules'][0].delete(key)
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' secinfo: rule entry missing mandatory parameter '#{key}'!})
          end
        end

        it 'includes the expected lines' do
          info_file = "/sapmnt/#{sid}/global/secinfo"
          content = catalogue.resource('file', info_file).send(:parameters)[:content]
          sec_info.each do |rule|
            mode = rule['MODE']
            rule_line = mode.to_s
            ['TP', 'HOST', 'USER', 'USER-HOST'].each do |key|
              next unless rule.key?(key)
              value = case key
                      when 'HOST', 'USER-HOST'
                        rule[key].join(',')
                      else
                        rule[key]
                      end
              value.gsub!(%r{[*]}, '[*]')
              value.gsub!(%r{[.]}, '[.]')
              rule_line += " #{key}=#{value}"
            end
            expect(content).to match(%r{#{rule_line}})
          end
        end
      end

      context 'reginfo validation' do
        data_types = {
          'MODE' => {
            'name' => 'Sap::SecRuleMode',
            'bad_value' => '*',
          },
          'TP' => {
            'name' => 'Sap::TargetProgram',
            'bad_value' => ['*'],
          },
          'NO' => {
            'name' => 'Integer',
            'bad_value' => '*',
          },
          'HOST' => {
            'name' => 'Array\[Sap::HostRuleEntry\]',
            'bad_value' => 'P',
          },
          'CANCEL' => {
            'name' => 'Array\[Sap::HostRuleEntry\]',
            'bad_value' => 'P',
          },
          'ACCESS' => {
            'name' => 'Array\[Sap::HostRuleEntry\]',
            'bad_value' => 'P',
          },
        }
        data_types.each do |key, key_data|
          it "fails when '#{key}' is not '#{key_data['name']}'" do
            params[:system_ids][sid]['reg_info_rules'][0][key] = key_data['bad_value']
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' reginfo: rule entry parameter '#{key}' must be type '#{key_data['name']}'!})
          end
        end

        ['MODE', 'TP'].each do |key|
          it "fails when mandatory key '#{key}' is missing" do
            params[:system_ids][sid]['reg_info_rules'][0].delete(key)
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' reginfo: rule entry missing mandatory parameter '#{key}'!})
          end
        end

        it "fails when 'TP' includes a wildcard and 'NO' is set" do
          params[:system_ids][sid]['reg_info_rules'][0]['TP'] = '/bin/*'
          params[:system_ids][sid]['reg_info_rules'][0]['NO'] = 10
          is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' reginfo: instance count '10' cannot be specified with wildcard target program! Rule for '/bin/[*]'!})
        end

        it 'includes the expected lines' do
          info_file = "/sapmnt/#{sid}/global/reginfo"
          content = catalogue.resource('file', info_file).send(:parameters)[:content]
          reg_info.each do |rule|
            mode = rule['MODE']
            rule_line = mode.to_s
            ['TP', 'NO', 'HOST', 'ACCESS', 'CANCEL'].each do |key|
              next unless rule.key?(key)
              value = case key
                      when 'HOST', 'ACCESS', 'CANCEL'
                        rule[key].join(',')
                      else
                        rule[key]
                      end
              value.gsub!(%r{[*]}, '[*]')
              value.gsub!(%r{[.]}, '[.]')
              rule_line += " #{key}=#{value}"
            end
            expect(content).to match(%r{#{rule_line}})
          end
        end
      end
    end
  end
end
