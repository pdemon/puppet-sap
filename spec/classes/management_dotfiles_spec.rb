require 'spec_helper'

describe 'sap::management::dotfiles' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      before(:each) do
        facts.merge!(
          sap: {
            sid_hash: {
              PRD: {
                instances: {
                  '01' => {
                    type: 'ASCS',
                  },
                },
              },
            },
          },
        )
      end

      it { is_expected.to compile.with_all_deps }

      context 'present SID' do
        it { is_expected.to compile.with_all_deps }

        it 'updates the existing bash_profile' do
          is_expected.to contain_file('prdadm_bash_profile').with(
            ensure: 'file',
            path: '/home/prdadm/.bash_profile',
            owner: 'prdadm',
            group: 'sapsys',
          )
          is_expected.to contain_file('prdadm_bash_profile').with_content(
            %r{if \[ -f ~/[.]profile \]; then\n\s*source ~/[.]profile\nfi},
          )
        end
      end

      context 'database instance' do
        before(:each) do
          facts[:sap][:sid_hash][:PRD][:instances].merge!(
            database: {
              type: 'db2',
            },
          )
        end

        it { is_expected.to compile.with_all_deps }

        it 'updates both the db2prd and prdadm profiles' do
          is_expected.to contain_file('prdadm_bash_profile').with(
            ensure: 'file',
            path: '/home/prdadm/.bash_profile',
            owner: 'prdadm',
            group: 'sapsys',
          )
          is_expected.to contain_file('db2prd_bash_profile').with_content(
            %r{if \[ -f ~/[.]profile \]; then\n\s*source ~/[.]profile\nfi},
          )
          is_expected.to contain_file('db2prd_bash_profile').with(
            ensure: 'file',
            path: '/db2/db2prd/.bash_profile',
            owner: 'db2prd',
            group: 'dbprdadm',
          )
          is_expected.to contain_file('db2prd_bash_profile').with_content(
            %r{if \[ -f ~/[.]profile \]; then\n\s*source ~/[.]profile\nfi},
          )
        end
      end
    end
  end
end
