# Structure detailing the attributes of a given SAP instance class. This is
# assigned as a component of a specific SID's sidconfigentry.
# 
# Note that the 'host' parameter is optional and will be present only in the
# case that this is a service which can move between nodes.
type Sap::SIDInstance = Hash[
  Enum[
    'number',               # e.g. '02'
    'types',                # e.g { 'ASCS' => [ node1.example.com, node2.example.com ] }
    'host',                 # e.g. 'ecd-cs'
    'legacy_start_profile', # Boolean
  ],
  Variant[
    Sap::InstNumber,
    Hash[Sap::InstType, Array[Stdlib::Fqdn]],
    Stdlib::Host,
    Boolean,
  ],
]
