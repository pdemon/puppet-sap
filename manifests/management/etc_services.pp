# This class is an implementation detail and should not be called directly.
#
# @summary Creates all relevant etc services entries for this system
#
# @param message_servers
#   Sourced directly from the parent message_servers parameter on the SAP class.
#   See that for further detail.
# 
# @param system_ids
#   Sourced directly from the parent system_ids parameter on the SAP class. See
#   that for further detail.
class sap::management::etc_services (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids,
  Hash[Sap::SID, Sap::MessageServer] $message_servers,
) {
  if $facts['sap'] != undef {
    $sid_hash = $facts['sap']['sid_hash']
    $instance_classes = $facts['sap']['inst_classes']
  } else {
    $sid_hash = {}
    $instance_classes = []
  }

  # Mapping of port class to values
  $port_classes_sap = {
    'disp'     => {
      'comment'      => 'SAP System Dispatcher Port',
      'base_value'   => 3200,
      'name_pattern' => 'sapdp%02d',
    },
    'gw'       => {
      'comment'      => 'SAP System Gateway Port',
      'base_value'   => 3300,
      'name_pattern' => 'sapgw%02d',
      'prior_port'   => 'sapdp99',
    },
    'disp-sec' => {
      'comment'      => 'SAP System Dispatcher Security Port',
      'base_value'   => 4700,
      'name_pattern' => 'sapdp%02ds',
      'prior_port'   => 'sapgw99',
    },
    'gw-sec'   => {
      'comment'      => 'SAP System Gateway Security Port',
      'base_value'   => 4800,
      'name_pattern' => 'sapgw%02ds',
      'prior_port'   => 'sapdp99s',
    },
    'cs-abap'  => {
      'comment'      => 'SAP System Message Server ABAP Port',
      'base_value'   => 3600,
      'name_pattern' => 'sapms%02d',
      'prior_port'   => 'sapgw99s',
    },
    'cs-java'  => {
      'comment'      => 'SAP System Message Server Internal Port',
      'base_value'   => 3900,
      'name_pattern' => 'sapms%02di',
      'prior_port'   => 'sapms99',
    },
    'as-ctrl'        => {
      'comment'          => 'SAP HTTP Control Port',
      'base_value'       => 50013,
      'name_pattern'     => 'sapctrl%02d',
      'prior_port'       => 'sapms99i',
      'port_multiplier'  => 100,
    },
    'as-ctrls'        => {
      'comment'          => 'SAP HTTPS Control Port',
      'base_value'       => 50014,
      'name_pattern'     => 'sapctrl%02ds',
      'prior_port'       => 'sapctrl99',
      'port_multiplier'  => 100,
    },
  }

  ## Build message server lookup structures
  # This assumes validation of message_servers has already occured in the main
  $ms_aliases_tmp = $message_servers.map |$entry| {
    $number = $entry[1]['number']
    $class = $entry[1]['class']
    $sid = $entry[0]
    case $class {
      'cs-abap': { $name = "sapms${sid}" }
      'cs-java': { $name = "sapms${sid}j" }
      default: {}
    }
    if 'port_override' in $entry[1] {
      $port = $entry[1]['port_override']
    } else {
      $port = $port_classes_sap[$class]['base_value'] + $number
    }
    [
      "${number}_${sid}",
      {
        'name'  => $name,
        'port'  => $port,
        'class' => $class,
      },
    ]
  }
  $ms_aliases = Hash.new(flatten($ms_aliases_tmp))

  # Manage SAP Standard Component ports
  if Sap::InstClassSap in $instance_classes {
    $port_classes_sap.each |$class, $data| {
      $comment = $data['comment']
      $base_value = $data['base_value']
      $name_pattern = $data['name_pattern']
      if 'port_multiplier' in $data {
        $multiplier = $data['port_multiplier']
      } else {
        $multiplier = 1
      }

      # Create the default port ranges
      Integer[0, 99].each |$port| {
        $port_id = sprintf('%02d', $port)
        $port_id_previous = sprintf('%02d', $port - 1)
        $port_number = $base_value + $port * $multiplier
        $port_name = sprintf($name_pattern, $port)

        # Construct the base argument
        $params_base = {
          ensure    => 'present',
          comment   => $comment,
          protocols => { 'tcp' => $port_number },
        }

        # Require either the previous port in this section or the last
        # port of the previous section
        if $port > 0 {
          $previous_name = sprintf($name_pattern, $port - 1)
          $params_require = { require => Etc_services[$previous_name] }
        } elsif 'prior_port' in $data {
          $previous_name = $data['prior_port']
          $params_require = { require => Etc_services[$previous_name] }
        } else {
          $params_require = {}
        }

        # Check the alias hash to see if any are relevant for this port
        $port_fixed_width = sprintf('%02d', $port)
        $port_regex = Regexp.new("^${port_fixed_width}_")
        $matching_aliases = $ms_aliases.filter |$entry| {
          $entry[0] =~ $port_regex and $entry[1]['class'] == $class
        }

        if length($matching_aliases) > 0 {
          $alias_list = $matching_aliases.map |$entry| {
            $entry[1]['name']
          }
          $params_aliases = { 'aliases' => $alias_list }
        } else {
          $params_aliases = {}
        }

        $params = $params_base + $params_require + $params_aliases
        etc_services { $port_name:
          * => $params,
        }
      }

      # Comment out any conflicting standard port lines
      $line_pattern = regsubst($name_pattern, /%02d/, '[0-9]{2}')

      # Construct the substitution pattern for sed
      $sed_line_exclusion = "/^${line_pattern}/!"

      # Construct the grep patterns to check if the file still needs processing
      $grep_line_exclusion = "^${line_pattern}"

      if $base_value < 10000 {
        $range_prefix = $base_value / 100
        $sed_line_sub_pattern = 's/^([a-zA-Z0-9_-]+\s+%02d[0-9]{2}\/\w{,4}\s+.*)/#&/'
        $sed_line_sub = sprintf($sed_line_sub_pattern, $range_prefix)

        $grep_line_match_pattern = '^[a-zA-Z0-9_-]+\s+%02d[0-9]{2}\/\w{,4}\s+.*'
        $grep_line_match = sprintf($grep_line_match_pattern, $range_prefix)
        # Raw example expression: grep -vE '^sapdb[0-9]{2}s' /etc/services | grep -E '^[a-zA-Z0-9-]+\s+5[0-9]{2}13\/\w{,4}\s+.*' 
      } else {
        $base_value_string = "${base_value}" # lint:ignore:only_variable_string 
        $range_prefix = $base_value_string[0]
        $range_suffix = $base_value_string[3,4]
        $sed_line_sub_pattern = 's/^([a-zA-Z0-9_-]+\s+%d[0-9]{2}%02d\/\w{,4}\s+.*)/#&/'
        $sed_line_sub = sprintf($sed_line_sub_pattern, $range_prefix, $range_suffix)

        $grep_line_match_pattern = '^[a-zA-Z0-9_-]+\s+%d[0-9]{2}%02d\/\w{,4}\s+.*'
        $grep_line_match = sprintf($grep_line_match_pattern, $range_prefix, $range_suffix)
        # Raw example expression: grep -vE '^sapdb[0-9]{2}s' /etc/services | grep -E '^[a-zA-Z0-9-]+\s+5[0-9]{2}13\/\w{,4}\s+.*' 
      }

      $sed_expression = "'${sed_line_exclusion} ${sed_line_sub}'"
      # Raw example expression: /^sapdp[0-9]{2}s/! s/^([a-zA-Z0-9-]+\s+47[0-9]{2}\/\w{,4}\s+.*)/#&/
      # or
      # Raw example expression: /^sapdp[0-9]{2}s/! s/^([a-zA-Z0-9-]+\s+5[0-9]{2}13\/\w{,4}\s+.*)/#&/

      exec { "sed -i -r ${sed_expression} /etc/services":
        path   => '/sbin:/bin:/usr/sbin:/usr/bin',
        onlyif => [
          "test 0 -eq \$(grep -vE '${grep_line_exclusion}' /etc/services | grep -E '${grep_line_match}' >/dev/null; echo $?)",
        ],
      }
    }
  }

  # Database Connection Ports
  $system_ids.each |$sid, $entry| {
    unless('databases' in $entry) { next() }

    $databases = $entry['databases']
    $databases.each |$db_class, $db_instance| {
      case $db_class {
        'db-db2': {
          $port_name = "sapdb2${sid}"
          $comment = 'SAP DB2 Communication Port'
          $port = Integer.new($db_instance['com_port'])
        }
        default: {} # Should be impossible
      }

      etc_services { $port_name:
        ensure    => 'present',
        comment   => $comment,
        protocols => { tcp => $port },
      }
    }
  }

  if Sap::InstClassDb in $instance_classes {
    $database_sid_hash = $sid_hash.filter |$entry| {
      'database' in $entry[1]['instances']
    }
    $sid_list = $database_sid_hash.map |$entry| { $entry[0] }

    # Loop through the SIDs defined for this system
    $system_ids.each |$sid, $entry| {
      unless('databases' in $entry) { next() }
      $sid_lower = downcase($sid)

      $databases = $entry['databases']
      $databases.each |$db_class, $db_instance| {
        unless($db_class in $instance_classes) { next() }

        case $db_class {
          'db-db2': {
            $instance = "db2${sid_lower}"
            $base_port = Integer.new($db_instance['fcm_base_port'])
            $node_count = length($db_instance['nodes'])
            if $node_count > 2 {
              $fcm_last = $node_count
            } else {
              $fcm_last = 2
            }
            $fcm_range = range(1, $fcm_last)

            # DB2 FCM start, Per node (1 to N), and FCM End
            etc_services { "DB2_${instance}":
              ensure         => 'present',
              comment        => "DB2 FCM Instance Port (Range Start): ${sid}",
              protocols      => { 'tcp' => $base_port },
              enforce_syntax => false,
              before         => "Etc_services[DB2_${instance}_1]",
            }
            $fcm_range.each |$index| {
              $params_base = {
                ensure         => 'present',
                comment        => "DB2 FCM Instance Port ${index}: ${sid}",
                enforce_syntax => false,
                protocols      => { 'tcp' => $base_port + $index },
              }
              if $index > 1 {
                $previous = $index - 1
                $params_require = {
                  'require' => "Etc_services[DB2_${instance}_${previous}]",
                }
              } else {
                $params_require = {
                  'require' => "Etc_services[DB2_${instance}]",
                }
              }
              $params = $params_base + $params_require
              etc_services { "DB2_${instance}_${index}":
                * => $params,
              }
            }
            etc_services { "DB2_${instance}_END":
              ensure         => 'present',
              comment        => "DB2 FCM Instance Port (Range End): ${sid}",
              enforce_syntax => false,
              protocols      => { 'tcp' => $base_port + $fcm_last + 1 },
              require        => "Etc_services[DB2_${instance}_${fcm_last}]",
            }

            # Extra service definitions needed for HA
            case $db_instance['ha_mode'] {
              'hadr': {
                $hadr_base_port = $db_instance['hadr_base_port']
                $hadr_range = range(0, $node_count - 1)
                $hadr_range.each |$index| {
                  $node = $db_instance['nodes'][$index]
                  $params_base = {
                    ensure         => 'present',
                    comment        => "HADR port for ${node}",
                    enforce_syntax => false,
                    protocols      => { 'tcp' => $hadr_base_port + $index },
                  }
                  if $index > 0 {
                    $params_require = {
                      'require' => "Etc_services[${sid}_HADR_${index}]",
                    }
                  } else {
                    $params_require = {
                      'require' => "Etc_services[DB2_${instance}_END]",
                    }
                  }
                  $params = $params_base + $params_require
                  $index_id = $index + 1
                  etc_services { "${sid}_HADR_${index_id}":
                    * => $params,
                  }
                }
              }
              default: {} # Do nothing for unknown modes
            }
          }
          default: {} # Ignore other databases for now
        }
      }
    }
  }

  # SAP Hostctrl if any instances are present
  if length($instance_classes) > 0 {
    etc_services { 'saphostctrl':
      ensure    => 'present',
      comment   => 'SAP Host Agent with SOAP/HTTP',
      protocols => { 'tcp' => 1128 },
    }
    etc_services { 'saphostctrls':
      ensure    => 'present',
      comment   => 'SAP Host Agent with SOAP/HTTPS',
      protocols => { 'tcp' => 1129 },
    }
  }

  # LiveCache Communication
  if /livecache/ in $instance_classes {
    etc_services { 'sql30':
      ensure    => 'present',
      comment   => 'MaxDB/liveCache Connection Port',
      protocols => { 'tcp' => 7200 },
    }
    etc_services { 'sql6':
      ensure    => 'present',
      comment   => 'MaxDB/liveCache Connection Port',
      protocols => { 'tcp' => 7210 },
    }
    etc_services { 'sapdbni72':
      ensure    => 'present',
      comment   => 'MaxDB/liveCache Port',
      protocols => { 'tcp' => 7269 },
    }
    etc_services { 'sdbnissl76':
      ensure    => 'present',
      comment   => 'MaxDB/liveCache Port',
      protocols => { 'tcp' => 7270 },
    }
    etc_services { 'SDB':
      ensure         => 'present',
      comment        => 'MaxDB/liveCache Webtools Port',
      enforce_syntax => false,
      protocols      => { 'tcp' => 7575 },
    }
  }
}
