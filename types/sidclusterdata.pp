type Sap::SIDClusterData = Hash[
  Sap::SID,
  Hash[
    Enum['node_roles', 'components'],
    Variant[
      Sap::ClusterNodeRoles,
      Array[Sap::ClusterComponent],
    ],
  ],
]
