# Creates and manages mountpoints relevant to each instance type.
class sap::config::mount_points {
  $mount_components = $sap::enabled_components + ['common']
  $sap::mount_points.each | $component, $mpaths | {
    unless($component in $mount_components) { next() }
    # Configure the paths for each enabled component
    $mpaths.each |$base_path, $parameters| {
      # Validate the mountpoint definition
      unless('per_sid' in $parameters) {
        fail("mount_point: '${component}' path '${base_path}' missing 'per_sid'!")
      }
      $per_sid = $parameters['per_sid']

      # Ensure file parameters were specified - this is mandatory
      unless('file_params' in $parameters) {
        fail("mount_point: '${component}' path '${base_path}' missing 'file_params'!")
      }

      # Per SID entries have the upper and lower case versions of the SID
      # substituted into the path and into the user and group names. 
      case $per_sid {
        # Iterate through each SID
        true, 'user': {
          $sap::system_ids.each |$sid_entry| {
            $sid = $sid_entry[0]
            $sid_components = $sid_entry[1]['components']
            # Skip SID specific mount-points for non-relevant SIDs
            case $component {
              'common': {
                # Skip SID specific mount-points for the common component if the
                # flag is present and true
                if 'exclude_common_mount_points' in $sid_entry[1] {
                  if $sid_entry[1]['exclude_common_mount_points'] { next() }
                }
              }
              default: {
                unless($component in $sid_components) { next() }
              }
            }

            if 'pattern_values' in $sap::system_ids[$sid] {
              $sid_values = $sap::system_ids[$sid]['pattern_values'] + $sap::sid_case_mapping[$sid]
            } else {
              $sid_values = $sap::sid_case_mapping[$sid]
            }

            # Construct the parameter hash for the mount point
            $default_parameters = {
              'pattern_map'    => $sap::pattern_map,
              'pattern_values' => $sid_values,
              'mount_path'     => $base_path,
              'mount_defaults' => $sap::params::config_default_mount_options,
            }
            $merge_parameters = deep_merge($parameters, $default_parameters)

            # Assemble the argument to the defined type
            sap::config::mount_point { "${base_path}_${sid}":
              * => $merge_parameters,
            }
          }
        }
        false: {
          # Construct the parameter hash for the mount point
          $default_parameters = {
            'pattern_map'    => {},
            'pattern_values' => {},
            'mount_path'     => $base_path,
            'mount_defaults' => $sap::params::config_default_mount_options,
          }
          $merge_parameters = deep_merge($parameters, $default_parameters)
          sap::config::mount_point { $base_path:
            * => $merge_parameters,
          }
        }
        default: {
          fail("Invaild 'per_sid' ${per_sid} for '${base_path}'!")
        }
      }
    }
  }
}
