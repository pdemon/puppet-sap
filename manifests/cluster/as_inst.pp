# @summary Creates Corosync/Pacemaker cluster rules for a single AS instance.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param sid
#   System ID this instance is associated with
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param db_mode
#   Pass-through parameter from the parent sap::cluster::as type. See that
#   description for details.
#
# @param profile_dir
#   Location storing the profile files for each instance. Typically this path is
#   defined as `/sapmnt/${SID}/profile`. The full start profile is constructed
#   using the instance specific data provided in other parameters.
#
# @param relevant_nodes
#   A filtered list of nodes which are relevant to the AS instance described by
#   this instance. One entry in this must match the `host` parameter defined
#   below or the declaration will fail.
#
# @param type
#   'Name' of the instance type. For example 'D', 'DVEBMGS', or 'J'.
#
# @param number
#   Two digit code assigned to the instance.
#
# @param host
#   Short hostname used when the instance was installed.
#
# @param legacy_start_profile
#   Indicates whether this instance uses the separate START_* profiles as was
#   common on older releases of SAP Netweaver prior to 7.10. More recent builds
#   include the START details within the standard profile. For further detail
#   see https://launchpad.support.sap.com/#/notes/1751381.
#
# @example Definition for an instance on pr0-app01
#   sap::cluster::as_inst { 'as_inst_PR0_0':
#     sid            => 'PR0',
#     cib_name       => 'sap',
#     scs_primitive  => 'ms_prm_PR0_SCS',
#     db_mode        => 'log_shipping',
#     profile_dir    => '/sapmnt/PR0/profile',
#     relevant_nodes => ['pr0-app00.example.org', 'pr0-app01.example.org'],
#     type           => 'DVEBMGS',
#     number         => '00',
#     host           => 'pr0-app01',
#   }
#
define sap::cluster::as_inst (
  Sap::SID $sid,
  String $cib_name,
  Sap::ClusterDbMode $db_mode,
  Stdlib::AbsolutePath $profile_dir,
  Array[Stdlib::Host] $relevant_nodes,
  Sap::InstType $type,
  Sap::InstNumber $number,
  Stdlib::Host $host,
  Boolean $legacy_start_profile        = false,
) {
  # Define the standard operation latencies
  # Note that the master/slave/promote/demote are not actually used here but are
  # declare to prevent puppet-corosync from getting fiesty with the default
  # monitor statements
  $as_operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'monitor' => {
        'interval' => '120s',
        'timeout'  => '60s',
      },
    },
    {
      'monitor' => {
        'interval' => '121s',
        'timeout'  => '60s',
        'role'     => 'Slave',
      },
    },
    {
      'monitor' => {
        'interval' => '119s',
        'timeout'  => '60s',
        'role'     => 'Master',
      },
    },
    {
      'promote' => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'demote' => {
        'interval' => '0s',
        'timeout'  => '320s',
      },
    },
    {
      'methods' => {
        'interval' => '0s',
        'timeout'  => '5s',
      },
    },
  ]

  # Restrict the app server resource to it's local host
  $target_node_list = $relevant_nodes.filter |$node| { $node =~ "^${host}" }
  if $target_node_list.length != 1 {
    fail("ClusterAs: ${title} could not identify node matching ${host}!")
  }
  $target_node = $target_node_list[0]

  # Add failure checks for this instance
  $instname = "${sid}_${type}${number}_${host}"
  if $legacy_start_profile {
    $as_start_profile = "${profile_dir}/START_${type}${number}_${host}"
  } else {
    $as_start_profile = "${profile_dir}/${instname}"
  }
  $as_primitive = "prm_${sid}_AS_${host}"

  # Construct the instance parameters
  $as_parameters = {
    'InstanceName'  => $instname,
    'START_PROFILE' => $as_start_profile,
    'DIR_PROFILE'   => $profile_dir,
  }

  # Create the AS primitive
  cs_primitive { $as_primitive:
    primitive_class => 'ocf',
    primitive_type  => 'SAPInstance',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => $as_parameters,
    operations      => $as_operations,
  }

  # Constrain the AS primitive to the target node
  cs_location { "locate-${as_primitive}-on-${target_node}":
    cib       => $cib_name,
    primitive => $as_primitive,
    node_name => $target_node,
    score     => 'INFINITY',
    require   => Cs_primitive[$as_primitive],
  }

  # Ensure the SCS instance starts and is promoted first
  # TODO: Maybe change this to depend on the IP instead?
  $scs_primitive = "prm_${sid}_SCS"
  cs_order { "order-promote-ms_${scs_primitive}-then-start-${as_primitive}":
    cib         => $cib_name,
    first       => "ms_${scs_primitive}:promote",
    second      => "${as_primitive}:start",
    symmetrical => false,
    require     => [
      Cs_primitive[$as_primitive],
      Cs_primitive[$scs_primitive],
    ],
  }

  # Ensure the database instance is started or promoted first
  # TODO: Maybe change this to depend on the IP instead?
  $db_primitive_base = "prm_${sid}_DB"
  $order_db_require = "Cs_primitive[${db_primitive_base}]"
  case $db_mode {
    'log_shipping': {
      $db_primitive = "ms_${db_primitive_base}"
      $order_db_rule = "order-promote-${db_primitive}-then-start-${as_primitive}"
      $order_db_first = "${db_primitive}:promote"
    }
    'shared_storage': {
      $db_primitive = $db_primitive_base
      $order_db_rule = "order-start-${db_primitive}-then-start-${as_primitive}"
      $order_db_first = "${db_primitive}:start"
    }
    default: {
      fail("ClusterAs: ${title} unsupported database cluster mode: ${db_mode}!")
    }
  }

  cs_order { $order_db_rule:
    cib         => $cib_name,
    first       => $order_db_first,
    second      => "${as_primitive}:start",
    symmetrical => false,
    require     => $order_db_require,
  }
}
