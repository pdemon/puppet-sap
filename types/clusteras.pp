# Used to model linux high availability rules for one or more application server
# instances. Note that this does not support the 'primary application server'
# model.
type Sap::ClusterAs = Hash[
  Enum[
    'as_instances',    # Instance details for each AS
  ],
  Variant[
    Array[Sap::InstData],
  ],
]
