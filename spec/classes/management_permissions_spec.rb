require 'spec_helper'
require 'deep_merge'

describe 'sap::management::permissions' do
  let(:params) do
    {
      system_ids: {
        'PRD' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
          instances: {
            'as-abap' => {},
          },
        },
        'QAS' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
        },
        'EP0' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
        },
        'EP1' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
        },
        'SCP' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
        },
        'WP0' => {
          manage_profiles: false,
          components: [
            'base',
            'base_extended',
          ],
        },
      },
    }
  end

  it 'compiles' do
    is_expected.to compile.with_all_deps
  end

  it 'ensures icmbnd has the appropriate permissions for all declared instances' do
    params[:system_ids].each do |sid, data|
      next unless data.key?(:instances)
      next unless data[:instances].key?('as-abap')
      is_expected.to contain_file("/sapmnt/#{sid}/exe/icmbnd.new").with(
        ensure: 'file',
        owner: 'root',
        group: 'sapsys',
        mode: '4750',
      )
    end
  end
end
