# @summary Implementation detail - corrects permissions as needed
#
# @param system_ids
#   Replicated from sap::system_ids primarily to make the test code easier to
#   write. See the parent for further detail.
#   
class sap::management::permissions (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids = $sap::system_ids,
) {
  $system_ids.each |$sid, $sid_data| {
    $sid_lower = downcase($sid)

    # For now, limit this to instances include abap application servers
    unless('instances' in $sid_data) { next() }
    unless('as-abap' in $sid_data['instances']) { next() }

    # Attempt to set the sticky bit for icmbnd
    $exedir = "/sapmnt/${sid}/exe"
    file { "${exedir}/icmbnd.new":
      ensure => 'file',
      owner  => 'root',
      group  => 'sapsys',
      mode   => '4750',
    }
  }
}
