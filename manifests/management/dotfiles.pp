# This is an internal class and should not be called directly.
# If $bash_source_profile is set to true for the instance as a whole this class
# will be enabled and will cause the .bash_profile of the sid_adm and db2 users
# to source their '.profile'.
#
# @summary Manages the dotfiles belonging to SAP users.
#
class sap::management::dotfiles {
  if $facts['sap'] != undef {
    $sid_hash = $facts['sap']['sid_hash']
  } else {
    $sid_hash = {}
  }

  $sid_hash.each |$sid, $data| {
    $sid_lower = downcase($sid)
    $sidadm = "${sid_lower}adm"

    # Skip SID entries which don't have instances
    unless('instances' in $data) { next() }

    $instances = $data['instances']

    # Skip SIDs with an empty instance structure
    if empty($instances) { next() }

    # Create the <sid>adm user bash profile
    file { "${sidadm}_bash_profile":
      ensure         => 'file',
      path           => "/home/${sidadm}/.bash_profile",
      owner          => $sidadm,
      group          => 'sapsys',
      mode           => '0644',
      content        => file('sap/sidadm_bash_profile'),
      checksum       => 'sha256',
      checksum_value => 'fd59129b812b607e2f7989742ac65fafa81a3c059588db970582945c1645b947',
    }

    # Create Database (db2<sid>) user .bash_profile
    if 'database' in $instances {
      $db_type = $instances['database']['type']
      case $db_type {
        'db2': {
          file { "db2${sid_lower}_bash_profile":
            ensure         => 'file',
            path           => "/db2/db2${sid_lower}/.bash_profile",
            owner          => "db2${sid_lower}",
            group          => "db${sid_lower}adm",
            mode           => '0644',
            content        => file('sap/sidadm_bash_profile'),
            checksum       => 'sha256',
            checksum_value => 'fd59129b812b607e2f7989742ac65fafa81a3c059588db970582945c1645b947',
          }
        }
        default: {}
      }
    }
  }
}
