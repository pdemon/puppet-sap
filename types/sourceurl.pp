type Sap::SourceURL = Variant[
  Sap::PuppetUrl,
  Stdlib::Httpurl,
  Stdlib::Httpsurl,
]
