# @summary Simple function to retrieve only the nodes with a desired role
#
# @param node_role_hash
#   Mapping from node names to a list of roles which that node can perform.
#
# @param target_role
#   Nodes with this role will be returned in the resulting list
#
# @return [Array[Stdlib::Fqdn]]
#   Array of node names which have the target_role role.
#
function sap::cluster::filter_nodes(
  Sap::NodeRole $target_role,
  Sap::ClusterNodeRoles $node_role_hash
) >> Array[Stdlib::Fqdn] {
  $func_name = 'sap::filter_nodes'

  # Reduce the hash to nodes with the desired role
  $matching_node_hash = $node_role_hash.filter | $items | { $target_role in $items[1] }

  # Extract the keys from our map
  $matching_node_list = $matching_node_hash.map | $key,$value | { $key }
}
