type Sap::FirewallRulePort = Hash[
  Enum[
    'port_pattern',
    'port',
    'protocol',
  ],
  Variant[
    Enum['tcp', 'udp'],
    String,
  ],
]
