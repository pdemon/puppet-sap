require 'spec_helper'

describe 'Sap::ABAPLang' do
  it 'accepts strings that are exactly 2 characters' do
    is_expected.to allow_value('EN', 'DE')
  end

  it 'rejects strings longer than 3 charcters' do
    is_expected.not_to allow_value('ENG', 'GERMAN')
  end

  it 'rejects strings shorter than 3 charcters' do
    is_expected.not_to allow_value('E', 'A')
  end

  it 'rejects strings which include lowercase and special characters' do
    is_expected.not_to allow_value('en', 'A_', 'B*', 'De')
  end
end
