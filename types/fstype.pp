# @summary Supported filesystems
type Sap::FsType = Enum[
  'xfs',
  'ext4',
]
