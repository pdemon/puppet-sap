# Constrains the list of valid resource types
type Sap::ClusterComponentType = Enum[
  'As',
  'ScsErs',
  'Db2',
  'Lvm',
]
