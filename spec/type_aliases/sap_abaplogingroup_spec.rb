require 'spec_helper'

describe 'Sap::ABAPLoginGroup' do
  it 'accepts strings that are less than 21 characters' do
    is_expected.to allow_value('SPACE', 'DIALOG', 'LONG_GROUP_NAME')
  end

  it 'rejects strings longer than 20 charcters' do
    is_expected.not_to allow_value('0123456789ABCDEFGHIJK')
  end

  it 'rejects strings which include lowercase and special characters' do
    is_expected.not_to allow_value('a00', 'cPt', '03p', 'space', 'cif_generators', 'STTUFFF*')
  end
end
