require 'spec_helper'
require 'deep_merge'

describe 'sap::cluster::prepare', type: :class do
  let :facts do
    {
      os: {
        family: 'RedHat',
        release: {
          major: 7,
        },
      },
    }
  end
  let(:node) { 'prd-a-dba.example.org' }
  let(:params) do
    {
      cluster_sid_list: ['PRD'],
    }
  end

  context 'db2 cleanup environment files' do
    let(:node) { 'prd-a-dba.example.org' }

    before(:each) do
      facts.merge!(
        sap: {
          sid_hash: {
            PRD: {
              instances: {
                database: {
                  type: 'db2',
                },
              },
            },
          },
        },
      )
    end

    it 'manages the appropriate number of environment files' do
      is_expected.to have_file_resource_count(12)
    end

    it 'removes all hostname specific files from the db2 home' do
      is_expected.to contain_file('/db2/db2prd/.dbenv_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/db2/db2prd/.dbenv_prd-a-dba.csh').with_ensure('absent')
      is_expected.to contain_file('/db2/db2prd/.sapenv_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/db2/db2prd/.sapenv_prd-a-dba.csh').with_ensure('absent')
    end

    it 'removes all hostname specific files the sidadm home' do
      is_expected.to contain_file('/home/prdadm/.sapsrc_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.sapsrc_prd-a-dba.csh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.sapenv_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.sapenv_prd-a-dba.csh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.j2eeenv_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.j2eeenv_prd-a-dba.csh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.dbenv_prd-a-dba.sh').with_ensure('absent')
      is_expected.to contain_file('/home/prdadm/.dbenv_prd-a-dba.csh').with_ensure('absent')
    end

    it 'installs the cluster packages' do
      is_expected.to contain_package('resource-agents').with_ensure('installed')
      is_expected.to contain_package('resource-agents-sap').with_ensure('installed')
    end
  end

  context 'parse instance data for ASCS and ERS' do
    let(:node) { 'prd-a-app00.example.org' }

    before(:each) do
      facts.merge!(
        hostname: 'prd-a-app00',
        sap: {
          sid_hash: {
            'PRD' => {
              instances: {
                '01' => {
                  type: 'ASCS',
                  profiles: [
                    '/sapmnt/PRD/profile/PRD_ASCS01_prd-a-cs',
                    '/sapmnt/PRD/profile/START_ASCS01_prd-a-cs',
                  ],
                },
                '02' => {
                  type: 'ERS',
                  profiles: [
                    '/sapmnt/PRD/profile/PRD_ERS02_prd-a-ers',
                    '/sapmnt/PRD/profile/START_ERS02_prd-a-ers',
                  ],
                },
                '00' => {
                  type: 'D',
                  profiles: [
                    '/sapmnt/PRD/profile/PRD_D00_prd-a-app00',
                    '/sapmnt/PRD/profile/START_D00_prd-a-app00',
                  ],
                },
                database: {
                  type: 'db2',
                },
              },
            },
            'DAA' => {
              instances: {
                '98' => {
                  type: 'SMDA',
                  profiles: [
                    '/usr/sap/DAA/SYS/profile/DAA_SMDA98_ecp-app1',
                  ],
                },
              },
            },
          },
        },
      )
    end

    it 'manages the appropriate environment files' do
      sid_hash = facts[:sap][:sid_hash]
      sid_hash.each do |sid, sid_data|
        base_prefixes = ['.dbenv', '.sapenv']
        app_prefixes = ['.j2eeenv', '.sapsrc'] + base_prefixes
        if sid_data[:instances].include?(:database)
          username = "db2#{sid.downcase}"
          home = "/db2/#{username}"
          base_prefixes.each do |prefix|
            is_expected.to contain_file("#{home}/#{prefix}_#{facts[:hostname]}.sh").with_ensure('absent')
            is_expected.to contain_file("#{home}/#{prefix}_#{facts[:hostname]}.csh").with_ensure('absent')
          end
        end

        username = "#{sid.downcase}adm"
        home = "/home/#{username}"
        app_prefixes.each do |prefix|
          is_expected.to contain_file("#{home}/#{prefix}_#{facts[:hostname]}.sh").with_ensure('absent')
          is_expected.to contain_file("#{home}/#{prefix}_#{facts[:hostname]}.csh").with_ensure('absent')
        end
      end
    end

    it 'modifies the correct number of profiles' do
      # 4 substitutions Restart->Start and 3 /usr/sap/sapservices comments
      is_expected.to have_exec_resource_count(7)
    end

    it 'converts Restart to Start and comments out /usr/sap/sapservices where relevant' do
      sid_hash = facts[:sap][:sid_hash]
      sid_hash.each do |sid, sid_data|
        inst_hash = sid_data[:instances]
        inst_hash.each do |inst, inst_data|
          type = inst_data[:type]
          profiles = inst_data[:profiles]
          cluster_sid = params[:cluster_sid_list].include?(sid)

          instname = "#{type}#{inst}"
          sfile = '/usr/sap/sapservices'
          if cluster_sid && !%r{database}.match?(inst)
            is_expected.to contain_exec("sed -e '/^\\w.*#{sid}\\/#{instname}.*/ s/^#*/#/' -i #{sfile}").with(
              path: '/sbin:/bin:/usr/sbin:/usr/bin',
              onlyif: [
                "test -f #{sfile}",
                "test 0 -eq $(grep -v '^#' #{sfile} | grep '#{sid}/#{instname}' >/dev/null; echo $?)",
              ],
            )
          else
            is_expected.not_to contain_exec("sed -e '/^\\w.*#{sid}\\/#{instname}.*/ s/^#*/#/' -i #{sfile}")
          end

          next if profiles.nil?
          # Don't update unclustered profiles
          unless cluster_sid
            profiles.each do |profile|
              is_expected.not_to contain_exec("sed -i 's/^Restart_Program/Start_Program/' #{profile}")
            end
            next
          end

          # Only update profiles for SCS/ERS
          case type
          when %r{SCS}, %r{ERS}
            profiles.each do |profile|
              is_expected.to contain_exec("sed -i 's/^Restart_Program/Start_Program/' #{profile}").with(
                path: '/sbin:/bin:/usr/sbin:/usr/bin',
                onlyif: [
                  "test -f #{profile}",
                  "test 0 -eq $(grep \"^Restart_Program\" #{profile} >/dev/null; echo $?)",
                ],
              )
            end
          else
            profiles.each do |profile|
              is_expected.not_to contain_exec("sed -i 's/^Restart_Program/Start_Program/' #{profile}")
            end
          end
        end
      end
    end

    it 'installs the cluster packages' do
      is_expected.to contain_package('resource-agents').with_ensure('installed')
      is_expected.to contain_package('resource-agents-sap').with_ensure('installed')
    end
  end
end
