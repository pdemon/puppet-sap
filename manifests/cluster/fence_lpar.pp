# @summary Creates Corosync/Pacemaker rules for a fence_lpar STONITH device
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param ipaddr
#   IP address or hostname of the fencing HMC
#
# @param login
#   Login username for the HMC
#
# @param managed
#   Name of the CEC or managed system which hosts the target LPAR(s)
#
# @param identity_file
#   Private key file used to connect to the HMC
#
# @param pcmk_delay_max
#  a random maximum delay for stonith actions to prevent double fencing
#
# @param pcmk_host_map
#   One to one mapping from trusted node host names to the LPAR names in the
#   HMC. An individual stonith resource will be created for each entry in this
#   list and will be allowed to run on any node other than the node it is
#   responsible for fencing.
#
# @param node_list
#   Full list of nodes in the cluster. This is used to co-locate the resource on
#   any node other than the home node.
#
# @example Asymmetric 4-node cluster defining the AS resource for each app node
#   sap::cluster::as { 'fence_lpar_0':
#     cib_name                  => 'sap',
#     ipaddr                    => 'hmc0.example.org',
#     login                     => 'service-fence',
#     managed                   => 'host0',
#     identity_file             => '/root/.ssh/hmc_secret.rsa',
#     pcmk_delay_max            => 15,
#     pcmk_host_map             => {
#       'pr0-app00.example.org' => 'pr0-app00',
#       'pr0-app01.example.org' => 'pr0-app01',
#       'pr0-dba.example.org'   => 'pr0-dba',
#       'pr0-dbb.example.org'   => 'pr0-dbb',
#     },
#   }
#
define sap::cluster::fence_lpar (
  String $cib_name,
  Stdlib::Host $ipaddr,
  Stdlib::AbsolutePath $identity_file,
  String $login,
  Stdlib::Host $managed,
  Integer $pcmk_delay_max,
  Sap::ClusterFenceHostMap $pcmk_host_map,
  Array[Stdlib::Host] $node_list,
) {
  # Constants
  $primitive_type = 'fence_lpar'
  $primitive_class = 'stonith'

  # Construct the base primitive parameters
  $base_parameters = {
    'ipaddr'         => $ipaddr,
    'login'          => $login,
    'identity_file'  => $identity_file,
    'managed'        => $managed,
    'pcmk_delay_max' => "${pcmk_delay_max}s",
  }

  # Construct the default operation
  $operations = {
    'monitor' => { 'interval' => '60s' },
  }

  # Retrieve the raw node list
  $target_node_list = $pcmk_host_map.keys()

  $target_node_list.each |$target_node| {
    $lpar = $pcmk_host_map[$target_node]
    $primitive_name = "${name}_${target_node}"

    $parameters = $base_parameters + {
      'pcmk_host_map' => "${target_node}:${lpar}"
    }
    cs_primitive { $primitive_name:
      primitive_class => $primitive_class,
      primitive_type  => $primitive_type,
      cib             => $cib_name,
      parameters      => $parameters,
      operations      => $operations,
    }

    # Locate the fence agent on every node other than the fence target
    $node_list.each |$node| {
      if $node == $target_node { next() }
      cs_location { "locate-${primitive_name}_on_${node}":
        cib       => $cib_name,
        primitive => $primitive_name,
        node_name => $node,
        score     => '200',
        require   => Cs_primitive[$primitive_name],
      }
    }
  }
}
