type Sap::Db2QuerySchedule = Hash[
  Enum[
    'query_file_name',
    'query_content',
    'schema',
    'cron_entry',
    'output_target',
    'output_group',
    'monitor_alias',
  ],
  Variant[
    String,
    Sap::SourceURL,
    Sap::ScheduleEntry,
    Stdlib::AbsolutePath,
    Stdlib::Host,
  ],
]
