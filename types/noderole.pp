type Sap::NodeRole = Enum[
  'app-standard',
  'app-scs',
  'app-apo-opt',
  'db-standard',
  'db-livecache',
]
