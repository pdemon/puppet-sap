require 'spec_helper'

describe 'sap::cluster::fence_lpar' do
  let(:sid) { 'PR0' }
  let(:sid_lower) { sid.downcase }
  let(:fence_agents) do
    [
      {
        'type' => 'fence_lpar',
        'data' => {
          ipaddr: 'hmc00.exmaple.org',
          login: 'service-fence',
          managed: 'host0',
          identity_file: '/root/.ssh/service-fence_rsa',
          pcmk_delay_max: 10,
          pcmk_host_map: {
            "#{sid_lower}-app00.example.org" => "#{sid_lower}-app00",
            "#{sid_lower}-dba.example.org" => "#{sid_lower}-dba",
          },
        },
      },
      {
        'type' => 'fence_lpar',
        'data' => {
          ipaddr: 'hmc00.exmaple.org',
          login: 'service-fence',
          managed: 'host1',
          identity_file: '/root/.ssh/service-fence_rsa',
          pcmk_delay_max: 10,
          pcmk_host_map: {
            "#{sid_lower}-app01.example.org" => "#{sid_lower}-app01",
            "#{sid_lower}-dbb.example.org" => "#{sid_lower}-dbb",
          },
        },
      },
    ]
  end
  let(:default_operations) do
    {
      'monitor' => { 'interval' => '60s' },
    }
  end
  let(:node_list) do
    [
      "#{sid_lower}-app00.example.org",
      "#{sid_lower}-app01.example.org",
      "#{sid_lower}-dba.example.org",
      "#{sid_lower}-dbb.example.org",
    ]
  end
  let(:params) do
    {
      cib_name: 'sap',
      node_list: node_list,
    }
  end

  [0, 1].each do |idx|
    context "valid instances for lpar_fence_#{idx}" do
      let(:title) { "fence_lpar_#{idx}" }

      before(:each) do
        params.merge!(
          fence_agents[idx]['data'],
        )
      end

      it 'compiles without error' do
        is_expected.to compile.with_all_deps
      end

      it 'declares stonith primitives for each pcmk_host_map entry' do
        agent = fence_agents[idx]
        agent_data = agent['data']
        base_params = {
          'ipaddr' => agent_data[:ipaddr],
          'login' => agent_data[:login],
          'identity_file' => agent_data[:identity_file],
          'managed' => agent_data[:managed],
          'pcmk_delay_max' => "#{agent_data[:pcmk_delay_max]}s",
        }
        agent_data[:pcmk_host_map].each do |target_node, lpar|
          primitive_name = "#{title}_#{target_node}"
          fence_params = base_params.merge(
            'pcmk_host_map' => "#{target_node}:#{lpar}",
          )
          is_expected.to contain_cs_primitive(primitive_name).with(
            primitive_class: 'stonith',
            primitive_type: agent['type'],
            cib: 'sap',
            parameters: fence_params,
            operations: default_operations,
          )
        end
      end

      it 'does not locate the primitives on the target node' do
        agent = fence_agents[idx]
        agent_data = agent['data']
        agent_data[:pcmk_host_map].each do |target_node, _lpar|
          primitive_name = "#{title}_#{target_node}"
          node_list.each do |node|
            location_name = "locate-#{primitive_name}_on_#{node}"
            if node == target_node
              is_expected.not_to contain_cs_location(location_name)
            else
              is_expected.to contain_cs_location(location_name).with(
                cib: 'sap',
                primitive: primitive_name,
                node_name: node,
                score: '200',
                require: "Cs_primitive[#{primitive_name}]",
              )
            end
          end
        end
      end
    end
  end
end
