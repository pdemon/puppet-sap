require 'spec_helper'

# See https://launchpad.support.sap.com/#/notes/614971 for detail

describe 'Sap::SecUser' do
  it "accepts 'P' or 'D'" do
    is_expected.to allow_value('P', 'D')
  end
end
