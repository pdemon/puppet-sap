type Sap::ScheduleEntry = Hash[
  String,
  Variant[
    String,
    Integer,
    Array,
  ],
]
