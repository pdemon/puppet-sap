# @summary Creates Corosync/Pacemaker cluster rules for a SAP DB2 HA/DR instance.
#
# This defined type should be used by the sap::cluster class and not called
# directly externally. 
#
# @param sid
#   System ID this instance is associated with
#
# @param cib_name
#   Name of the temporary Cluster Information Base to use during the building
#   and commiting of the rule changes incorporated in this module.
# 
# @param nodes
#   Mapping containing each a list of relevant roles for each node in the
#   cluster.
#
# @param db_ip
#   Service IP address which moves between nodes and represents the 'master'
#   of the cluster. Clients will connect to this instead of
#   the local IPs of the machines.
#
# @param mode
#   Indicates the type of HA cluster. There are currently two supported modes
#   * `log_shipping` - Shared nothing approach which utilizes log shipping
#       to keep one or more replicas synchronized with the current primary.
#   * `shared_storage` - Approach which moves disks containing the
#       database instance between one or more nodes.
#
# @param volumes
#   Array containing a series of hashes which describe the LVM cluster resources
#   needed for the primary node of this db2 cluster to function.
#
# @param filesystems
#   Array containing a series of hashes which describe the Filesystem cluster
#   resources needed on the primary node. The parent LVM volume for each file
#   system must be included in the `volume` resource.
#
# @param hadr_peer_window
#   Maximum number of seconds the standby nodes can be in an HADR peer topology.
#   This is used to construct 'sane' monitoring intervals for the log-shipping
#   based DB2 model.
#
# @example Asymmetric 4-node cluster with DB2 HADR on two of the nodes
#   sap::cluster::scsers { 'PR0_Db2':
#     sid          => 'PR0',
#     cib_name     => 'sap',
#     nodes        => {
#       'pr0-app00.example.org' => ['app-standard'],
#       'pr0-app01.example.org' => ['app-standard'],
#       'pr0-dba.example.org'   => ['db-standard'],
#       'pr0-dbb.example.org'   => ['db-standard'],
#     },
#     db_ip        => '10.0.128.15',
#     mode         => 'log_shipping',
#   }
#
define sap::cluster::db2 (
  Sap::SID $sid,
  String $cib_name,
  Sap::ClusterNodeRoles $nodes,
  Stdlib::IP::Address::Nosubnet $db_ip,
  Sap::ClusterDbMode $mode,
  Array[Sap::ClusterLvm] $volumes,
  Array[Sap::ClusterFilesystem] $filesystems,
  Integer $hadr_peer_window                  = 300,
) {
  # Constants
  $relevant_node_role = 'db-standard'
  $sid_lower = downcase($sid)
  $instance = "db2${sid_lower}"
  $dblist = $sid_lower

  # Ensure at least some nodes match the target pattern
  $relevant_nodes = sap::cluster::filter_nodes($relevant_node_role, $nodes)
  if empty($relevant_nodes) {
    fail("ClusterDb2: ${title} no nodes with ${relevant_node_role} role specified!")
  }

  # Ensure filesystems / LVM details were specified
  case $mode {
    'log_shipping': {
      $require_filesystems = false
    }
    'shared_storage': {
      $require_filesystems = true
    }
    # No handling required for default case as it is impossible
    default: {}
  }

  # Create each LVM resource and add to the group
  if ! empty($volumes) {
    $volumes.each |$index, $volume| {
      sap::cluster::lvm { "${instance}_lvm_${index}":
        sid      => $sid,
        cib_name => $cib_name,
        *        => $volume,
      }
    }
    # Generate LVM primitive list
    $lvm_primitives = $volumes.map |$value| {
      $volgrpname = $value['volgrpname']
      "prm_${sid}_${volgrpname}_LVM"
    }
  } elsif $require_filesystems {
    fail("ClusterDb2: ${title} volumes must be specified in ${mode} mode!")
  }

  # Create each Filesystem resource and add to the group
  if !empty($filesystems) {
    $filesystems.each |$index, $filesystem| {
      sap::cluster::filesystem { "${instance}_filesystem_${index}":
        sid            => $sid,
        cib_name       => $cib_name,
        lvm_primitives => $lvm_primitives,
        *              => $filesystem,
      }
    }
    # Generate filesystem primitive list and order such that child filesystems
    # are mounted after their parents
    $fs_primitives_unsorted = $filesystems.map |$filesystem| {
      $directory_underscores = regsubst($filesystem['directory'], '/', '_', 'G')
      "prm_${sid}_filesystem${directory_underscores}"
    }
    $fs_primitives = $fs_primitives_unsorted.sort
  } elsif $require_filesystems {
    fail("ClusterDb2: ${title} filesystems must be specified in ${mode} mode!")
  }

  # Service IP resource is shared between both modes
  $ip_operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '20s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '20s',
      },
    },
    {
      'monitor'  => {
        'interval' => '10s',
        'timeout'  => '20s',
      },
    },
  ]
  $db2_ip_prm = "prm_${instance}_IP"
  cs_primitive { $db2_ip_prm:
    primitive_class => 'ocf',
    primitive_type  => 'IPaddr2',
    provided_by     => 'heartbeat',
    cib             => $cib_name,
    parameters      => {
      'ip' => $db_ip,
    },
    operations      => $ip_operations,
  }

  # Calculate safe timouts for the database.
  case $mode {
    'log_shipping': {
      # Let's ensure our monitor interval is always greater than or equal to 10
      $monitor_interval = max($hadr_peer_window - 30, 10)
      $monitor_interval_master = $monitor_interval + 2
      $promote_timeout = $hadr_peer_window + 20
    }
    'shared_storage': {
      # Standard uses the defaults for the resource
      $monitor_interval = 20
      $monitor_interval_master = 22
      $promote_timeout = 120
    }
    default: {}
  }

  # Define the standard operation latencies, master slave details, and 
  # db2 specific parameters.
  $db2_operations = [
    {
      'start' => {
        'interval' => '0s',
        'timeout'  => '120s',
      },
    },
    {
      'stop'  => {
        'interval' => '0s',
        'timeout'  => '120s',
      },
    },
    {
      'monitor' => {
        'interval' => "${monitor_interval}s",
        'timeout'  => '60s',
      },
    },
    {
      'monitor' => {
        'interval' => "${monitor_interval_master}s",
        'timeout'  => '60s',
        'role'     => 'Master',
      },
    },
    {
      'promote' => {
        'interval' => '0s',
        'timeout'  => "${promote_timeout}s",
      },
    },
    {
      'demote' => {
        'interval' => '0s',
        'timeout'  => "${promote_timeout}s",
      },
    },
  ]
  $db2_parameters = {
    'instance' => $instance,
    'dblist'   => $sid_lower,
  }

  $db2_primitive_base = "prm_${sid}_DB"
  $db2_primitive = "ms_${db2_primitive_base}"

  # Configure rules and non-shared primitives
  case $mode {
    'log_shipping': {
      # Configure the DB2 primitive
      $db2_ms_metadata = {
        'master-max' => '1',
        'notify'     => true,
      }
      cs_primitive { $db2_primitive_base:
        primitive_class => 'ocf',
        primitive_type  => 'db2',
        provided_by     => 'heartbeat',
        cib             => $cib_name,
        promotable      => true, # Flag as master/slave
        ms_metadata     => $db2_ms_metadata,
        parameters      => $db2_parameters,
        operations      => $db2_operations,
      }

      # ========== Filesystem Rules
      if !empty($filesystems) or !empty($volumes) {
        # ======== Group Declaration
        # Create the group with all the filesystem primitives ensuring the LVM
        # primitives happen first
        $database_group = "grp_${instance}_fs"
        $group_primitives = $lvm_primitives + $fs_primitives
        $group_requires = $group_primitives.map |$primitive| {
          case $primitive {
            /^ms_/: {
              $name = regsubst($primitive, /^ms_/, '')
            }
            default: {
              $name = $primitive
            }
          }
          Cs_primitive[$name]
        }
        cs_group { $database_group:
          cib        => $cib_name,
          primitives => $group_primitives,
          require    => $group_requires,
        }

        # ======== Colocation
        # The master must be colocated with the filesystems
        # Note that this implies the filesystems start before the master
        cs_colocation { "colocate-${db2_primitive}_master-with-${database_group}":
          cib        => $cib_name,
          score      => '2000',
          primitives => [
            $database_group,
            "${db2_primitive}:Master",
          ],
          require    => [
            Cs_primitive[$db2_primitive_base],
            Cs_group[$database_group],
          ],
        }

        # ========== Location Primitive List
        $location_primitives = [
          $database_group,
          $db2_primitive,
          $db2_ip_prm,
        ]
      } else {
        # ========== Location Primitive List
        $location_primitives = [
          $db2_primitive,
          $db2_ip_prm,
        ]
      }

      # ========== Colocation
      # IP must be colocated with the master
      # Note that this implies the master must be promoted first
      cs_colocation { "colocate-${db2_ip_prm}-with-${db2_primitive}_master":
        cib        => $cib_name,
        score      => '2000',
        primitives => [
          "${db2_primitive}:Master",
          $db2_ip_prm,
        ],
        require    => [
          Cs_primitive[$db2_primitive_base],
          Cs_primitive[$db2_ip_prm],
        ],
      }
    }
    'shared_storage': {
      # Configure the DB2 primitive
      cs_primitive { $db2_primitive_base:
        primitive_class => 'ocf',
        primitive_type  => 'db2',
        provided_by     => 'heartbeat',
        cib             => $cib_name,
        promotable      => false, # Flag as non-master slave
        parameters      => $db2_parameters,
        operations      => $db2_operations,
      }

      # Create the group with all the primitives using the following order
      # 1. LVM
      # 2. Filesystem
      # 3. IP
      # 4. DB2 instance
      $database_group = "grp_${instance}"
      $group_primitives =
        $lvm_primitives + $fs_primitives + $db2_ip_prm + $db2_primitive_base
      $group_requires = $group_primitives.map |$primitive| {
        case $primitive {
          /^ms_/: {
            $name = regsubst($primitive, /^ms_/, '')
          }
          default: {
            $name = $primitive
          }
        }
        Cs_primitive[$name]
      }
      cs_group { $database_group:
        cib        => $cib_name,
        primitives => $group_primitives,
        require    => $group_requires,
      }

      # ========== Location Primitive List
      $location_primitives = [$database_group]
    }
    default: {
      # Shouldn't be possible - if this is hit it's a bug!
      fail("Db2 unsupported cluster mode ${mode}!")
    }
  }

  # ========== Location constraints
  # Add each node primitive combination.
  $relevant_nodes.each |$node| {
    $location_primitives.each |$primitive| {
      case $primitive {
        /^grp_/: { $require_name = Cs_group[$primitive] }
        /^ms_/: {
          $require_name = Cs_primitive[regsubst($primitive, /^ms_/, '')]
        }
        default: { $require_name = Cs_primitive[$primitive] }
      }
      cs_location { "locate-${primitive}-${node}":
        cib       => $cib_name,
        primitive => $primitive,
        node_name => $node,
        score     => '200',
        require   => $require_name,
      }
    }
  }
}
