type Sap::FirewallServices = Enum[
  'dispatcher',
  'enqueue',
  'gateway',
  'igs',
  'java_as',
  'livecache',
  'message_server',
  'sapctrl',
  'webdisp',
  'web_icm',
]
