# Changelog

All notable changes to this project will be documented in this file.

## 3.0.0 (2023-03-06)

### Feature (1 change)

- [feat: parameter to disable DEFAULT.PFL](pdemon/puppet-sap@536cc4a634dd7646c787bf9b7fb2ceebae6ec313)

### Maintenance (3 changes)

- [refactor: SIDDatabaseDb2 as Struct](pdemon/puppet-sap@0b81f120d473c3e719dcea6ed72529f8f00b9af5)
- [refactor: sidconfigentry to struct](pdemon/puppet-sap@459675c35f84eeec0692237ae6bf3437091572ba)
- [refactor: pdk version and fixtures update](pdemon/puppet-sap@2825e98e6d267b72fd1d285fc238bc490294edc9)

## 2.0.10 (2021-09-01)

### Bugfix (1 change)

- [fix: changelog generation instructions](pdemon/puppet-sap@e85fc9b0fa26fc1a4764b4ef259e05e4b33924a9) ([merge request](pdemon/puppet-sap!22))

### Maintenance (6 changes)

- [maint: pdk version bump](pdemon/puppet-sap@e7c7ebd12b21722dffa7e2c74726f47dc251a1c7) ([merge request](pdemon/puppet-sap!22))
- [maint: update firewalld to the latest version](pdemon/puppet-sap@5467ffe5268f516e56cbef68225fc6c8f9c909da) ([merge request](pdemon/puppet-sap!22))
- [maint: supporting module update](pdemon/puppet-sap@c1ab7ca7b0849d2e58834eaf88ca9b4cfcfa4c71) ([merge request](pdemon/puppet-sap!22))
- [maint: replace stub with allow syntax](pdemon/puppet-sap@892389420a8f4f15bf7e538fac1b212981bd8a86) ([merge request](pdemon/puppet-sap!22))
- [maint: rubocop fixes](pdemon/puppet-sap@ea85d7fa224cac69f5bad921869325ed4739f5d3) ([merge request](pdemon/puppet-sap!22))
- [maint: PDK update to latest template](pdemon/puppet-sap@0d83a338c5edd2fe419b3e9768c58729f749bee0) ([merge request](pdemon/puppet-sap!22))

## Release 2.0.9

**Features**
* PDK 1.15 support & switch to the latest firewalld module.

## Release 2.0.8

**Features**
* Suppress output of db2 query schedule cron jobs

## Release 2.0.7

**Features**
* Add support for STONITH, currently limited to fence_lpar

## Release 2.0.5

**Features**
* Correct icmbnd.new permissions for SIDs containing AS-ABAP instances.

**Bugfixes**
* Syntax errors and missing paths are now fixed in db2_execute_query.

## Release 2.0.4

**Features**
* ADS Customer fonts & XDC config
* DB2 cron based scheduled queries
* PDK 1.12 support
* Possible support for puppet 6.x

## Release 2.0.3

**Features**
* New Types: ABAPUser, ABAPLang, ABAPPassword, ABAPLoginGroup, ABAPClient
* Updated for PDK 1.11.1

## Release 2.0.2

**Features**
* Fact Changes:
  * Major rewrite to incorporate unit testing.
  * Change from `/sapmnt/${SAPSYSTEMNAME}/profile` to `/usr/sap/${SAPSYSTEMNAME}/SYS/profile` for profile directories.
  * Addition of an `inst_classes` component containing a unique list of instance types.
* etc/services entry generation
  * DB2 connection ports
  * Standard SAP Ports
  * Aliases for message servers
  * Conflicting standard port entries are commented out
* When enabled, firewall rules for all relevant instances are automatically generated
* 'Mount points' which have the type link
* Creates the 'interface' directory structure needed for our EDI module
* When enabled on a per SID basis, the DEFAULT.PFL file for that SID is automatically generated.
  * handles J2EE, ABAP, and 'gateway only' specific parameters
  * allows additional parameters to be included via an array on a per-SID basis
  * Also creates baseline secinfo and reginfo files for each node

## Release 2.0.1

**Features**
* Restructured the SID argument such that component types are specified on a per SID basis
* Added support for liveCache and liveCache_client components
* Introduced flexible global and SID specific pattern mappings for use in mount point definitions
* Added support for mount-points which are SID specific but only in their user/group settings. As such they must be system level unique.
* Introduced a number of helper functions to simplify implementation of the mount point changes

## Release 1.0.1

**Features**
* Manage /usr/sap/sapservices for cluster nodes
* Updated profile path for facts

## Release 1.0.0

**Features**
* Major rework to use hiera data far more heavily than static config
* Removal of legacy 3.x syntax (e.g. `::sap`
* Refactor to support other architectures, ppc64 in particular
* Addition of dynamically calculated limits and sysctl entries
* Updated to match the latest RHEL 7 guidelines from SAP
* Added notify check for swap partition size
* Added validation and update of /dev/shm size
* Added test to ensure an SID is specified
* Added management of default mount point creation for 'base' and 'db2' components
* Added a top-level configuration of SID string replacement patterns
* Added support for managing NFS mounts 
* Includes required packages for db2 10.5
* Updated the sap facts to be a structure which includes the following detail
  * `sid_hash` - Local SIDs in a hash containing entries for each instance with the number as the key 
    * `type` - the kind of instance (ERS, ASCS, etc)
    * `profiles` - an array of all profile files located in `/sapmnt/SID/profile/` which belong to this instance
* Correctly tests ppc64 and x86_64 architectures
* Added support for pacemaker/corosync cluster configuration automation

**Bugfixes**
* Expanded the yum groups to actual package names to avoid constant service restarts
* Corrected the root cause of the restarts by specifying the uuidd.socket service instead of just uuidd.service. This is due to systemctl shenanigans as described in [PUP-6759](https://tickets.puppetlabs.com/browse/PUP-6759)

## Release 0.9.1
* Some changes ?

## Release 0.9.0
* Initial release


