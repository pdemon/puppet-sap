# This package manages configuration which is generated during the SAP install.
# Note that it must not be applied until after the base 'sap' class.
# Additionally, much of the functionality of this subclass is ignored if the
# base SAP facts do not seem to indicate that the class has been installed and
# configured.
#
# @summary Manages configuration post install of SAP.
#
class sap::management {
  if $sap::bash_source_profile {
    contain sap::management::dotfiles
  }

  class { 'sap::management::etc_services':
    system_ids      => $sap::system_ids,
    message_servers => $sap::message_servers,
  }
  contain sap::management::etc_services

  if $sap::manage_firewall {
    class { 'sap::management::firewall':
      system_ids => $sap::system_ids,
    }
    contain sap::management::firewall
  }

  class { 'sap::management::profiles':
    system_ids => $sap::system_ids,
  }
  contain sap::management::profiles

  contain sap::management::ads_fonts

  if 'db2' in $sap::enabled_components {
    contain sap::management::db2_queries
  }

  contain sap::management::permissions
}
