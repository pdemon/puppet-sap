require 'spec_helper'

describe 'sap::cluster::as' do
  let(:title) { 'PR0_As' }
  let(:params) do
    {
      sid: 'PR0',
      cib_name: 'sap',
      nodes: {
        'pr0-app00.example.org' => ['app-standard'],
        'pr0-app01.example.org' => ['app-standard'],
        'pr0-dba.example.org' => ['db-standard'],
        'pr0-dbb.example.org' => ['db-standard'],
      },
      db_mode: 'log_shipping',
    }
  end
  let(:pre_condition) do
    <<-HEREDOC
      $nodes = {
        'pr0-app00.example.org' => ['app-standard', 'app-scs'],
        'pr0-app01.example.org' => ['app-standard', 'app-scs'],
        'pr0-dba.example.org' => ['db-standard'],
        'pr0-dbb.example.org' => ['db-standard'],
      }
      sap::cluster::db2 { 'PR0_Db2':
        sid         => 'PR0',
        cib_name    => 'sap',
        nodes       => $nodes,
        db_ip       => '10.0.0.20',
        mode        => 'log_shipping',
        volumes     => [],
        filesystems => [],
      }
      sap::cluster::scsers { 'PR0_ScsErs':
        sid         => 'PR0',
        cib_name    => 'sap',
        nodes       => $nodes,
        ers_ip      => '10.0.0.11',
        ers_inst    => {
          'type' => 'ERS',
          'number' => '02',
          'host' => 'pr0-ers',
        },
        scs_ip      => '10.0.0.12',
        scs_inst    => {
          'type' => 'ASCS',
          'number' => '01',
          'host' => 'pr0-cs',
        },
      }
    HEREDOC
  end

  context 'empty as_inst array' do
    before(:each) do
      params.merge!(
        as_instances: [],
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{ClusterAs: #{title} at least one AS instance must be specified!}) }
  end

  context 'no relevant node included' do
    before(:each) do
      params.merge!(
        as_instances: [
          {
            type: 'D',
            number: '00',
            host: 'pr0-app00',
          },
        ],
        nodes: {
          'pr0-app00.example.org' => ['app-scs'],
          'pr0-app01.example.org' => ['app-scs'],
          'pr0-dbb.example.org' => ['db-standard'],
          'pr0-dba.example.org' => ['db-standard'],
        },
      )
    end

    it { is_expected.to compile.with_all_deps.and_raise_error(%r{ClusterAs: #{title} no nodes with app-standard role specified!}) }
  end

  context 'cannot find node matching host' do
    before(:each) do
      params.merge!(
        as_instances: [
          {
            type: 'D',
            number: '00',
            host: 'pr0-app02',
          },
        ],
        nodes: {
          'pr0-app00.example.org' => ['app-standard'],
          'pr0-app01.example.org' => ['app-standard'],
          'pr0-dbb.example.org' => ['db-standard'],
          'pr0-dba.example.org' => ['db-standard'],
        },
      )
    end

    it {
      as_inst_title = "as_inst_#{params[:sid]}_0"
      is_expected.to compile.with_all_deps.and_raise_error(%r{ClusterAs: #{as_inst_title} could not identify node matching pr0-app02!})
    }
  end

  context 'valid four node cluster' do
    before(:each) do
      params.merge!(
        as_instances: [
          {
            type: 'DVEBMGS',
            number: '00',
            host: 'pr0-app00',
          },
          {
            type: 'D',
            number: '00',
            host: 'pr0-app01',
          },
        ],
      )
    end

    it 'compiles without error' do
      is_expected.to compile.with_all_deps
    end

    it 'declares two AS primitives' do
      as_operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'monitor' => {
            'interval' => '120s',
            'timeout'  => '60s',
          },
        },
        {
          'monitor' => {
            'interval' => '121s',
            'timeout'  => '60s',
            'role'     => 'Slave',
          },
        },
        {
          'monitor' => {
            'interval' => '119s',
            'timeout'  => '60s',
            'role'     => 'Master',
          },
        },
        {
          'promote' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'demote' => {
            'interval' => '0s',
            'timeout'  => '320s',
          },
        },
        {
          'methods' => {
            'interval' => '0s',
            'timeout'  => '5s',
          },
        },
      ]
      is_expected.to contain_sap__cluster__as_inst('as_inst_PR0_0')
      is_expected.to contain_sap__cluster__as_inst('as_inst_PR0_1')
      is_expected.to contain_cs_primitive('prm_PR0_AS_pr0-app00').with(
        primitive_class: 'ocf',
        primitive_type: 'SAPInstance',
        provided_by: 'heartbeat',
        cib: 'sap',
        parameters: {
          'InstanceName' => 'PR0_DVEBMGS00_pr0-app00',
          'START_PROFILE' => '/sapmnt/PR0/profile/PR0_DVEBMGS00_pr0-app00',
          'DIR_PROFILE' => '/sapmnt/PR0/profile',
        },
        operations: as_operations,
      )
      is_expected.to contain_cs_primitive('prm_PR0_AS_pr0-app01').with(
        primitive_class: 'ocf',
        primitive_type: 'SAPInstance',
        provided_by: 'heartbeat',
        cib: 'sap',
        parameters: {
          'InstanceName' => 'PR0_D00_pr0-app01',
          'START_PROFILE' => '/sapmnt/PR0/profile/PR0_D00_pr0-app01',
          'DIR_PROFILE' => '/sapmnt/PR0/profile',
        },
        operations: as_operations,
      )
    end

    context 'with legacy start profiles' do
      before(:each) do
        params.merge!(
          as_instances: [
            {
              type: 'DVEBMGS',
              number: '00',
              host: 'pr0-app00',
              legacy_start_profile: true,
            },
            {
              type: 'D',
              number: '00',
              host: 'pr0-app01',
              legacy_start_profile: true,
            },
          ],
        )
      end

      it 'uses the other start profile format' do
        is_expected.to contain_cs_primitive('prm_PR0_AS_pr0-app00').with(
          parameters: {
            'InstanceName' => 'PR0_DVEBMGS00_pr0-app00',
            'START_PROFILE' => '/sapmnt/PR0/profile/START_DVEBMGS00_pr0-app00',
            'DIR_PROFILE' => '/sapmnt/PR0/profile',
          },
        )
        is_expected.to contain_cs_primitive('prm_PR0_AS_pr0-app01').with(
          parameters: {
            'InstanceName' => 'PR0_D00_pr0-app01',
            'START_PROFILE' => '/sapmnt/PR0/profile/START_D00_pr0-app01',
            'DIR_PROFILE' => '/sapmnt/PR0/profile',
          },
        )
      end
    end

    it 'starts the AS resources after the SCS has been promoted' do
      params[:as_instances].each do |instance|
        host = instance[:host]
        prim_scs = "prm_#{params[:sid]}_SCS"
        prim_as = "prm_#{params[:sid]}_AS_#{host}"
        is_expected.to contain_cs_order("order-promote-ms_#{prim_scs}-then-start-#{prim_as}").with(
          cib: 'sap',
          first: "ms_#{prim_scs}:promote",
          second: "#{prim_as}:start",
          symmetrical: false,
        ).that_requires(["Cs_primitive[#{prim_scs}]", "Cs_primitive[#{prim_as}]"])
      end
    end

    it 'in log_shipping mode it starts the AS resources after the database resource has been promoted' do
      params[:as_instances].each do |instance|
        host = instance[:host]
        prim_db = "prm_#{params[:sid]}_DB"
        prim_as = "prm_#{params[:sid]}_AS_#{host}"
        is_expected.to contain_cs_order("order-promote-ms_#{prim_db}-then-start-#{prim_as}").with(
          cib: 'sap',
          first: "ms_#{prim_db}:promote",
          second: "#{prim_as}:start",
          symmetrical: false,
        ).that_requires(["Cs_primitive[#{prim_db}]", "Cs_primitive[#{prim_as}]"])
      end
    end

    it 'in shared_storage mode it starts the AS resources after the database primitive has started' do
      params[:db_mode] = 'shared_storage'
      params[:as_instances].each do |instance|
        host = instance[:host]
        prim_db = "prm_#{params[:sid]}_DB"
        prim_as = "prm_#{params[:sid]}_AS_#{host}"
        is_expected.to contain_cs_order("order-start-#{prim_db}-then-start-#{prim_as}").with(
          cib: 'sap',
          first: "#{prim_db}:start",
          second: "#{prim_as}:start",
          symmetrical: false,
        ).that_requires(["Cs_primitive[#{prim_db}]", "Cs_primitive[#{prim_as}]"])
      end
    end

    it 'does not locate the primitives on the db nodes or the wrong nodes' do
      params[:as_instances].each do |instance|
        host = instance[:host]
        prim_as = "prm_#{params[:sid]}_AS_#{host}"
        params[:nodes].each do |node, roles|
          if !roles.include? 'app-standard'
            is_expected.not_to contain_cs_location("locate-#{prim_as}-#{node}")
          elsif !%r{#{host}}.match?(node)
            is_expected.not_to contain_cs_location("locate-#{prim_as}-#{node}")
          else
            is_expected.to contain_cs_location("locate-#{prim_as}-on-#{node}").with(
              cib: 'sap',
              primitive: prim_as,
              node_name: node,
              score: 'INFINITY',
            ).that_requires("Cs_primitive[#{prim_as}]")
          end
        end
      end
    end
  end
end
