# This module manages SAP prerequisites for several types of SAP installations
# based on corresponding SAP OSS notes
#
# @summary Installs prerequisites for an SAP environment on RedHat derivatives
#
# @param system_ids
#   An hash of SAP system IDs (SIDs) which will be present on the target
#   host along with the following possible attributes for each SID:
#   * `components` - a list of associated sap applications to enable for this
#                    SID (mandatory)
#   * `backend_databases` - types of databases to which this system connects
#                           (mandatory for SIDs with 'base' enabled)
#   * `manage_profiles` - Flag indicating whether the instance profiles for this
#                         should be managed by the module. If specified
#                         `instances`, `databases`, and `primary_backend_db`
#                         become mandatory parameters!
#   * `exclude_common_mount_points` - flag indicating if this SID should skip
#                                     SID specific entries for the 'common'
#                                     mountpoint. Some system types do not have
#                                     /sapmnt directories (e.g. a DAA instance
#                                     or liveCache)
#   * `pattern_map` - A hash mapping pattern lookup keys to regular expressions
#                     which are unique to this SID. Note that if one is added it
#                     must have a corresponding value added in `pattern_values`
#   * `pattern_values` - Mapping from a pattern lookup key to the value which
#                        should be provided wherever the corresponding pattern
#                        is found.
#   * `instances` - Hash detailing the instances which are part of this SID.
#                   Entries are keyed by `instance class` and contain the
#                   corresponding Sap::SidInstance detail. See [Data Types in
#                   the readme](./README.md#data-types) for detail.
#   * `databases` - Hash detailing the database parameters for this SID.
#                   Entries in this hash are keyed by `instance class`. For
#                   detail, see [Data Types in the readme](./README.md#data-types)
#   * `primary_backend_db` - Sap::InstClassDb of the primary backend database
#                            for this node
#   * `default_pfl_entries` - Hash of profile keys and the desired value which
#                             will be appended to the end of the DEFAULT.PFL
#                             file for this instance. Currently not validated.
#   * `sec_info_rules` - Array of Sap::SecInfo entries which will be inserted
#                        before the final rules in the secinfo file.
#   * `reg_info_rules` - Array of Sap::RegInfo entries which will be inserted
#                        before the final rules in the reginfo file.
#   * `db2_query_schedules` - Array of Sap::Db2QuerySchedule entries describing
#                             a query script file, associated parameters, and
#                             the desired cron schedule entry for each. These
#                             queries are then configured to execute as the 
#                             DB2 instance owner on the host system.
#
#   Note that each entry must be exactly 3 characters in length and contain
#   exclusively uppercase characters. Also, if `base` is included in the
#   components list for an SID at least one backend_database entry must be
#   provided.
#
# @param pattern_map
#   Hash mapping a given global key such as 'sid_upper' to the corresponding
#   regexp used to substitute this value in standard strings. See the
#   mount_point defined type for detail. Note that the values in this hash
#   will be converted to regular expressions via `Regexp($value)`
#
# @param create_mount_points
#   Indicates whether the standard mount points should be created for the
#   specified instance.
#
# @param mount_points
#   Defines the mount points and supporting directories which should be created
#   for each component type. Note that this structure is a deep hash with a `--`
#   knockout value.
#
# @param router_oss_realm
#   Specify OSS realm for SAP router connection. For example,
#   `'p:CN=hostname.domain.tld, OU=0123456789, OU=SAProuter, O=SAP, C=DE'`
#
# @param manage_mount_dependencies
#   When enabled this module will install and configure the puppet-nfs module
#   detailed here: https://forge.puppet.com/derdanne/nfs
#   Note that currently only NFSv4 is supported for clients.
#
# @param router_rules
#   Specify array of rules for the SAP router
#
# @param distro_text
#   Modify text in /etc/redhat-release
#
# @param component_packages
#   Hash mapping each component type to an array containing the names of the
#   requried packages on this operating system.
#
# @param backenddb_packages
#   Hash mapping each backend database type to an array containing the names
#   of the requried packages on this operating system.
#
# @param message_servers
#   Hash detailing the message server connection definitions for other systems
#   this particular instance will need to communicate with. For entry structure
#   detail See [Data Types in the readme](./README.md#data-types).
#
# @param bash_source_profile
#   When enabled the `.profile` generated by the SAP installer will be sourced
#   by the .bash_profile of each sidadm user if that file exists.
#
# @param manage_firewall
#   When enabled all relevant firewall rules for the local system will be
#   created and managed via firewalld.
#
# @param deployed_fonts
#   Hash with a list of fonts to deploy and their source locations. Each file in
#   this structure will be installed in
#   `/usr/sap/${SAPSYSTEMNAME}/SYS/global/AdobeDocumentServices/FontManagerService/fonts/customer/`
#   for each local SID with the ADS component enabled.
#
# @param deployed_xdc
#   Hash with a list of xdc files to deploy and their source locations. Each file in
#   this structure will be installed in
#   `/usr/sap/${sid}/SYS/global/AdobeDocumentServices/lib/XDC/Customer/`
#   for each local SID with the ADS component enabled.
#
# @example Application server containing an ADS instance and a Netweaver instance
#  class { 'sap':
#    system_ids       => {
#      'PR0'          => {
#        'components'       => [ 
#           'base',
#           'base-extended',
#           'ads',
#        ],
#        'backend_database' => [ 'db2' ],
#      },
#    },
#  }
#
# @author Phil DeMonaco <pdemon@gmail.com>
#
class sap (
  Hash[Sap::SID, Sap::SIDConfigEntry] $system_ids,
  Hash[String, String] $pattern_map,
  Boolean $create_mount_points                                  = false,
  Hash[Sap::SapComponent, Sap::MountPoints] $mount_points       = {},
  Boolean $manage_mount_dependencies                            = false,
  Optional[String] $router_oss_realm                            = undef,
  Optional[Array[String]] $router_rules                         = undef,
  Optional[String] $distro_text                                 = undef,
  Hash[Sap::SapComponent, Array[String]] $component_packages    = {},
  Hash[Sap::BackendDatabase, Array[String]] $backenddb_packages = {},
  Hash[Sap::SID, Sap::MessageServer] $message_servers           = {},
  Boolean $bash_source_profile                                  = false,
  Boolean $manage_firewall                                      = false,
  Sap::SourcedFile $deployed_fonts                              = {},
  Sap::SourcedFile $deployed_xdc                                = {},
) inherits sap::params {
  # Validate message server entries
  $sap::message_servers.each |$sid, $data| {
    $mandatory = ['class', 'number']

    # Ensure all mandatory keys are present
    $mandatory.each |$key| {
      unless($key in $data) {
        $message = @("EOT"/L)
          message_server: '${sid}' entry missing mandatory parameter '${key}'!
          |-EOT
        fail($message)
      }
    }

    # Check the content of all present keys
    $data.each |$key, $value| {
      case $key {
        'class': {
          $type = Sap::InstClassSapCs
          $type_name = 'Sap::InstClassSapCs'
        }
        'number': {
          $type = Sap::InstNumber
          $type_name = 'Sap::InstNumber'
        }
        'port_override': {
          $type = Stdlib::Port
          $type_name = 'Stdlib::Port'
        }
        default: {} # impossible to reach case
      }

      unless($value =~ $type) {
        $message = @("EOT"/L)
          message_server: '${sid}' entry parameter '${key}' must be type \
          '${type_name}'!
          |-EOT
        fail($message)
      }
    }
  }

  # Validate SID configuration entries
  $sap::system_ids.each |$sid, $data| {
    # Ensure Profile generation parameters are present
    if $data['manage_profiles'] {
      $mandatory_profile_keys = ['primary_backend_db', 'instances']
      $mandatory_profile_keys.each |$key| {
        unless($key in $data) {
          $message = @("EOT"/L)
            '${sid}': parameter '${key}' is mandatory when 'manage_profiles' \
            is enabled!
            |-EOT
          fail($message)
        }
      }

      $primary_backend_db = $data['primary_backend_db']
      case $primary_backend_db {
        'none': {} # Systems with no databases do nothing here
        default: {
          unless('databases' in $data) {
            $message = @("EOT"/L)
              '${sid}': parameter 'databases' is mandatory when \
              'primary_backend_db' is not 'none'!
              |-EOT
            fail($message)
          }

          unless($primary_backend_db in $data['databases']) {
            $message = @("EOT"/L)
              '${sid}': missing primary_backend_db entry \
              '${primary_backend_db}' in 'databases'!
              |-EOT
            fail($message)
          }
        }
      }
    }

    # Validate component dependencies 
    $comps = $data['components']
    # Ensure a backend is present for application servers
    if 'base' in $comps and !('backend_databases' in $data) {
      $message = @("EOT"/L)
        SID ${sid}: All application servers must specify at least one \
        backend database! ('base' implies application server)
        |-EOT
      fail($message)
    }

    # Ensure liveCache_client is present whenever liveCache is included
    if 'liveCache' in $comps and !('liveCache_client' in $comps) {
      $message = @("EOT"/L)
        SID ${sid}: liveCache_client is mandatory for liveCache \
        database nodes!
        |-EOT
      fail($message)
    }

    # Validate instances parameter
    if('instances' in $data) {
      $instances = $data['instances']
    } else {
      $instances = {}
    }
    $instances.each |$inst_class, $inst_data| {
      case $inst_class {
        default: {
          $type_map = {
            'number' => {
              'type' => Sap::InstNumber,
              'name' => 'Sap::InstNumber',
            },
            'types' => {
              'type' => Hash[Sap::InstType, Array[Stdlib::Fqdn]],
              'name' => 'Hash[Sap::InstType, Array[Stdlib::Fqdn]]',
            },
            'host' => {
              'type' => Stdlib::Fqdn,
              'name' => 'Stdlib::Fqdn',
            },
            'legacy_start_profile' => {
              'type' => Boolean,
              'name' => 'Boolean',
            },
          }
        }
      }
      case $inst_class {
        'cs-abap', 'cs-java', 'ers': {
          $mandatory = ['number', 'types', 'host']
        }
        default: {
          $mandatory = ['number', 'types']
        }
      }

      # Ensure all mandatory keys are present
      $mandatory.each |$key| {
        unless($key in $inst_data) {
          $message = @("EOT"/L)
            '${sid}' instance: '${inst_class}' entry missing mandatory parameter \
            '${key}'!
            |-EOT
          fail($message)
        }
      }

      # Check the content of all present keys
      $inst_data.each |$key, $value| {
        $type = $type_map[$key]['type']
        $type_name = $type_map[$key]['name']
        unless($value =~ $type) {
          $message = @("EOT"/L)
            '${sid}' instance: '${inst_class}' entry parameter '${key}' must be \
            type '${type_name}'!
            |-EOT
          fail($message)
        }
      }
    }

    # Validate SID databases
    if('databases' in $data) {
      $databases = $data['databases']
    } else {
      $databases = {}
    }
    $databases.each |$db_class, $db_data| {
      # Check for HADR configuration details
      case $db_data['ha_mode'] {
        'hadr': {
          unless('hadr_base_port' in $db_data) {
            $message = @("EOT"/L)
              '${sid}' database: '${db_class}' parameter 'hadr_base_port' is \
              mandatory when 'ha_mode' is 'hadr'!
              |-EOT
            fail($message)
          }
        }
        default: {} # nothing to do here yet
      }
    }
  }

  # Build the unique list of enabled_components
  $components_wip = $system_ids.map |$entry| {
    $sid = $entry[0]
    unless('components' in $entry[1]) {
      fail("SID '${sid}' must have at least one component!")
    }
    $entry[1]['components']
  }
  $enabled_components = unique(flatten($components_wip))

  # Build the unique list of backend databases on this node
  $backends_wip = $system_ids.map |$entry| {
    $sid = $entry[0]
    if 'backend_databases' in $entry[1] {
      $entry[1]['backend_databases']
    } else {
      {}
    }
  }
  $backend_databases = unique(flatten($backends_wip))

  # Fail if dependencies are not met
  $base_enabled = 'base' in $enabled_components
  $sap::params::requires_base.each |$component| {
    if ($component in $enabled_components) {
      unless($base_enabled) {
        fail("Component '${component}' requires 'base'!")
      }
    }
  }

  # Fail if a backend_database is not specified for a node which is not
  # 'database only' (we're assuming if base is enabled that's the case)
  if ($base_enabled and empty($backend_databases)) {
    fail('All application servers must specify a backend database!')
  }

  $base_extended_enabled = 'base_extended' in $enabled_components
  $sap::params::requires_base_extended.each |$component| {
    if ($component in $enabled_components) {
      unless($base_extended_enabled) {
        fail("Component '${component}' requires 'base_extended'!")
      }
    }
  }

  # Convert the pattern_map_global values to regular expressions
  $pattern_map_temp = $pattern_map.map |$entry| {
    [$entry[0], Regexp($entry[1])]
  }
  $regex_map = Hash.new(flatten($pattern_map_temp))

  # Construct the default pattern listing
  $temp_sid_case_mapping = $system_ids.map |$entry| {
    $sid = $entry[0]
    $data = $entry[1]

    # Return_value
    [
      $sid,
      {
        'sid_upper' => $sid,
        'sid_lower' => downcase($sid),
      },
    ]
  }
  $sid_case_mapping = Hash.new(flatten($temp_sid_case_mapping))

  # liveCache client cannot be specified for more than one SID
  $lc_sids = $system_ids.filter |$sid, $data| {
    'liveCache_client' in $data['components']
  }
  if $lc_sids.length > 1 {
    fail('At most one liveCache SID can be specified!')
  }

  # Start workflow
  if $facts['os']['family'] == 'RedHat' {
    # Ensure the install, config, and service componets happen within here
    contain sap::install
    contain sap::config
    contain sap::management
    contain sap::service

    Class['sap::install']
    -> Class['sap::config']
    -> Class['sap::management']
    ~> Class['sap::service']
  } else {
    warning('The current operating system is not supported!')
  }
}
