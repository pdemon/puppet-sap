require 'spec_helper'

# See https://launchpad.support.sap.com/#/notes/1069911 for detail

describe 'Sap::TargetProgram' do
  it 'accepts a wildcard' do
    is_expected.to allow_value('*', 'atestendingin*')
  end

  it 'accepts strings that ar exactly 64 characters' do
    is_expected.to allow_value('/usr/sap/garbage/something/some/bin/thisisaverylongbinarynameee')
  end

  it 'rejects strings longer than 64 charcters' do
    is_expected.not_to allow_value('/usr/sap/garbage/something/some/bin/thisisaverylongbinarynameeeee')
  end

  it "rejects strings which includebut do not end with '*'" do
    is_expected.not_to allow_value('*aoeuaoeu', 'aoeu*aoeu')
  end
end
