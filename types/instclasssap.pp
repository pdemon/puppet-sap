type Sap::InstClassSap = Variant[
  Sap::InstClassSapApp,
  Sap::InstClassSapCs,
  Enum[
    'webdisp',
    'gateway',
    'ers',
  ],
]
