require 'spec_helper'
require 'deep_merge'

describe 'sap::management::ads_fonts' do
  let(:xdc_list) do
    {
      'customer_00_zpl300.xdc' => 'puppet:///module_specific/sap/customer_00_zpl300.xdc',
      'customer_00_pcl.xdc' => 'puppet:///module_specific/sap/customer_00_pcl.xdc',
    }
  end
  let(:font_list) do
    {
      'MYRIADPSBC.TFF' => 'puppet:///module_specific/sap/MYRIADPSBC.TFF',
      'HACK.TFF' => 'puppet:///module_specific/sap/HACK.TFF',
    }
  end
  let(:components_ads) do
    [
      'base',
      'base_extended',
      'ads',
    ]
  end
  let(:components_as) do
    [
      'base',
    ]
  end
  let(:components_db2) do
    [
      'db2',
    ]
  end

  context 'multiple SIDs' do
    let(:params) do
      {
        system_ids: {
          'JS0' => {
            components: components_ads,
            manage_profiles: true,
          },
          'PRD' => {
            components: components_as,
            manage_profiles: true,
          },
        },
        deployed_fonts: font_list,
        deployed_xdc: xdc_list,
      }
    end

    it 'compiles without error' do
      is_expected.to compile.with_all_deps
    end

    it 'detects ads for each local SID and deploys the files' do
      params[:system_ids].each do |sid, sid_data|
        font_dir = "/usr/sap/#{sid}/SYS/global/AdobeDocumentServices/FontManagerService/fonts/customer"
        xdc_dir = "/usr/sap/#{sid}/SYS/global/AdobeDocumentServices/lib/XDC/Customer"
        if sid_data[:components].include?('ads')
          file_params = {
            ensure: 'file',
            mode: '0644',
            owner: "#{sid.downcase}adm",
            group: 'sapsys',
          }
          xdc_list.each do |file, source|
            is_expected.to contain_file("#{xdc_dir}/#{file}").with(
              file_params.merge(source: source),
            )
          end
          font_list.each do |file, source|
            is_expected.to contain_file("#{font_dir}/#{file}").with(
              file_params.merge(source: source),
            )
          end
        else
          xdc_list.each do |file, _source|
            is_expected.not_to contain_file("#{xdc_dir}/#{file}")
          end
          font_list.each do |file, _source|
            is_expected.not_to contain_file("#{font_dir}/#{file}")
          end
        end
      end
    end
  end
end
