# This artificially restricts vg names to [a-zA-Z0-9_]{2,} for 
# simplification purposes.
type Sap::VgLvName = Pattern[/[a-zA-Z0-9_]{2,}/]
