require 'spec_helper'
require 'deep_merge'

common_packages = [
  'uuidd',
  'numactl',
  'numad',
  'hwloc',
  'libhugetlbfs',
  'libhugetlbfs-utils',
  'compat-db47',
  'compat-glibc',
  'compat-libcap1',
  'compat-libf2c-34',
  'compat-libgfortran-41',
  'compat-libtiff3',
  'compat-openldap',
  'libpng12',
  'openssl098e',
  'autofs',
  'cgdcbxd',
  'cifs-utils',
  'device-mapper-multipath',
  'fcoe-utils',
  'gssproxy',
  'iscsi-initiator-utils',
  'lldpad',
  'samba-client',
  'targetcli',
  'blktrace',
  'sysstat',
  'dstat',
  'iotop',
  'iowatcher',
  'latencytop',
  'latencytop-tui',
  'oprofile',
  'parfait',
  'pcp',
  'perf',
  'powertop',
]

describe 'sap', type: :class do
  context 'on RedHat family' do
    let(:facts) do
      {
        page_size: 4096,
        memory: {
          system: {
            total_bytes: 7_930_249_216,
          },
          swap: {
            total_bytes: 4_294_901_760,
          },
        },
        mountpoints: {
          '/dev/shm' => {
            size_bytes: 3_961_782_272,
          },
        },
        os: {
          family: 'RedHat',
          release: { major: '7' },
        },
        osfamily: 'RedHat',
        operatingsystem: 'RedHat',
        operatingsystemmajrelease: '7',
      }
    end

    let(:params) do
      {
        system_ids: { 'EP0' => {} },
      }
    end

    context 'message_servers' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'base',
            ],
            backend_databases: ['db6'],
          },
        )
      end

      it 'contains SID missing required keys' do
        params[:message_servers] = { 'PRD' => { 'class' => 'cs-abap' } }
        is_expected.to compile.with_all_deps.and_raise_error(%r{message_server: 'PRD' entry missing mandatory parameter 'number'!})
        params[:message_servers] = { 'PRD' => { 'number' => '00' } }
        is_expected.to compile.with_all_deps.and_raise_error(%r{message_server: 'PRD' entry missing mandatory parameter 'class'!})
      end

      it 'contains SID with mixed key types' do
        params[:message_servers] = {
          'PRD' => {
            class: '00',
            number: 'cs-abap',
          },
        }
        is_expected.to compile.with_all_deps.and_raise_error(%r{message_server: 'PRD' entry parameter 'class' must be type 'Sap::InstClassSapCs'!})
      end
    end

    context 'system_ids entry' do
      let(:sid) { 'PRD' }
      let(:instances) do
        {
          'as-abap' => {
            'number' => '00',
            'types' => {
              'D' => ['prd-app01.example.org'],
              'DVEBMGS' => ['prd-app00.example.org'],
            },
          },
          'cs-abap' => {
            'number' => '01',
            'types' => {
              'ASCS' => ['prd-app00.example.org', 'prd-app01.example.org'],
            },
            'host' => 'prd-cs.example.org',
          },
        }
      end
      let(:databases) do
        {
          'db-db2' => {
            'com_port' => 5912,
            'fcm_base_port' => 5914,
            'hadr_base_port' => 55_000,
            'nodes' => [
              'ep0-dba.example.com',
              'ep0-dbb.example.com',
            ],
            'host' => 'ep0-db.example.com',
            'ha_mode' => 'hadr',
          },
        }
      end
      let(:default_pfl) do
        {
          'em/global_area_MB' => '450',
          'rspo/global_shm/job_list' => '1500',
        }
      end
      let(:reg_info) do
        [
          {
            'MODE' => 'P',
            'TP' => '*',
            'HOST' => ['*.example.org'],
            'ACCESS' => ['*.example.org'],
            'CANCEL' => ['*.example.org'],
          },
        ]
      end
      let(:sec_info) do
        [
          {
            'MODE' => 'P',
            'TP' => '*',
            'USER' => '*',
            'HOST' => ['*.example.org'],
            'USER-HOST' => ['*.example.org'],
          },
          {
            'MODE' => 'P',
            'TP' => '*',
            'USER' => '*',
            'HOST' => ['10.48.*.*'],
            'USER-HOST' => ['10.48.*.*'],
          },
        ]
      end
      let(:db2_query_schedules) do
        [
          {
            'query_file_name' => 'shipment_history_extraction',
            'query_content' => 'puppet://module_specific/sap/db2_queries/shipment_history_extraction',
            'cron_entry' => {
              'hour' => '2',
              'minute' => '15',
            },
            'output_target' => '/sapmnt/PRD/interface/outbound/scm/sdp_sh_history.txt',
            'monitor_alias' => 'prd-a-db.example.org',
          },
        ]
      end

      before(:each) do
        params[:system_ids][sid] = {
          'components' => [
            'base',
          ],
          'backend_databases' => ['db6'],
          'exclude_common_mount_points' => false,
          'pattern_map' => {},
          'pattern_values' => {},
          'databases' => databases,
          'instances' => instances,
          'primary_backend_db' => 'db-db2',
          'manage_profiles' => false,
          'default_pfl_entries' => default_pfl,
          'db2_query_schedules' => db2_query_schedules,
        }
        params[:system_ids].delete('EP0')
      end

      context 'when manage_profiles is true' do
        before(:each) do
          params[:system_ids][sid]['manage_profiles'] = true
        end

        ['instances', 'primary_backend_db'].each do |key|
          it "fails when '#{key}' is missing" do
            params[:system_ids][sid].delete(key)
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}': parameter '#{key}' is mandatory when 'manage_profiles' is enabled!})
          end
        end

        it "fails when 'primary_backend_db' is not 'none' and 'databases' is missing" do
          params[:system_ids][sid].delete('databases')
          is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}': parameter 'databases' is mandatory when 'primary_backend_db' is not 'none'!})
        end

        it "fails when 'primary_backend_db' is a valid Sap::InstClassDb but not contained in 'databases'" do
          primary_backend_db = params[:system_ids][sid]['primary_backend_db']
          params[:system_ids][sid]['databases'].delete(primary_backend_db)
          is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}': missing primary_backend_db entry '#{primary_backend_db}' in 'databases'!})
        end
      end
    end

    context 'system_ids SID entry contains databases' do
      before(:each) do
        params[:system_ids]['PRD'] = {
          backend_databases: ['db6'],
          manage_profiles: false,
          components: [
            'base',
          ],
          databases: {
            'db-db2' => {
              'com_port' => 5912,
              'fcm_base_port' => 5914,
              'hadr_base_port' => 55_000,
              'nodes' => [
                'prd-dba.example.com',
                'prd-dbb.example.com',
              ],
              'host' => 'prd-db.example.com',
              'ha_mode' => 'hadr',
            },
          },
        }
        params[:system_ids].delete('EP0')
      end

      context 'with a db-db2 instance' do
        let(:sid) { 'PRD' }

        it 'compiles with valid parameters' do
          is_expected.to compile.with_all_deps
        end
        it "fails when ha_mode is 'hadr' and hadr_base_port is missing" do
          params[:system_ids][sid][:databases]['db-db2']['ha_mode'] = 'hadr'
          params[:system_ids][sid][:databases]['db-db2'].delete('hadr_base_port')
          is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' database: 'db-db2' parameter 'hadr_base_port' is mandatory when 'ha_mode' is 'hadr'!})
        end
      end
    end

    context 'system_ids SID entry contains instances' do
      let(:instances) do
        {
          'as-abap' => {
            'number' => '00',
            'types' => {
              'D' => ['prd-app01.example.com', 'prd-app02.example.com'],
              'DVEBMGS' => ['prd-app00.example.com'],
            },
          },
          'as-dual' => {
            'number' => '00',
            'types' => {
              'D' => ['prd-app01.example.com', 'prd-app02.example.com'],
              'DVEBMGS' => ['prd-app00.example.com'],
            },
          },
          'as-java' => {
            'number' => '10',
            'types' => {
              'J' => ['prd-app00.example.com', 'prd-app01.example.com'],
            },
          },
          'webdisp' => {
            'number' => '50',
            'types' => {
              'W' => ['prd-app00.example.com', 'prd-app01.example.com'],
            },
          },
          'gateway' => {
            'number' => '60',
            'types' => {
              'G' => ['prd-app00.example.com', 'prd-app01.example.com'],
            },
          },
          'cs-java' => {
            'number' => '00',
            'types' => {
              'SCS' => ['prd-app00.example.com', 'prd-app01.example.com', 'prd-app02.example.com'],
            },
            'host' => 'prd-cs.example.com',
          },
          'cs-abap' => {
            'number' => '01',
            'types' => {
              'ASCS' => ['prd-app00.example.com', 'prd-app01.example.com', 'prd-app02.example.com'],
            },
            'host' => 'prd-cs.example.com',
          },
          'ers' => {
            'number' => '02',
            'types' => {
              'ERS' => ['prd-app00.example.com', 'prd-app01.example.com', 'prd-app02.example.com'],
            },
            'host' => 'prd-ers.example.com',
          },
        }
      end
      let(:sid) { 'PRD' }

      before(:each) do
        params[:system_ids]['PRD'] = {
          backend_databases: ['db6'],
          manage_profiles: false,
          components: [
            'base',
          ],
          instances: instances,
        }
        params[:system_ids].delete('EP0')
      end

      it 'compiles with valid parameters' do
        is_expected.to compile.with_all_deps
      end

      inst_classes = [
        'as-abap',
        'as-java',
        'as-dual',
        'cs-java',
        'cs-abap',
        'webdisp',
        'gateway',
        'ers',
      ]
      inst_classes_host_mandatory = [
        'cs-java',
        'cs-abap',
        'ers',
      ]
      mandatory_keys_base = ['number', 'types']
      inst_classes.each do |instclass|
        mandatory_keys = case instclass
                         when *inst_classes_host_mandatory
                           mandatory_keys_base + ['host']
                         else
                           mandatory_keys_base
                         end

        mandatory_keys.each do |key|
          it "fails when type '#{instclass}' is missing '#{key}'" do
            params[:system_ids][sid][:instances][instclass].delete(key)
            is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' instance: '#{instclass}' entry missing mandatory parameter '#{key}'!})
          end
        end
      end

      data_types = {
        'number' => {
          'name' => 'Sap::InstNumber',
          'bad_value' => 'prd-cs.example.com',
        },
        'types' => {
          'name' => 'Hash\[Sap::InstType, Array\[Stdlib::Fqdn\]\]',
          'bad_value' => false,
        },
        'host' => {
          'name' => 'Stdlib::Fqdn',
          'bad_value' => {},
        },
        'legacy_start_profile' => {
          'name' => 'Boolean',
          'bad_value' => {},
        },
      }
      data_types.each do |key, key_data|
        it "fails when '#{key}' is not '#{key_data['name']}'" do
          params[:system_ids][sid][:instances]['cs-abap'][key] = key_data['bad_value']
          is_expected.to compile.with_all_deps.and_raise_error(%r{'#{sid}' instance: 'cs-abap' entry parameter '#{key}' must be type '#{key_data['name']}'!})
        end
      end
    end

    context 'ads requires base' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'ads',
            ],
            backend_databases: ['db6'],
          },
        )
      end

      it { is_expected.to compile.with_all_deps.and_raise_error(%r{Component 'ads' requires 'base'!}) }
    end

    context 'ads requires base_extended' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'base',
              'ads',
            ],
            backend_databases: ['db6'],
          },
        )
      end

      it { is_expected.to compile.with_all_deps.and_raise_error(%r{Component 'ads' requires 'base_extended'!}) }
    end

    context 'a backend database must be specified when base is enabled' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'base',
            ],
          },
        )
      end

      it {
        is_expected.to compile.with_all_deps.and_raise_error(%r{SID EP0: All application servers must specify at least one backend database!})
      }
    end

    context 'liveCache specified without liveCache_client' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'liveCache',
            ],
          },
        )
      end

      it 'fails on the dependency' do
        is_expected.to compile.with_all_deps.and_raise_error(%r{SID EP0: liveCache_client is mandatory for liveCache database nodes!})
      end
    end

    context 'liveCache_client specified for more than one SID' do
      before(:each) do
        params[:system_ids].merge!(
          'EP0' => {
            manage_profiles: false,
            components: [
              'liveCache_client',
            ],
          },
          'EP1' => {
            manage_profiles: false,
            components: [
              'liveCache_client',
            ],
          },
        )
      end

      it 'fails on the dependency' do
        is_expected.to compile.with_all_deps.and_raise_error(%r{At most one liveCache SID can be specified!})
      end
    end
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      before(:each) do
        facts.merge!(
          page_size: 4096,
          memory: {
            system: {
              total_bytes: 7_930_249_216,
            },
            swap: {
              total_bytes: 4_294_901_760,
            },
          },
          mountpoints: {
            '/dev/shm' => {
              size_bytes: 3_961_782_272,
            },
          },
        )

        case facts[:operatingsystemmajrelease]
        when '6'
          facts[:os].merge!(
            family: 'RedHat',
            name: facts[:operatingsystem],
            release: {
              major: '6',
              full: facts[:operatingsystemrelease],
            },
          )
        when '7'
          facts[:os].merge!(
            family: 'RedHat',
            name: facts[:operatingsystem],
            release: {
              major: '7',
              full: facts[:operatingsystemrelease],
            },
          )
        end

        case facts[:architecture]
        when 'x86_64'
          facts[:os].merge!(
            architecture: 'x86_64',
            hardware: 'x86_64',
          )
        when 'ppc64'
          facts[:os].merge!(
            architecture: 'ppc64',
            hardware: 'ppc64',
          )
        else
          facts[:os].merge!(
            architecture: 'x86_64',
            hardware: 'x86_64',
          )
        end
      end

      context 'liveCache only' do
        let(:params) do
          {
            system_ids: {
              'SCP' => {
                manage_profiles: false,
                components: [
                  'liveCache',
                  'liveCache_client',
                ],
                pattern_values: {
                  lcid_upper: 'SLP',
                },
              },
            },
            create_mount_points: true,
          }
        end

        it 'installs the common packages' do
          common_packages.each do |package|
            is_expected.to contain_package(package)
          end
        end

        it 'installs the liveCache specific package' do
          is_expected.to contain_package('compat-libstdc++-33')
        end

        ['ppc64', 'x86_64'].each do |arch|
          context "on #{arch}" do
            before(:each) do
              facts[:os].merge!(
                architecture: arch,
                hardware: arch,
              )
            end

            it 'installs the 32-bit liveCache dependencies' do
              arch_suffix = case arch
                            when 'ppc64'
                              'ppc'
                            when 'x86_64'
                              'i686'
                            else
                              'unknown_arch'
                            end
              is_expected.to contain_package("compat-libstdc++-33.#{arch_suffix}")
            end
          end
        end

        # test the livecache database mount points
        it 'creates the database mount points' do
          is_expected.to contain_sap__config__mount_point('/sapdb')
          is_expected.to contain_file('/sapdb').with(
            ensure: 'directory',
            owner: 'root',
            group: 'root',
            mode: '0755',
          )
          sid = 'SCP'
          lcid_upper = params[:system_ids][sid][:pattern_values][:lcid_upper]
          dirs = {
            '/sapdb/data' => { mode: '0775', per_sid: true },
            '/sapdb/programs' => { mode: '0555', per_sid: true },
            '/sapdb/_LID_' => { mode: '0555', per_sid: true },
            '/sapdb/_LID_/db' => { mode: '0555', per_sid: true },
            '/sapdb/_LID_/sapdata' => { mode: '0750', per_sid: true },
            '/sapdb/_LID_/saplog' => { mode: '0750', per_sid: true },
          }
          dirs.each do |dir_base, detail|
            dir_sub = dir_base.gsub(%r{_LID_}, lcid_upper)
            dir_parent = File.dirname(dir_sub)
            mount_name = if detail[:per_sid]
                           "#{dir_base}_#{sid}"
                         else
                           dir_base
                         end
            is_expected.to contain_sap__config__mount_point(mount_name)
            is_expected.to contain_file(dir_sub).with(
              ensure: 'directory',
              owner: "#{sid.downcase}db",
              group: "#{sid.downcase}dba",
              mode: detail[:mode],
            ).that_requires("File[#{dir_parent}]")
          end
        end
      end

      context 'full config' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: [
                  'base',
                  'base_extended',
                  'experimental',
                  'ads',
                  'bo',
                  'cloudconnector',
                  'hana',
                  'router',
                ],
                backend_databases: ['db6'],
              },
            },
            router_oss_realm: 'p:CN=sr.domain.tld, OU=0123456789, OU=SAProuter, O=SAP, C=DE',
            router_rules: [
              'P0,1  *  192.168.1.1  3200  password  # SID dispatcher',
              'P0,1  *  192.168.1.2  3200  password  # SID dispatcher',
            ],
            distro_text: 'Best distribution ever build version 7.2',
          }
        end

        it 'compiles' do
          is_expected.to compile.with_all_deps
        end

        it 'contains all expected classes' do
          is_expected.to contain_class('sap::params')
          is_expected.to contain_class('sap::install')
          is_expected.to contain_class('sap::config').that_requires(
            'Class[sap::install]',
          )
          is_expected.to contain_class('sap::config::common')
          is_expected.to contain_class('sap::config::limits')
          is_expected.to contain_class('sap::config::sysctl')
          is_expected.to contain_class('sap::config::tmpfs')
          is_expected.to contain_class('sap::config::router')
          is_expected.to contain_class('sap::management').that_requires(
            'Class[sap::config]',
          )
          is_expected.to contain_class('sap::management::etc_services')
          is_expected.to contain_class('sap::management::ads_fonts')
          is_expected.to contain_class('sap::management::permissions')
          is_expected.to contain_class('sap::service').that_subscribes_to(
            'Class[sap::management]',
          )
          is_expected.to contain_class('sap::service::router')
        end

        it 'does not contain the db2 classes' do
          is_expected.not_to contain_class('sap::management::db2_queries')
        end

        context 'bash_source_profile enabled' do
          before(:each) do
            params.merge!(
              bash_source_profile: true,
            )
          end

          it 'includes the profile management class' do
            is_expected.to contain_class('sap::management::dotfiles')
          end
        end

        context 'manage_firewall is enabled' do
          before(:each) do
            params.merge!(
              manage_firewall: true,
            )
          end

          it 'includes the firewall management class' do
            is_expected.to contain_class('sap::management::firewall')
          end
        end

        it 'ensures all common packages are present' do
          common_packages.each do |package|
            is_expected.to contain_package(package).with_ensure('installed')
          end
        end

        it 'ensures the base packges are present' do
          is_expected.to contain_package('compat-libstdc++-33').with_ensure('installed')
          is_expected.to contain_package('elfutils-libelf-devel').with_ensure('installed')
          is_expected.to contain_package('gcc-c++').with_ensure('installed')
          is_expected.to contain_package('glibc').with_ensure('installed')
          is_expected.to contain_package('glibc-devel').with_ensure('installed')
          is_expected.to contain_package('glibc-headers').with_ensure('installed')
          is_expected.to contain_package('libaio').with_ensure('installed')
          is_expected.to contain_package('libaio-devel').with_ensure('installed')
          is_expected.to contain_package('libstdc++').with_ensure('installed')
          is_expected.to contain_package('libstdc++-devel').with_ensure('installed')
          is_expected.to contain_package('tcsh').with_ensure('installed')
          is_expected.to contain_package('xorg-x11-utils').with_ensure('installed')
        end

        it 'ensures the base extension packges are present' do
          is_expected.to contain_package('expat').with_ensure('installed')
          is_expected.to contain_package('libgcc').with_ensure('installed')
          is_expected.to contain_package('libX11').with_ensure('installed')
          is_expected.to contain_package('libXau').with_ensure('installed')
          is_expected.to contain_package('libxcb').with_ensure('installed')
          is_expected.to contain_package('krb5-libs').with_ensure('installed')
        end

        # Experimental packages
        it 'ensures the experimental packages are present' do
          is_expected.to contain_package('sap-common').with_ensure('installed')
          is_expected.to contain_package('sap-toolbox').with_ensure('installed')
          is_expected.to contain_package('sap-sapcar').with_ensure('installed')
        end

        # SAP ADS packages
        it 'ensures the ADS packages are present' do
          is_expected.to contain_package('autoconf').with_ensure('installed')
          is_expected.to contain_package('automake').with_ensure('installed')
          is_expected.to contain_package('transfig').with_ensure('installed')
          is_expected.to contain_package('cyrus-sasl-lib').with_ensure('installed')
          is_expected.to contain_package('fontconfig').with_ensure('installed')
          is_expected.to contain_package('freetype').with_ensure('installed')
          is_expected.to contain_package('keyutils-libs').with_ensure('installed')
          is_expected.to contain_package('libcom_err').with_ensure('installed')
          is_expected.to contain_package('libidn').with_ensure('installed')
          is_expected.to contain_package('libidn-devel').with_ensure('installed')
          is_expected.to contain_package('libselinux').with_ensure('installed')
          is_expected.to contain_package('nspr').with_ensure('installed')
          is_expected.to contain_package('nss').with_ensure('installed')
          is_expected.to contain_package('nss-softokn').with_ensure('installed')
          is_expected.to contain_package('nss-softokn-freebl').with_ensure('installed')
          is_expected.to contain_package('nss-util').with_ensure('installed')
          is_expected.to contain_package('openldap').with_ensure('installed')
          is_expected.to contain_package('zlib').with_ensure('installed')
        end

        if os_facts[:operatingsystemmajrelease] == '7'
          context 'on major release 7' do
            it 'configures RHEL7 only features' do
              is_expected.to contain_class('sap::service::cloudconnector')
            end

            # SAP BO packges
            it 'ensures the Business Objects packages are present' do
              is_expected.to contain_package('libXcursor').with_ensure('installed')
              is_expected.to contain_package('libXext').with_ensure('installed')
              is_expected.to contain_package('libXfixes').with_ensure('installed')
              is_expected.to contain_package('libXrender').with_ensure('installed')
            end

            # SAP Cloud connector packages and service
            it 'ensures the cloud connector packages are present and the service is configured' do
              is_expected.to contain_package('sapjvm_8').with_ensure('installed')
              is_expected.to contain_package('com.sap.scc-ui').with_ensure('installed')
              is_expected.to contain_service('scc_daemon').with('ensure' => 'running', 'enable' => 'true')
            end

            # SAP HANA packages
            it 'ensures the HANA packages are present' do
              is_expected.to contain_package('PackageKit-gtk3-module').with_ensure('installed')
              is_expected.to contain_package('bind-utils').with_ensure('installed')
              is_expected.to contain_package('cairo').with_ensure('installed')
              is_expected.to contain_package('expect').with_ensure('installed')
              is_expected.to contain_package('graphviz').with_ensure('installed')
              is_expected.to contain_package('gtk2').with_ensure('installed')
              is_expected.to contain_package('iptraf-ng').with_ensure('installed')
              is_expected.to contain_package('java-1.8.0-openjdk').with_ensure('installed')
              is_expected.to contain_package('krb5-workstation').with_ensure('installed')
              is_expected.to contain_package('libcanberra-gtk2').with_ensure('installed')
              is_expected.to contain_package('libicu').with_ensure('installed')
              is_expected.to contain_package('libpng12').with_ensure('installed')
              is_expected.to contain_package('libssh2').with_ensure('installed')
              is_expected.to contain_package('libtool-ltdl').with_ensure('installed')
              is_expected.to contain_package('net-tools').with_ensure('installed')
              is_expected.to contain_package('numactl').with_ensure('installed')
              is_expected.to contain_package('openssl098e').with_ensure('installed')
              is_expected.to contain_package('openssl').with_ensure('installed')
              is_expected.to contain_package('xfsprogs').with_ensure('installed')
              is_expected.to contain_package('xulrunner').with_ensure('installed')
            end

            # Test that the sap sysctl config file is created & the refresh is defined
            it 'configures and refreshes sysctl' do
              is_expected.to contain_file('/etc/sysctl.d/00-sap-base.conf').with(
                ensure: 'file',
                notify: 'Exec[sysctl-reload]',
              )

              is_expected.to contain_exec('/sbin/sysctl --system').with(
                refreshonly: true,
                alias: 'sysctl-reload',
              )
            end

            # Ensure the contents of the file match our expectations
            it 'is expected to contain valid SAP sysctl entries in /etc/sysctl.d/00-sap-base.conf' do
              content = catalogue.resource('file', '/etc/sysctl.d/00-sap-base.conf').send(:parameters)[:content]
              expect(content).to match(%r{\nkernel[.]sem = 1250 256000 100 8192\n})
              expect(content).to match(%r{\nvm[.]max_map_count = 2000000\n})
            end

            # Ensure limits is configured
            it { is_expected.to contain_file('/etc/security/limits.d/00-sap-base.conf').with_ensure('file') }

            it 'is expected to contain valid limits for the core applicaiton in /etc/security/limits.d/00-sap-base.conf' do
              content = catalogue.resource('file', '/etc/security/limits.d/00-sap-base.conf').send(:parameters)[:content]
              expect(content).to match(%r{\n@sapsys    hard    nofile    65536\n})
              expect(content).to match(%r{\n@sapsys    soft    nofile    65536\n})
              expect(content).to match(%r{\n@sapsys    soft    nproc    unlimited\n})
              expect(content).to match(%r{\n@sdba    hard    nofile    32800\n})
              expect(content).to match(%r{\n@sdba    soft    nofile    32800\n})
              expect(content).to match(%r{\n@dba    hard    nofile    32800\n})
              expect(content).to match(%r{\n@dba    soft    nofile    32800\n})
            end
          end
        end

        # General Architecture specific packages
        context 'on x86_64' do
          before(:each) do
            facts[:os].merge!(
              architecture: 'x86_64',
              hardware: 'x86_64',
            )
          end

          it 'includes the appropriate base packages for 32-bit' do
            is_expected.to contain_package('compat-libstdc++-33.i686').with_ensure('installed')
          end

          it 'includes the appropriate base_extension packages for 32-bit' do
            is_expected.to contain_package('glibc.i686').with_ensure('installed')
            is_expected.to contain_package('glibc-devel.i686').with_ensure('installed')
            is_expected.to contain_package('libgcc.i686').with_ensure('installed')
            is_expected.to contain_package('libX11.i686').with_ensure('installed')
            is_expected.to contain_package('libXau.i686').with_ensure('installed')
            is_expected.to contain_package('libxcb.i686').with_ensure('installed')
          end

          it 'includes the 32-bit packages required by ADS' do
            is_expected.to contain_package('nss-softokn-freebl.i686').with_ensure('installed')
          end

          if os_facts[:operatingsystemmajrelease] == '7'
            context 'on major release 7' do
              it 'ensures the 32-bit packages required by Business Objects are present' do
                is_expected.to contain_package('compat-libstdc++-33.i686').with_ensure('installed')
                is_expected.to contain_package('libstdc++.i686').with_ensure('installed')
                is_expected.to contain_package('libXcursor.i686').with_ensure('installed')
                is_expected.to contain_package('libXext.i686').with_ensure('installed')
                is_expected.to contain_package('libXfixes.i686').with_ensure('installed')
                is_expected.to contain_package('libXrender.i686').with_ensure('installed')
              end
            end
          end
        end

        context 'on ppc64' do
          before(:each) do
            facts[:os].merge!(
              architecture: 'ppc64',
              hardware: 'ppc64',
            )
          end

          it 'includes the appropriate base packages for 32-bit' do
            is_expected.to contain_package('compat-libstdc++-33.ppc').with_ensure('installed')
          end

          it 'includes the appropriate base_extension packages for 32-bit' do
            is_expected.to contain_package('glibc.ppc').with_ensure('installed')
            is_expected.to contain_package('glibc-devel.ppc').with_ensure('installed')
            is_expected.to contain_package('libgcc.ppc').with_ensure('installed')
            is_expected.to contain_package('libX11.ppc').with_ensure('installed')
            is_expected.to contain_package('libXau.ppc').with_ensure('installed')
            is_expected.to contain_package('libxcb.ppc').with_ensure('installed')
          end

          it 'includes the 32-bit packages required by ADS' do
            is_expected.to contain_package('nss-softokn-freebl.ppc').with_ensure('installed')
          end

          if os_facts[:operatingsystemmajrelease] == '7'
            context 'on major release 7' do
              it 'ensures the 32-bit packages required by Business Objects are present' do
                is_expected.to contain_package('compat-libstdc++-33.ppc').with_ensure('installed')
                is_expected.to contain_package('libstdc++.ppc').with_ensure('installed')
                is_expected.to contain_package('libXcursor.ppc').with_ensure('installed')
                is_expected.to contain_package('libXext.ppc').with_ensure('installed')
                is_expected.to contain_package('libXfixes.ppc').with_ensure('installed')
                is_expected.to contain_package('libXrender.ppc').with_ensure('installed')
              end
            end
          end

          # DB2 requires the following backend packages
          it 'db6 xlc compiler runtime is present' do
            is_expected.to contain_package('vacpp.rte').with_ensure('installed')
          end
        end

        # SAP router package
        it { is_expected.to contain_package('sap-router').with_ensure('installed') }

        # SAP router configuration
        it { is_expected.to contain_file('/etc/sysconfig/sap-router').with_ensure('file') }
        it 'is expected to generate valid content for sap-router' do
          content = catalogue.resource('file', '/etc/sysconfig/sap-router').send(:parameters)[:content]
          expect(content).to match('p:CN=sr.domain.tld, OU=0123456789, OU=SAProuter, O=SAP, C=DE')
        end
        it { is_expected.to contain_file('/opt/sap/R99/profile/saproutetab').with_ensure('file') }
        it 'is expected to generate valid content for saproutetab' do
          content = catalogue.resource('file', '/opt/sap/R99/profile/saproutetab').send(:parameters)[:content]
          expect(content).to match('P0,1  \*  192.168.1.1  3200  password  # SID dispatcher')
          expect(content).to match('P0,1  \*  192.168.1.2  3200  password  # SID dispatcher')
          expect(content).to match('D  \*  \*  \*  # If nothing match, traffic is denied')
        end

        # General UUID service
        it { is_expected.to contain_service('uuidd.socket').with('ensure' => 'running', 'enable' => 'true') }

        # SAP router service
        it { is_expected.to contain_service('sap-router').with('ensure' => 'running', 'enable' => 'true') }

        it { is_expected.to contain_file('/etc/redhat-release').with_ensure('file') }
        it 'must generate valid content for redhat-release' do
          content = catalogue.resource('file', '/etc/redhat-release').send(:parameters)[:content]
          expect(content).to match('Best distribution ever build version 7.2')
        end

        # Ensure tmpfs configuration is correct
        it 'correctly configures the tmpfs filesystem' do
          is_expected.to contain_file_line('fstab_tmpfs_size').with(
            ensure: 'present',
            path: '/etc/fstab',
            line: "tmpfs\t/dev/shm\ttmpfs\tsize=9g\t0 0",
          )

          is_expected.to contain_exec('remount_devshm').with(
            command: '/bin/mount -o remount /dev/shm',
            subscribe: 'File_line[fstab_tmpfs_size]',
            refreshonly: true,
          )
        end

        # Check for notify about swap space being too small
        it { is_expected.to contain_notify('SAP: Swap space may be undersized! Current 4 GiB, Target 16 GiB') }

        # Rudimentary sub 7 tests
        if os_facts[:operatingsystemmajrelease] == '6'
          context 'on major release 6' do
            it 'ensures redhat 6 packages are present' do
              is_expected.to contain_package('compat-gcc-34').with_ensure('installed')
              is_expected.to contain_package('pdksh').with_ensure('installed')
            end
          end
        end

        # True RHEL tests
        if os_facts[:operatingsystem] == 'RedHat'
          it 'ensures RHEL 7 SAP compatability packages are installed' do
            is_expected.to contain_package('sapconf').with_ensure('installed')
            is_expected.to contain_package('tuned-profiles-sap').with_ensure('installed')
          end
        end
      end

      context 'base mountpoints' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['base'],
                backend_databases: ['db6'],
              },
              'WP0' => {
                manage_profiles: false,
                components: ['base'],
                backend_databases: ['db6'],
              },
            },
            create_mount_points: true,
          }
        end

        # Test compiling
        it { is_expected.to compile.with_all_deps }

        it 'contains all mount related classes' do
          is_expected.to contain_class('sap::config')
          is_expected.to contain_class('sap::config::mount_points')
        end

        it 'creates the common directories' do
          is_expected.to contain_sap__config__mount_point('/sapmnt')
          is_expected.to contain_file('/sapmnt').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )
          is_expected.to contain_sap__config__mount_point('/usr/sap')
          is_expected.to contain_file('/usr/sap').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )
          ['EP0', 'WP0'].each do |sid|
            sid_lower = sid.downcase
            is_expected.to contain_sap__config__mount_point("/sapmnt/_SID__#{sid}")
            is_expected.to contain_file("/sapmnt/#{sid}").with(
              ensure: 'directory',
              owner: "#{sid_lower}adm",
              group: 'sapsys',
              mode: '0755',
            ).that_requires('File[/sapmnt]')
          end
        end

        it 'creates the \'base\' directories' do
          is_expected.to contain_sap__config__mount_point('/usr/sap/trans')
          is_expected.to contain_file('/usr/sap/trans').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          ).that_requires('File[/usr/sap]')

          ['EP0', 'WP0'].each do |sid|
            sid_lower = sid.downcase
            is_expected.to contain_sap__config__mount_point("/usr/sap/_SID__#{sid}")
            is_expected.to contain_file("/usr/sap/#{sid}").with(
              ensure: 'directory',
              owner: "#{sid_lower}adm",
              group: 'sapsys',
              mode: '0755',
            ).that_requires('File[/usr/sap]')
          end
        end
      end

      context 'edi directories' do
        let(:params) do
          {
            create_mount_points: true,
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['edi'],
              },
              'SCP' => {
                manage_profiles: false,
                components: ['edi'],
              },
            },
          }
        end

        sid_dirs = {
          '/sapmnt/_SID_/interface' => {},
          '/sapmnt/_SID_/.i' => { target: '/sapmnt/_SID_/interface' },
          '/sapmnt/_SID_/interface/outbound' => {},
          '/sapmnt/_SID_/interface/inbound' => {},
          '/sapmnt/_SID_/interface/log' => {},
          '/sapmnt/_SID_/interface/archive' => {},
          '/sapmnt/_SID_/interface/.o' => { target: '/sapmnt/_SID_/interface/outbound' },
          '/sapmnt/_SID_/interface/.i' => { target: '/sapmnt/_SID_/interface/inbound' },
        }
        it "creates the 'edi' directories" do
          ['EP0', 'SCP'].each do |sid|
            sid_lower = sid.downcase
            sid_dirs.each do |dir_name, data|
              dir_path = dir_name.gsub('_SID_', sid)
              dir_parent = File.dirname(dir_path)
              is_expected.to contain_sap__config__mount_point("#{dir_name}_#{sid}")

              ensure_type = if data.key?(:target)
                              'link'
                            else
                              'directory'
                            end
              params = {
                ensure: ensure_type,
                owner: "#{sid_lower}adm",
                group: 'sapsys',
                mode: '0775',
              }
              case ensure_type
              when 'link'
                params[:target] = data[:target].gsub('_SID_', sid)
              end
              requires = case ensure_type
                         when 'link'
                           "File[#{params[:target]}]"
                         else
                           "File[#{dir_parent}]"
                         end
              is_expected.to contain_file(dir_path).with(params).that_requires(requires)
            end
          end
        end
      end

      context 'nfsv4 sapmnt with nfs management' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['base'],
                backend_databases: ['db6'],
              },
              'WP0' => {
                manage_profiles: false,
                components: ['base'],
                backend_databases: ['db6'],
              },
            },
            create_mount_points: true,
            manage_mount_dependencies: true,
            mount_points: {
              common: {
                '/sapmnt' => {
                  per_sid: false,
                  file_params: {
                    owner: 'root',
                    group: 'sapsys',
                    mode: '0755',
                  },
                },
                '/sapmnt/_SID_' => {
                  per_sid: true,
                  file_params: {
                    owner: '_sid_adm',
                    group: 'sapsys',
                    mode: '0755',
                  },
                  mount_params: {
                    managed: true,
                    type: 'nfsv4',
                    server: 'nfs.puppet.sap',
                    share: '/srv/nfs/sapmnt/_SID_',
                  },
                  required_files: [
                    '/sapmnt',
                  ],
                },
              },
              base: {
                '/usr/sap' => {
                  per_sid: false,
                  file_params: {
                    owner: 'root',
                    group: 'sapsys',
                    mode: '0755',
                  },
                },
                '/usr/sap/trans' => {
                  per_sid: false,
                  file_params: {
                    owner: 'root',
                    group: 'sapsys',
                    mode: '0755',
                  },
                  mount_params: {
                    managed: true,
                    type: 'nfsv4',
                    server: 'nfs.puppet.sap',
                    share: '/srv/nfs/trans',
                  },
                  required_files: [
                    '/usr/sap',
                  ],
                },
                '/usr/sap/_SID_' => {
                  per_sid: true,
                  file_params: {
                    owner: '_sid_adm',
                    group: 'sapsys',
                    mode: '0755',
                  },
                  mount_params: {
                    managed: false,
                  },
                  required_files: [
                    '/usr/sap',
                  ],
                },
              },
            },
          }
        end

        # Ensure it compiles
        it { is_expected.to compile.with_all_deps }

        # Ensure the mount dependency class is included
        it { is_expected.to contain_class('sap::install::mount_dependencies') }

        # Project the expected file & resource counts
        it {
          is_expected.to have_sap__config__mount_point_resource_count(7)
          is_expected.to have_nfs__client__mount_resource_count(3)
        }

        it 'includes the sapmnt directories and creates the associated nfs client mounts' do
          is_expected.to contain_sap__config__mount_point('/sapmnt')

          is_expected.to contain_file('/sapmnt').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )

          ['EP0', 'WP0'].each do |sid|
            sid_lower = sid.downcase
            is_expected.to contain_sap__config__mount_point("/sapmnt/_SID__#{sid}")
            is_expected.to contain_nfs__client__mount("/sapmnt/#{sid}").with(
              ensure: 'mounted',
              server: 'nfs.puppet.sap',
              share: "/srv/nfs/sapmnt/#{sid}",
              atboot: true,
              options_nfsv4: 'rw,soft,noac,timeo=200,retrans=3,proto=tcp',
              owner: "#{sid_lower}adm",
              group: 'sapsys',
              mode: '0755',
            ).that_requires('File[/sapmnt]')
          end
        end

        it 'creates the base instance directories' do
          is_expected.to contain_sap__config__mount_point('/usr/sap')
          is_expected.to contain_sap__config__mount_point('/usr/sap/trans')

          is_expected.to contain_file('/usr/sap').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )

          is_expected.to contain_nfs__client__mount('/usr/sap/trans').with(
            ensure: 'mounted',
            server: 'nfs.puppet.sap',
            share: '/srv/nfs/trans',
            atboot: true,
            options_nfsv4: 'rw,soft,noac,timeo=200,retrans=3,proto=tcp',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          ).that_requires('File[/usr/sap]')

          ['EP0', 'WP0'].each do |sid|
            sid_lower = sid.downcase
            is_expected.to contain_sap__config__mount_point("/usr/sap/_SID__#{sid}")
            is_expected.to contain_file("/usr/sap/#{sid}").with(
              ensure: 'directory',
              owner: "#{sid_lower}adm",
              group: 'sapsys',
              mode: '0755',
            ).that_requires('File[/usr/sap]')
          end
        end
      end
    end
  end

  # Currently configuration only supports db2 on a limited set of OS
  # combinations
  test_on = {
    hardwaremodels: ['x86_64', 'ppc64'],
    supported_os: [
      {
        'operatingsystem' => 'RedHat',
        'operatingsystemrelease' => ['7'],
      },
    ],
  }
  on_supported_os(test_on).each do |os, facts|
    context "db2 on #{os}" do
      let(:facts) { facts }

      before(:each) do
        facts.merge!(
          page_size: 4_096,
          memory: {
            system: {
              total_bytes: 7_930_249_216,
            },
            swap: {
              total_bytes: 4_294_901_760,
            },
          },
        )
      end

      context 'without mountpoints' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
              'EP1' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: false,
          }
        end

        it 'compiles' do
          is_expected.to compile.with_all_deps
        end

        # Check the sysctl file
        it { is_expected.to contain_file('/etc/sysctl.d/00-sap-db2.conf') }

        # Ensure the contents of the sysctl file match our expectations
        it 'is expected to contain valid db2 sysctl entries in /etc/sysctl.d/00-sap-db2.conf' do
          content = catalogue.resource('file', '/etc/sysctl.d/00-sap-db2.conf').send(:parameters)[:content]
          expect(content).to match(%r{\nkernel[.]shmmni = 1890\n})
          expect(content).to match(%r{\nkernel[.]shmmax = 7930249216\n})
          expect(content).to match(%r{\nkernel[.]shmall = 3872192\n})
          expect(content).to match(%r{\nkernel[.]sem = 250 256000 32 1890\n})
          expect(content).to match(%r{\nkernel[.]msgmni = 7562\n})
          expect(content).to match(%r{\nkernel[.]msgmax = 65536\n})
          expect(content).to match(%r{\nkernel[.]msgmnb = 65536\n})
          expect(content).to match(%r{\nvm[.]swappiness = 0\n})
          expect(content).to match(%r{\nvm[.]overcommit_memory = 0\n})
        end

        # Ensure we have the filter file
        it { is_expected.to contain_file('/etc/security/limits.d/00-sap-db2.conf') }

        it 'is expected to contain valid sid specific limits for the database in /etc/security/limits.d/00-sap-db2.conf' do
          content = catalogue.resource('file', '/etc/security/limits.d/00-sap-db2.conf').send(:parameters)[:content]
          expect(content).to match(%r{\n@dbep0adm    hard    data    unlimited\n})
          expect(content).to match(%r{\n@dbep0adm    soft    data    unlimited\n})
          expect(content).to match(%r{\n@dbep0adm    hard    nofile    65536\n})
          expect(content).to match(%r{\n@dbep0adm    soft    nofile    65536\n})
          expect(content).to match(%r{\n@dbep0adm    hard    fsize    unlimited\n})
          expect(content).to match(%r{\n@dbep0adm    soft    fsize    unlimited\n})
          expect(content).to match(%r{\n@dbep1adm    hard    data    unlimited\n})
          expect(content).to match(%r{\n@dbep1adm    soft    data    unlimited\n})
          expect(content).to match(%r{\n@dbep1adm    hard    nofile    65536\n})
          expect(content).to match(%r{\n@dbep1adm    soft    nofile    65536\n})
          expect(content).to match(%r{\n@dbep1adm    hard    fsize    unlimited\n})
          expect(content).to match(%r{\n@dbep1adm    soft    fsize    unlimited\n})
        end
      end

      context 'missing per_sid value' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: true,
            mount_points: {
              db2: {
                '/db2/Missing/per_sid' => {
                },
              },
            },
          }
        end

        it 'raises an error about missing SIDs' do
          is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: 'db2' path '/db2/Missing/per_sid' missing 'per_sid'!})
        end
      end

      context 'missing file_params' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: true,
            mount_points: {
              db2: {
                '/db2/Missing/file_params' => {
                  per_sid: false,
                },
              },
            },
          }
        end

        it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: 'db2' path '/db2/Missing/file_params' missing 'file_params'!}) }
      end

      context 'SID specific but missing substitution' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: true,
            mount_points: {
              db2: {
                '/db2/Missing/pattern' => {
                  per_sid: true,
                  file_params: {
                    owner: 'db2_sid_',
                    group: 'db_sid_adm',
                    mode: '0755',
                  },
                },
              },
            },
          }
        end

        it 'raises error' do
          is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: SID specified for '/db2/Missing/pattern' but does not match '/_SID_/' or '/_sid_/'!})
        end
      end

      context 'count specified without pattern' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: true,
            mount_points: {
              db2: {
                '/db2/Missing/count_pattern' => {
                  per_sid: true,
                  count: 4,
                  file_params: {
                    owner: 'db2_sid_',
                    group: 'db_sid_adm',
                    mode: '0755',
                  },
                },
              },
            },
          }
        end

        it {
          is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '/db2/Missing/count_pattern' specifies \$count but does not contain '_N_'!})
        }
      end

      context 'invalid count specified' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
            },
            create_mount_points: true,
            mount_points: {
              db2: {
                '/db2/bad/count_N_' => {
                  per_sid: true,
                  count: 0,
                  file_params: {
                    owner: 'db2_sid_',
                    group: 'db_sid_adm',
                    mode: '0755',
                  },
                },
              },
            },
          }
        end

        it { is_expected.to compile.with_all_deps.and_raise_error(%r{mount_point: '/db2/bad/count_N_' specifies \$count not >= 1!}) }
      end

      context 'valid directories with non-db2 relevant SID' do
        let(:params) do
          {
            system_ids: {
              'EP0' => {
                manage_profiles: false,
                components: ['db2'],
              },
              'EP1' => {
                manage_profiles: false,
                components: ['db2'],
              },
              'SCP' => {
                manage_profiles: false,
                components: ['liveCache', 'liveCache_client'],
                exclude_common_mount_points: true,
                pattern_values: { lcid_upper: 'SLP' },
              },
            },
            create_mount_points: true,
          }
        end

        # Ensure we included the mountpoint class
        it 'includes the mountpoint classes' do
          is_expected.to contain_class('sap::config')
          is_expected.to contain_class('sap::config::mount_points')
        end

        # Ensure DB2 packages are present
        it 'installes the db2 pre-requisite classes' do
          is_expected.to contain_package('libaio').with_ensure('installed')
          is_expected.to contain_package('ksh').with_ensure('installed')
        end

        # Ensure Db2 classes are present as well
        it 'includes the DB2 query class' do
          is_expected.to contain_class('sap::management::db2_queries')
        end

        context 'on ppc64' do
          before(:each) do
            facts.merge!(
              os: {
                architecture: 'ppc64',
                family: 'RedHat',
                hardware: 'ppc64',
                name: 'RedHat',
                release: {
                  full: '7.5',
                  major: '7',
                  minor: '5',
                },
              },
            )
          end

          # DB2 requires the following backend packages
          it 'db6 xlc compiler runtime is present' do
            is_expected.to contain_package('vacpp.rte').with_ensure('installed')
          end
        end

        it 'ensure that the common directories are created' do
          is_expected.to contain_file('/sapmnt').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )
          is_expected.to contain_sap__config__mount_point('/usr/sap')
          is_expected.to contain_file('/usr/sap').with(
            ensure: 'directory',
            owner: 'root',
            group: 'sapsys',
            mode: '0755',
          )
          ['EP0', 'EP1'].each do |sid|
            sid_lower = sid.downcase
            is_expected.to contain_sap__config__mount_point("/sapmnt/_SID__#{sid}")
            is_expected.to contain_file("/sapmnt/#{sid}").with(
              ensure: 'directory',
              owner: "#{sid_lower}adm",
              group: 'sapsys',
              mode: '0755',
            ).that_requires('File[/sapmnt]')
          end
        end

        it 'ensure there are no appserver directories' do
          is_expected.not_to contain_file('/usr/sap/EP0')
          is_expected.not_to contain_file('/usr/sap/EP1')
          is_expected.not_to contain_file('/usr/sap/trans')
        end

        it 'creates the database mount points' do
          # Base db2
          is_expected.to contain_sap__config__mount_point('/db2')
          is_expected.to contain_file('/db2').with(
            ensure: 'directory',
            owner: 'root',
            group: 'root',
            mode: '0755',
          )

          # SID specific directories
          sid_dirs = {
            '/db2/db2_sid_' => { mode: '0755' },
            '/db2/_SID_' => { mode: '0755' },
            '/db2/_SID_/db2_sid_' => { mode: '0755' },
            '/db2/_SID_/log_dir' => { mode: '0755' },
            '/db2/_SID_/log_archive' => { mode: '0755' },
            '/db2/_SID_/db2dump' => { mode: '0755' },
            '/db2/_SID_/sapdata_N_' => { mode: '0750', num: 4 },
            '/db2/_SID_/saptmp_N_' => { mode: '0750', num: 4 },
          }
          ['EP0', 'EP1'].each do |sid|
            sid_lower = sid.downcase
            sid_dirs.each do |dir_base, detail|
              dir_sub = dir_base.gsub(%r{_SID_}, sid).gsub(%r{_sid_}, sid_lower)
              dir_parent = File.dirname(dir_sub)
              is_expected.to contain_sap__config__mount_point("#{dir_base}_#{sid}")
              if dir_sub.include?('_N_')
                (1..detail[:num]).each do |num|
                  dir_sub_num = dir_sub.gsub(%r{_N_}, num.to_s)
                  is_expected.to contain_file(dir_sub_num).with(
                    ensure: 'directory',
                    owner: "db2#{sid_lower}",
                    group: "db#{sid_lower}adm",
                    mode: detail[:mode],
                  ).that_requires("File[#{dir_parent}]")
                end
              else
                is_expected.to contain_file(dir_sub).with(
                  ensure: 'directory',
                  owner: "db2#{sid_lower}",
                  group: "db#{sid_lower}adm",
                  mode: detail[:mode],
                ).that_requires("File[#{dir_parent}]")
              end
            end
          end
        end
      end
    end
  end
end
