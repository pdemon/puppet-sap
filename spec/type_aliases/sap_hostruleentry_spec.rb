require 'spec_helper'

# See https://launchpad.support.sap.com/#/notes/1069911 for detail

describe 'Sap::HostRuleEntry' do
  it 'accepts a wildcard' do
    is_expected.to allow_value('*')
  end

  it 'accepts various types of hostnames' do
    is_expected.to allow_value('prd-app00', 'prd-app01.example.org')
  end

  it 'accepts wildcard domain names' do
    is_expected.to allow_value('*.example.org', '*.sap.example.org')
  end

  it 'accepts IPv4 addresses' do
    is_expected.to allow_value('10.58.23.17', '192.168.0.54', '172.17.17.54')
  end

  it 'accepts IPv4 addresses with wildcard ending octets' do
    is_expected.to allow_value('10.58.23.*', '10.58.*.*', '10.*.*.*')
  end

  it 'rejects wildcard domains where the wildcard is not at the extreme left' do
    is_expected.not_to allow_value('sap.*.example.org', 'sap*server.example.org', 'sap*')
  end

  it 'rejects wildcard IPv4 addresses where the wildcard is not at the extreme right and not all octets are represented' do
    is_expected.not_to allow_value('192.168.*.17', '19.5*.17.48', '*.10.0.4', '10.48.*')
  end
end
