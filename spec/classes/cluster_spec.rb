require 'spec_helper'
require 'deep_merge'

describe 'sap::cluster' do
  let(:node) { 'pr0-dba.example.org' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:sid) { 'PR0' }
      let(:sid_lower) { 'pr0' }
      let(:node_roles_4node) do
        {
          "#{sid_lower}-app00.example.org" => [
            'app-scs',
            'app-standard',
          ],
          "#{sid_lower}-app01.example.org" => [
            'app-scs',
            'app-standard',
          ],
          "#{sid_lower}-dba.example.org" => ['db-standard'],
          "#{sid_lower}-dbb.example.org" => ['db-standard'],
        }
      end
      let(:component_scs) do
        {
          type: 'ScsErs',
          data: {
            ers_ip: '10.0.0.10',
            ers_inst: {
              type: 'ERS',
              number: '02',
              host: "#{sid_lower}-ers",
            },
            scs_ip: '10.0.0.11',
            scs_inst: {
              type: 'ASCS',
              number: '01',
              host: "#{sid_lower}-cs",
            },
          },
        }
      end
      let(:component_as) do
        {
          type: 'As',
          data: {
            as_instances: [
              {
                'type' => 'DVEBMGS',
                'number' => '00',
                'host' => "#{sid_lower}-app00",
              },
              {
                'type' => 'D',
                'number' => '00',
                'host' => "#{sid_lower}-app01",
              },
            ],
          },
        }
      end
      let(:component_db2) do
        {
          type: 'Db2',
          data: {
            db_ip: '10.0.0.12',
            mode: 'log_shipping',
            hadr_peer_window: 200,
            volumes: [
              'volgrpname' => 'db2cv',
            ],
            filesystems: [
              'volume_group' => 'db2cv',
              'logical_volume' => 'db2cvarch',
              'directory' => "/db2/#{sid}/commvault/archive",
              'fstype' => 'xfs',
            ],
          },
        }
      end
      let(:params) do
        {
          cib_name: 'sap',
          sid_rules: {
            sid => {
              node_roles: node_roles_4node,
              components: [],
            },
          },
        }
      end

      context 'SID missing \'node_roles\'' do
        let(:params) do
          {
            cib_name: 'sap',
            sid_rules: {
              'PR0' => {},
            },
          }
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: 'node_roles' parameter is mandatory!})
        end
      end

      context 'SID missing \'components\'' do
        let(:params) do
          {
            cib_name: 'sap',
            sid_rules: {
              'PR0' => {
                node_roles: {},
              },
            },
          }
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: 'components' parameter is mandatory!})
        end
      end

      context 'component missing type' do
        before(:each) do
          params[:sid_rules]['PR0'].merge!(
            components: [
              {},
            ],
          )
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: component missing 'type'!})
        end
      end

      context 'component missing data' do
        before(:each) do
          params[:sid_rules]['PR0'].merge!(
            components: [
              {
                type: 'ScsErs',
              },
            ],
          )
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: component missing 'data'!})
        end
      end

      context 'As component declared without ScsErs' do
        before(:each) do
          params[:sid_rules]['PR0'].merge!(
            components: [
              {
                type: 'As',
              },
            ],
          )
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: when As type is defined ScsErs is mandatory!})
        end
      end

      context 'As component declared without Database' do
        before(:each) do
          params[:sid_rules]['PR0'].merge!(
            components: [
              {
                type: 'As',
              },
              {
                type: 'ScsErs',
              },
            ],
          )
        end

        it 'raises error' do
          is_expected.to compile.and_raise_error(%r{cluster PR0: when As type is defined Db2 is mandatory!})
        end
      end

      context 'valid four node scs/ers, as, and db2 config with fencing enabled' do
        let(:fence_agent_lpar) do
          {
            'type' => 'fence_lpar',
            'data' => {
              ipaddr: 'hmc00.exmaple.org',
              login: 'service-fence',
              managed: 'host0',
              identity_file: '/root/.ssh/service-fence_rsa',
              pcmk_delay_max: 10,
              pcmk_host_map: {
                "#{sid_lower}-app00.example.org" => "#{sid_lower}-app00",
                "#{sid_lower}-dba.example.org" => "#{sid_lower}-dba",
              },
            },
          }
        end
        let(:fence_agent_lpar2) do
          {
            'type' => 'fence_lpar',
            'data' => {
              ipaddr: 'hmc00.exmaple.org',
              login: 'service-fence',
              managed: 'host1',
              identity_file: '/root/.ssh/service-fence_rsa',
              pcmk_delay_max: 10,
              pcmk_host_map: {
                "#{sid_lower}-app01.example.org" => "#{sid_lower}-app01",
                "#{sid_lower}-dbb.example.org" => "#{sid_lower}-dbb",
              },
            },
          }
        end

        before(:each) do
          params.deep_merge!(
            enable_fencing: true,
            sid_rules: {
              sid => {
                node_roles: node_roles_4node,
                components: [
                  component_as,
                  component_scs,
                  component_db2,
                ],
              },
            },
            fence_agents: [
              fence_agent_lpar,
              fence_agent_lpar2,
            ],
          )
        end

        it 'fails when fence_agents is missing' do
          params[:fence_agents] = []
          is_expected.to compile.and_raise_error(%r{cluster: at least one fence agent must be defined when fencing is enabled!})
        end

        ['type', 'data'].each do |key|
          it "fails when a fence agent key '#{key}' is missing" do
            params[:fence_agents][0].delete(key)
            is_expected.to compile.and_raise_error(%r{cluster: fence_agent entry missing '#{key}'!})
          end
        end

        it 'fails when a fence agent is missing pcmk_host_map' do
          params[:fence_agents][0]['data'].delete(:pcmk_host_map)
          is_expected.to compile.and_raise_error(%r{cluster: all currently supported fence agents require 'pcmk_host_map' to be defined!})
        end

        it 'fails when a node is not covered by a fence agent' do
          node = "#{sid_lower}-app00.example.org"
          params[:fence_agents][0]['data'][:pcmk_host_map].delete(node)
          is_expected.to compile.and_raise_error(%r{cluster: node '#{node}' must be targeted by exactly one fence agent!})
        end

        it 'fails when a node is covered by more than one fence agent' do
          node = "#{sid_lower}-app00.example.org"
          params[:fence_agents][1]['data'][:pcmk_host_map][node] = 'garbage'
          is_expected.to compile.and_raise_error(%r{cluster: node '#{node}' cannot be targeted by more than one fence agent!})
        end

        it 'enables stonith' do
          is_expected.to contain_cs_property('stonith-enabled').with(
            cib: 'sap',
            value: 'true',
          )
        end

        it 'creates each fence agent' do
          node_list = []
          params[:sid_rules].each do |_sid, sid_data|
            sid_data[:node_roles].each do |node, _roles|
              node_list << node
            end
          end
          params[:fence_agents].each_index do |idx|
            agent_type = params[:fence_agents][idx]['type']
            agent_data = params[:fence_agents][idx]['data']
            agent_parms = agent_data.merge(
              cib_name: params[:cib_name],
              node_list: node_list,
            )
            fence_name = "#{agent_type}_#{idx}"
            is_expected.to contain_sap__cluster__fence_lpar(fence_name).with(
              agent_parms,
            )
          end
        end
      end

      context 'valid four node scs/ers, as, and db2 configuration' do
        before(:each) do
          sid = 'PR0'
          params[:sid_rules][sid].merge!(
            components: [
              component_as,
              component_scs,
              component_db2,
            ],
          )
        end

        it 'compiles with all dependencies' do
          is_expected.to compile.with_all_deps
        end

        it 'declares the scsers type' do
          is_expected.to contain_sap__cluster__scsers("#{sid}_ScsErs").with(
            sid: sid,
            cib_name: 'sap',
            nodes: {
              "#{sid_lower}-app00.example.org" => [
                'app-scs',
                'app-standard',
              ],
              "#{sid_lower}-app01.example.org" => [
                'app-scs',
                'app-standard',
              ],
              "#{sid_lower}-dba.example.org" => ['db-standard'],
              "#{sid_lower}-dbb.example.org" => ['db-standard'],
            },
            before: 'Cs_commit[sap]',
            ers_ip: '10.0.0.10',
            ers_inst: {
              'type' => 'ERS',
              'number' => '02',
              'host' => "#{sid_lower}-ers",
            },
            scs_ip: '10.0.0.11',
            scs_inst: {
              'type' => 'ASCS',
              'number' => '01',
              'host' => "#{sid_lower}-cs",
            },
          )
        end

        it 'declares the as type' do
          is_expected.to contain_sap__cluster__as('PR0_As').with(
            sid: 'PR0',
            cib_name: 'sap',
            nodes: {
              'pr0-app00.example.org' => [
                'app-scs',
                'app-standard',
              ],
              'pr0-app01.example.org' => [
                'app-scs',
                'app-standard',
              ],
              'pr0-dba.example.org' => ['db-standard'],
              'pr0-dbb.example.org' => ['db-standard'],
            },
            before: 'Cs_commit[sap]',
            db_mode: 'log_shipping',
            as_instances: [
              {
                'type' => 'DVEBMGS',
                'number' => '00',
                'host' => 'pr0-app00',
              },
              {
                'type' => 'D',
                'number' => '00',
                'host' => 'pr0-app01',
              },
            ],
          )
        end

        it 'declares the db2 type' do
          sid = 'PR0'
          is_expected.to contain_sap__cluster__db2('PR0_Db2').with(
            sid: sid,
            cib_name: 'sap',
            nodes: {
              'pr0-app00.example.org' => [
                'app-scs',
                'app-standard',
              ],
              'pr0-app01.example.org' => [
                'app-scs',
                'app-standard',
              ],
              'pr0-dba.example.org' => ['db-standard'],
              'pr0-dbb.example.org' => ['db-standard'],
            },
            before: 'Cs_commit[sap]',
            db_ip: '10.0.0.12',
            mode: 'log_shipping',
            hadr_peer_window: 200,
            volumes: [
              'volgrpname' => 'db2cv',
            ],
            filesystems: [
              'volume_group' => 'db2cv',
              'logical_volume' => 'db2cvarch',
              'directory' => "/db2/#{sid}/commvault/archive",
              'fstype' => 'xfs',
            ],
          )
        end

        it 'ensures an asymmetric cluster' do
          is_expected.to contain_cs_property('symmetric-cluster').with(
            cib: 'sap',
            value: 0,
          )
        end

        it 'defaults to disabling stonith' do
          is_expected.to contain_cs_property('stonith-enabled').with(
            cib: 'sap',
            value: 'false',
          )
        end

        it 'populates a valid stickiness value' do
          is_expected.to contain_cs_rsc_defaults('resource-stickiness').with(
            cib: 'sap',
            value: 500,
          )
        end

        context 'altered stickiness' do
          before(:each) do
            params.merge!(
              resource_stickiness: 1000,
            )
          end

          it 'accepts changes to the stickiness' do
            is_expected.to contain_cs_rsc_defaults('resource-stickiness').with(
              cib: 'sap',
              value: 1000,
            )
          end
        end

        it 'performs a cs_commit' do
          is_expected.to contain_cs_shadow('sap')
          is_expected.to contain_cs_commit('sap')
        end
      end
    end
  end
end
