require 'spec_helper'

describe 'sap::cluster::filesystem' do
  let(:title) { 'db2pr0_filesystem_0' }
  let(:params) do
    {
      sid: 'PR0',
      cib_name: 'sap',
      lvm_primitives: ['prm_PR0_othervg_LVM'],
      volume_group: 'db2cv',
      logical_volume: 'db2cvarch',
      fstype: 'xfs',
      directory: '/db2/PR0/commvault/archive',
    }
  end

  context 'missing the correct lvm_primitive' do
    it 'fails and alerts' do
      is_expected.to compile.and_raise_error(%r{The parent LVM primitive resource describing the db2cv vg must be defined for filesystem /db2/PR0/commvault/archive!})
    end
  end

  context 'with valid arguments' do
    before(:each) do
      params.merge!(
        lvm_primitives: ['prm_PR0_db2cv_LVM'],
      )
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'constructs the primitive as expected' do
      operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout'  => '60s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout'  => '60s',
          },
        },
        {
          'monitor' => {
            'interval' => '20s',
            'timeout'  => '40s',
          },
        },
        {
          'notify' => {
            'interval' => '0s',
            'timeout'  => '60s',
          },
        },
      ]
      is_expected.to contain_cs_primitive('prm_PR0_filesystem_db2_PR0_commvault_archive').with(
        primitive_class: 'ocf',
        primitive_type: 'Filesystem',
        provided_by: 'heartbeat',
        cib: 'sap',
        parameters: [
          'device' => '/dev/db2cv/db2cvarch',
          'directory' => '/db2/PR0/commvault/archive',
          'fstype' => 'xfs',
        ],
        operations: operations,
      )
    end
  end
end
