require 'spec_helper'
require 'deep_merge'

describe 'sap::cluster::db2' do
  let(:title) { 'PR0_Db2' }
  let(:params) do
    {
      sid: 'PR0',
      cib_name: 'sap',
      nodes: {
        'pr0-app00.example.org' => ['app-scs'],
        'pr0-app01.example.org' => ['app-scs'],
        'pr0-dba.example.org' => ['db-standard'],
        'pr0-dbb.example.org' => ['db-standard'],
      },
      db_ip: '10.0.0.20',
      mode: 'log_shipping',
      hadr_peer_window: 300,
      volumes: [],
      filesystems: [],
    }
  end

  context 'no relevant node specified' do
    before(:each) do
      params.merge!(
        nodes: {
          'pr0-app00.example.org' => ['app-scs'],
          'pr0-app01.example.org' => ['app-scs'],
        },
      )
    end

    it 'compiles and errors on no relevant nodes' do
      is_expected.to compile.with_all_deps.and_raise_error(%r{ClusterDb2: #{title} no nodes with db-standard role specified!})
    end
  end

  it 'creates the service IP primitive' do
    dbname = 'pr0'
    dbinst = "db2#{dbname}"
    db2prim_ip = "prm_#{dbinst}_IP"
    operations = [
      {
        'start' => {
          'interval' => '0s',
          'timeout'  => '20s',
        },
      },
      {
        'stop' => {
          'interval' => '0s',
          'timeout'  => '20s',
        },
      },
      {
        'monitor' => {
          'interval' => '10s',
          'timeout'  => '20s',
        },
      },
    ]
    is_expected.to contain_cs_primitive(db2prim_ip).with(
      primitive_class: 'ocf',
      primitive_type: 'IPaddr2',
      provided_by: 'heartbeat',
      cib: params[:cib_name],
      parameters: [
        'ip' => params[:db_ip],
      ],
      operations: operations,
    )
    params[:nodes].each do |node, roles|
      if roles.include? 'db-standard'
        is_expected.to contain_cs_location("locate-#{db2prim_ip}-#{node}").with(
          cib: params[:cib_name],
          primitive: db2prim_ip,
          node_name: node,
          score: '200',
        ).that_requires("Cs_primitive[#{db2prim_ip}]")
      else
        is_expected.not_to contain_cs_location("locate-#{db2prim_ip}-#{node}")
      end
    end
  end

  context 'log_shipping' do
    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the master primitive' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      db2prim_base = "prm_#{params[:sid]}_DB"
      db2prim = "ms_#{db2prim_base}"
      monitor_interval = params[:hadr_peer_window] - 30
      monitor_interval_master = monitor_interval + 2
      promote_timeout = params[:hadr_peer_window] + 20
      operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout' => '120s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout' => '120s',
          },
        },
        {
          'monitor' => {
            'interval' => "#{monitor_interval}s",
            'timeout' => '60s',
          },
        },
        {
          'monitor' => {
            'interval' => "#{monitor_interval_master}s",
            'timeout' => '60s',
            'role' => 'Master',
          },
        },
        {
          'promote' => {
            'interval' => '0s',
            'timeout' => "#{promote_timeout}s",
          },
        },
        {
          'demote' => {
            'interval' => '0s',
            'timeout' => "#{promote_timeout}s",
          },
        },
      ]
      is_expected.to contain_cs_primitive(db2prim_base).with(
        primitive_class: 'ocf',
        primitive_type: 'db2',
        provided_by: 'heartbeat',
        cib: params[:cib_name],
        promotable: true,
        ms_metadata: {
          'master-max' => '1',
          'notify' => true,
        },
        parameters: {
          'instance' => dbinst,
          'dblist' => dbname,
        },
        operations: operations,
      )
      params[:nodes].each do |node, roles|
        if roles.include? 'db-standard'
          is_expected.to contain_cs_location("locate-#{db2prim}-#{node}").with(
            cib: params[:cib_name],
            primitive: db2prim,
            node_name: node,
            score: '200',
          ).that_requires("Cs_primitive[#{db2prim_base}]")
        else
          is_expected.not_to contain_cs_location("locate-#{db2prim}-#{node}")
        end
      end
    end

    it 'does not contain filesystem/LVM primitives, their group, or their rules' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      db2fsgrp = "grp_#{dbinst}_fs"
      is_expected.to have_sap__cluster__filesystem_resource_count(0)
      is_expected.to have_sap__cluster__lvm_resource_count(0)
      is_expected.to have_cs_group_resource_count(0)
      is_expected.not_to contain_cs_group(db2fsgrp)
      is_expected.not_to contain_cs_colocation("colocate-#{db2fsgrp}-with-#{dbinst}_master")
      # rubocop:disable Lint/UnusedBlockArgument
      params[:nodes].each do |node, roles|
        # rubocop:enable Lint/UnusedBlockArgument
        is_expected.not_to contain_cs_location("locate-#{db2fsgrp}-#{node}")
      end
    end

    it 'colocates the IP with the master database instance and starts it second' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      db2prim_base = "prm_#{params[:sid]}_DB"
      db2prim = "ms_#{db2prim_base}"
      db2prim_ip = "prm_#{dbinst}_IP"
      is_expected.to contain_cs_colocation("colocate-#{db2prim_ip}-with-#{db2prim}_master").with(
        cib: 'sap',
        score: '2000',
        primitives: [
          "#{db2prim}:Master",
          db2prim_ip,
        ],
      ).that_requires(
        [
          "Cs_primitive[#{db2prim_base}]",
          "Cs_primitive[#{db2prim_ip}]",
        ],
      )
    end

    context 'with filesystems' do
      before(:each) do
        instdir = "/db2/#{params[:sid]}"
        params.merge!(
          volumes: [
            { 'volgrpname' => 'db2cv' },
          ],
          filesystems: [
            {
              'volume_group' => 'db2cv',
              'logical_volume' => 'db2cvarch',
              'directory' => "#{instdir}/commvault/archive",
              'fstype' => 'xfs',
            },
            {
              'volume_group' => 'db2cv',
              'logical_volume' => 'db2cvretrieve',
              'directory' => "#{instdir}/commvault/retrieve",
              'fstype' => 'xfs',
            },
            {
              'volume_group' => 'db2cv',
              'logical_volume' => 'db2cvaudit',
              'directory' => "#{instdir}/commvault/audit",
              'fstype' => 'xfs',
            },
          ],
        )
      end

      it 'compiles' do
        is_expected.to compile.with_all_deps
      end

      it 'creates the correct number of primitives' do
        is_expected.to have_sap__cluster__lvm_resource_count(1)
        is_expected.to have_sap__cluster__filesystem_resource_count(3)
        # 1 lvm, 3 filesystem, 1 ip, and 1 db2
        is_expected.to have_cs_primitive_resource_count(6)
      end

      it 'creates the LVM primitives' do
        dbname = 'pr0'
        dbinst = "db2#{dbname}"
        params[:volumes].each_index do |idx|
          volgrpname = params[:volumes][idx]['volgrpname']
          is_expected.to contain_sap__cluster__lvm("#{dbinst}_lvm_#{idx}")
          is_expected.to contain_cs_primitive("prm_#{params[:sid]}_#{volgrpname}_LVM")
        end
      end

      it 'creates the filesystem primitives' do
        dbname = 'pr0'
        dbinst = "db2#{dbname}"
        params[:filesystems].each_index do |idx|
          directory = params[:filesystems][idx]['directory'].gsub(%r{[/]}, '_')
          is_expected.to contain_sap__cluster__filesystem("#{dbinst}_filesystem_#{idx}")
          is_expected.to contain_cs_primitive("prm_#{params[:sid]}_filesystem#{directory}")
        end
      end

      it 'creates the filesystem group and associated relationships' do
        dbname = 'pr0'
        dbinst = "db2#{dbname}"
        db2prim_base = "prm_#{params[:sid]}_DB"
        db2prim = "ms_#{db2prim_base}"
        db2fsgrp = "grp_#{dbinst}_fs"
        is_expected.to contain_cs_group(db2fsgrp).with(
          cib: params[:cib_name],
          primitives: [
            "prm_#{params[:sid]}_db2cv_LVM",
            "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_archive",
            "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_audit",
            "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_retrieve",
          ],
        ).that_requires(
          [
            "Cs_primitive[prm_#{params[:sid]}_db2cv_LVM]",
            "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_archive]",
            "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_audit]",
            "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_retrieve]",
          ],
        )
        is_expected.to contain_cs_colocation("colocate-#{db2prim}_master-with-#{db2fsgrp}").with(
          cib: params[:cib_name],
          score: '2000',
          primitives: [
            db2fsgrp,
            "#{db2prim}:Master",
          ],
        ).that_requires(
          [
            "Cs_primitive[#{db2prim_base}]",
            "Cs_group[#{db2fsgrp}]",
          ],
        )
        params[:nodes].each do |node, roles|
          unless roles.include? 'db-standard'
            next
          end
          is_expected.to contain_cs_location("locate-#{db2fsgrp}-#{node}").with(
            cib: params[:cib_name],
            primitive: db2fsgrp,
            node_name: node,
            score: '200',
          ).that_requires("Cs_group[#{db2fsgrp}]")
        end
      end
    end
  end

  context 'shared_storage' do
    before(:each) do
      instdir = "/db2/#{params[:sid]}"
      params.merge!(
        mode: 'shared_storage',
        volumes: [
          { 'volgrpname' => 'db2' },
          { 'volgrpname' => 'db2cv' },
        ],
        filesystems: [
          {
            'volume_group' => 'db2',
            'logical_volume' => 'db2base',
            'directory' => instdir,
            'fstype' => 'xfs',
          },
          {
            'volume_group' => 'db2',
            'logical_volume' => 'db2logarch',
            'directory' => "#{instdir}/log_archive",
            'fstype' => 'xfs',
          },
          {
            'volume_group' => 'db2cv',
            'logical_volume' => 'db2cvarch',
            'directory' => "#{instdir}/commvault/archive",
            'fstype' => 'xfs',
          },
          {
            'volume_group' => 'db2cv',
            'logical_volume' => 'db2cvretrieve',
            'directory' => "#{instdir}/commvault/retrieve",
            'fstype' => 'xfs',
          },
          {
            'volume_group' => 'db2cv',
            'logical_volume' => 'db2cvaudit',
            'directory' => "#{instdir}/commvault/audit",
            'fstype' => 'xfs',
          },
        ],
      )
    end

    context 'missing volumes' do
      it 'compiles and raises error' do
        params[:volumes] = []
        is_expected.to compile.and_raise_error(%r{ClusterDb2: #{title} volumes must be specified in #{params[:mode]} mode!})
      end
    end

    context 'missing filesystems' do
      it 'compiles and raises error' do
        params[:filesystems] = []
        is_expected.to compile.and_raise_error(%r{ClusterDb2: #{title} filesystems must be specified in #{params[:mode]} mode!})
      end
    end

    context 'missing dependent volumes' do
      before(:each) do
        params.merge!(
          volumes: [
            { 'volgrpname' => 'db2cv' },
          ],
        )
      end

      it 'raises error' do
        is_expected.to compile.and_raise_error(%r{The parent LVM primitive resource describing the db2 vg must be defined for filesystem /db2/PR0!})
      end
    end

    it 'compiles' do
      is_expected.to compile.with_all_deps
    end

    it 'creates the correct number of primitives' do
      is_expected.to have_sap__cluster__lvm_resource_count(2)
      is_expected.to have_sap__cluster__filesystem_resource_count(5)
      # 2 lvm, 5, filesystem, 1 ip, and 1 db2
      is_expected.to have_cs_primitive_resource_count(9)
    end

    it 'creates the LVM primitives' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      params[:volumes].each_index do |idx|
        volgrpname = params[:volumes][idx]['volgrpname']
        is_expected.to contain_sap__cluster__lvm("#{dbinst}_lvm_#{idx}")
        is_expected.to contain_cs_primitive("prm_#{params[:sid]}_#{volgrpname}_LVM")
      end
    end

    it 'creates the filesystem primitives' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      params[:filesystems].each_index do |idx|
        directory = params[:filesystems][idx]['directory'].gsub(%r{[/]}, '_')
        is_expected.to contain_sap__cluster__filesystem("#{dbinst}_filesystem_#{idx}")
        is_expected.to contain_cs_primitive("prm_#{params[:sid]}_filesystem#{directory}")
      end
    end

    it 'creates the db2 primitive' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      db2prim_base = "prm_#{params[:sid]}_DB"
      monitor_interval = 20
      monitor_interval_master = 22
      promote_timeout = 120
      operations = [
        {
          'start' => {
            'interval' => '0s',
            'timeout' => '120s',
          },
        },
        {
          'stop' => {
            'interval' => '0s',
            'timeout' => '120s',
          },
        },
        {
          'monitor' => {
            'interval' => "#{monitor_interval}s",
            'timeout' => '60s',
          },
        },
        {
          'monitor' => {
            'interval' => "#{monitor_interval_master}s",
            'timeout' => '60s',
            'role' => 'Master',
          },
        },
        {
          'promote' => {
            'interval' => '0s',
            'timeout' => "#{promote_timeout}s",
          },
        },
        {
          'demote' => {
            'interval' => '0s',
            'timeout' => "#{promote_timeout}s",
          },
        },
      ]
      is_expected.to contain_cs_primitive(db2prim_base).with(
        primitive_class: 'ocf',
        primitive_type: 'db2',
        provided_by: 'heartbeat',
        cib: params[:cib_name],
        parameters: {
          'instance' => dbinst,
          'dblist' => dbname,
        },
        operations: operations,
      )
    end

    it 'builds the group with the correct order and assigns its location' do
      dbname = 'pr0'
      dbinst = "db2#{dbname}"
      db2prim_base = "prm_#{params[:sid]}_DB"
      db2prim_ip = "prm_#{dbinst}_IP"
      db2grp = "grp_#{dbinst}"
      group_prim = [
        "prm_#{params[:sid]}_db2_LVM",
        "prm_#{params[:sid]}_db2cv_LVM",
        "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}",
        "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_archive",
        "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_audit",
        "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_retrieve",
        "prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_log_archive",
        db2prim_ip,
        db2prim_base,
      ]
      group_requires = [
        "Cs_primitive[prm_#{params[:sid]}_db2_LVM]",
        "Cs_primitive[prm_#{params[:sid]}_db2cv_LVM]",
        "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}]",
        "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_archive]",
        "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_audit]",
        "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_commvault_retrieve]",
        "Cs_primitive[prm_#{params[:sid]}_filesystem_db2_#{params[:sid]}_log_archive]",
        "Cs_primitive[#{db2prim_ip}]",
        "Cs_primitive[#{db2prim_base}]",
      ]
      is_expected.to contain_cs_group(db2grp).with(
        cib: params[:cib_name],
        primitives: group_prim,
      ).that_requires(group_requires)
      params[:nodes].each do |node, roles|
        unless roles.include? 'db-standard'
          next
        end
        is_expected.to contain_cs_location("locate-#{db2grp}-#{node}").with(
          cib: params[:cib_name],
          primitive: db2grp,
          node_name: node,
          score: '200',
        ).that_requires("Cs_group[#{db2grp}]")
      end
    end
  end
end
