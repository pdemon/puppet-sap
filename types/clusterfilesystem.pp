type Sap::ClusterFilesystem = Hash[
  Enum[
    'volume_group',   # Name of the LVM volume group backing this fs
    'logical_volume', # Name of the LVM logical volume containing this fs
    'directory',      # Mountpoint for this fs
    'fstype',         # Filesystem type used at this mount point
  ],
  Variant[
    Sap::VgLvName,
    Stdlib::AbsolutePath,
    Sap::FsType,
  ]
]
